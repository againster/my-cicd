#!/bin/bash

proj=$(dirname $(realpath "$BASH_SOURCE"))
buildroot="${proj}/build"
distdir="my-cicd"
distroot="${buildroot}/${distdir}"

function buildlib() {
  echo "build armk..."
  cd $proj/go.d/armk || exit $?
  go build -o armk main.go || exit $?
  ./armk -C $proj/mk.d -T ../armormk -pkg armormk || exit $?
  ./armk -C $proj/j2.d/simple -T ../armorjj -pkg armorjj || exit $?
  ./armk -C $proj/schema.d/simple -T ../armorjj -pkg armorjj || exit $?

  echo "build libjj..."
  cd $proj/go.d/libjj || exit $?
  go build -o libjj.so -ldflags "-s -w" -buildmode=c-shared *.go || exit $?

  echo "build libcicd..."
  cd $proj/go.d/libcicd || exit $?
  go build -o libcicd.so -ldflags "-s -w" -buildmode=c-shared *.go || exit $?

  mkdir -p "$distroot/bin"
  cp -afR ${proj}/go.d/libcicd/libcicd.so $distroot/bin || exit $?
  cp -afR ${proj}/go.d/libjj/libjj.so $distroot/bin || exit $?
}

function buildcicd() {
  echo "build cicd..."
  cd $proj || exit $?
  local version="${RELEASE_VERSION:-x.x.x}"
  local autoinstall=$(
    cat -v <<EOF
__version__ = "${version}"

EOF
  )
  echo "$autoinstall" >py.d/_version.py
  test -z "${PYTHON_VENV}" || PYTHON_VENV="${PYTHON_VENV%/}/"
  "${PYTHON_VENV}"python3 -m nuitka --output-filename=cicd --output-dir="$buildroot" --remove-output --assume-yes-for-downloads --no-pyi-file --standalone py.d/cicd || exit $?
  cp -afR $buildroot/cicd.dist/* $distroot/bin || exit $?
  rm -rf "$buildroot/cicd.dist" || exit $?
}

function buildmake() {
  echo "build make..."
  local makever="4.4.1"
  local make="make-${makever}"
  cd $buildroot || exit $?
  if test "$(command -v curl)" != ""; then
    curl -LO "https://ftp.gnu.org/gnu/make/${make}.tar.gz" || exit $?
  elif test "$(command -v wget)" != ""; then
    wget "https://ftp.gnu.org/gnu/make/${make}.tar.gz" || exit $?
  else
    echo "command curl/wget no found" && exit 1
  fi
  tar zxvf "${make}.tar.gz" || exit $?
  cd ${make} || exit $?
  ./configure --with-guile=no || exit $?
  ./build.sh || exit $?

  cp -afR make $distroot/bin || exit $?
}

function buildtar() {
  echo "build tar..."
  cd $buildroot || exit $?

  local version="${RELEASE_VERSION:-x.x.x}"
  local os=$(uname -s | awk '{print tolower($0)}')
  local arch=$(uname -m)
  local final=cicd${version}-${os}-${arch}.tgz

  tar zcvf $final $distdir || exit $?
  ln -snf $final cicd.tgz || exit $?
}

function buildsea() {
  echo "build sea..."
  cd $buildroot || exit $?

  # fix command realpath, readlink -f not found
  # see: https://medium.com/mkdir-awesome/posix-alternatives-for-readlink-21a4bfe0455c

  # fix macos bash argument forward single quote problem. "error: unrecognized arguments"
  # see: How do I forward parameters to other command in bash script? https://stackoverflow.com/questions/1537673/how-do-i-forward-parameters-to-other-command-in-bash-script
  # see: https://stackoverflow.com/questions/12314451/accessing-bash-command-line-args-vs
  local autoinstall=$(
    cat -v <<'EOL'

readlinkf() {
  [ "${1:-}" ] || return 1
  max_symlinks=40
  CDPATH='' # to avoid changing to an unexpected directory

  target=$1
  [ -e "${target%/}" ] || target=${1%"${1##*[!/]}"} # trim trailing slashes
  [ -d "${target:-/}" ] && target="$target/"

  cd -P . 2>/dev/null || return 1
  while [ "$max_symlinks" -ge 0 ] && max_symlinks=$((max_symlinks - 1)); do
    if [ ! "$target" = "${target%/*}" ]; then
      if test "${target:0:1}" = "/"; then
        cd -P "${target%/*}/" 2>/dev/null || break
      else
        cd -P "./${target%/*}" 2>/dev/null || break
      fi
      target=${target##*/}
    fi

    if [ ! -L "$target" ]; then
      target="${PWD%/}${target:+/}${target}"
      printf '%s\n' "${target:-/}"
      return 0
    fi
    link=$(ls -dl -- "$target" 2>/dev/null) || break
    target=${link#*" $target -> "}
  done
  return 1
}

function install() {
  echo "install...";
  file=$(test -L "$0" && readlinkf "$0" || echo "$0");
  me=$(basename "${file}");
  sed '1,/^#__CICD_AUTO_INSTALL_END__$/d' "$me" | tar zxf - -C "/usr/local";
  if test $? -ne 0; then exit $?; fi;
  if test "$(uname -s)" = "Darwin"; then
    echo '/usr/local/my-cicd/bin/cicd "$@";' > /usr/local/my-cicd/bin/cicd.sh || exit $?;
    chmod +x /usr/local/my-cicd/bin/cicd.sh || exit $?;
    ln -snf /usr/local/my-cicd/bin/cicd.sh /usr/local/bin/cicd || exit $?;
  else
    ln -snf /usr/local/my-cicd/bin/cicd /usr/local/bin/cicd || exit $?;
  fi;
  cicd -h;
};

function uninstall() {
  echo "uninstall...";
  rm -rf /usr/local/my-cicd || exit $?;
  echo "uninstall...done";
};

function check() {
  list="git go protoc npm jinja2 docker buildah kubectl bash sed grep md5 md5sum awk tar install";
  for p in ${list[@]}; do
    if test "$(command -v $p)" = ""; then
      echo "checking $p...no";
    else
      echo "checking $p...yes";
    fi;
  done;
};

var_install=1;
while getopts uih flag; do
  if test "${flag}" = "u"; then
    var_uninstall=1;
  elif test "${flag} = "i"; then
    var_install=1;
  elif test "${flag} = "h"; then
    echo -e "-u uninstall\n-i install\n-h help"; exit 0;
  else
    var_install=1;
  fi;
done;

if test "${var_uninstall}" = "1"; then
  uninstall;
elif test "${var_install}" = "1"; then
  check; install;
fi;

exit 0;
EOL
  )

  local version="${RELEASE_VERSION:-x.x.x}"
  local os=$(uname -s | awk '{print tolower($0)}')
  local arch=$(uname -m)
  local final=cicd${version}-${os}-${arch}.sea

  echo "${autoinstall}" >${final}
  echo '#__CICD_AUTO_INSTALL_END__' >>${final}
  cat cicd.tgz >>${final}
}

function buildpkg() {
  echo "build pkg..."
  cd $buildroot || exit $?

  local version="${RELEASE_VERSION:-x.x.x}"
  local os=$(uname -s | awk '{print tolower($0)}')
  local arch=$(uname -m)
  local final=cicd${version}-${os}-${arch}.pkg

  if test "${os}" != "darwin"; then
    echo "build pkg...skip"
    return
  fi

  local autoinstall=$(
    cat -v <<'EOL'
#!/bin/bash

function install() {
  if test "$(uname -s)" = "Darwin"; then
    touch /usr/local/my-cicd/bin/cicd.sh;
    echo '/usr/local/my-cicd/bin/cicd "$@";' > /usr/local/my-cicd/bin/cicd.sh || exit $?;
    chmod +x /usr/local/my-cicd/bin/cicd.sh || exit $?;
    ln -snf /usr/local/my-cicd/bin/cicd.sh /usr/local/bin/cicd || exit $?;
  else
    ln -snf /usr/local/my-cicd/bin/cicd /usr/local/bin/cicd || exit $?;
  fi;
  /usr/local/bin/cicd -h || exit $?;
};

install;

EOL
  )

  mkdir -p mac-pkg/script
  echo "${autoinstall}" >mac-pkg/script/postinstall || exit $?
  chmod +x mac-pkg/script/postinstall || exit $?
  pkgbuild --identifier com.againster-cicd.pkg.app --root my-cicd --version ${version} --scripts mac-pkg/script --install-location /usr/local/my-cicd ${final} || exit $?
}

buildlib
buildmake
buildcicd
buildtar
buildsea
buildpkg

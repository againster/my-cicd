---
tags: CICD
---

# Overviews

my-cicd is a CICD client. You can integrate it into Jenkins or gitlab pipeline. Running the command with a json file, you can relax and enjoy a coffee.

# Tech Design

```mermaid
graph LR
    a(("start"))
    b("init")
    c("build")
    d("install")
    e("pack")
    f("push")
    g("pull")
    h("deploy")
    i(("done"))
    a-->b-->c-->d-->e-->f-->g-->h-->i
```

# Download

## 1.1.1 build 1

Here are all the 1.1.1 version list. You can download and install one of the artifact.

| File name                                                                                                                | Kind      | OS    | Arch   | Size | SHA256 Checksum                                                  |
|--------------------------------------------------------------------------------------------------------------------------|-----------|-------|--------|------|------------------------------------------------------------------|
| [cicd1.1.1-darwin-x86_64.pkg](https://github.com/Againster/my-cicd/releases/download/v1.1.1/cicd1.1.1-darwin-x86_64.pkg) | Installer | macOS | x86-64 | 18MB | `ef766e67364575b6e01f874713b28e230e72fe09de6848fc505d008e79b8bf5a` |
| [cicd1.1.1-darwin-x86_64.sea](https://github.com/Againster/my-cicd/releases/download/v1.1.1/cicd1.1.1-darwin-x86_64.sea) | Archive   | macOS | x86-64 | 18MB | `25ec975f24870dadea5e68aa4bc162309b019e8e54e57bea082e33eb5978d1d0` |
| [cicd1.1.1-darwin-x86_64.tgz](https://github.com/Againster/my-cicd/releases/download/v1.1.1/cicd1.1.1-darwin-x86_64.tgz) | Archive   | macOS | x86-64 | 18MB | `e062f7b339778115394fd7fd22296b5729f9db243488cdce2a30b33bd9ed3db6` |
| [cicd1.1.1-linux-x86_64.sea](https://github.com/Againster/my-cicd/releases/download/v1.1.1/cicd1.1.1-linux-x86_64.sea)   | Archive   | Linux | x86-64 | 16MB | `4c433e2ff1ac7db5d9495c623ca4c0441cb3a129b4f4816f03f5e640b9a6537f` |
| [cicd1.1.1-linux-x86_64.tgz](https://github.com/Againster/my-cicd/releases/download/v1.1.1/cicd1.1.1-linux-x86_64.tgz)   | Archive   | Linux | x86-64 | 16MB | `f2ad1edab95f2ec7cfd54e7f4a65e4544aaf0c7bc2e122fad14599a728196c64` |


## Linux

Recommend installing by `.sea` package.

### sea

Go to download page, select and download any version of package, furthermore install it.

```bash
CICD_VERSION=1.1.1;
curl -LO "https://github.com/Againster/my-cicd/releases/download/v${CICD_VERSION}/cicd${CICD_VERSION}-linux-x86_64.sea";
chmod +x cicd${CICD_VERSION}-linux-x86_64.sea;
sudo ./cicd${CICD_VERSION}-linux-x86_64.sea;
```

### tgz

It's a `tar.gz` archive that you could install to anywhere by `tar` command. Run these scripts:

```bash
CICD_VERSION=1.1.1;
curl -LO "https://github.com/Againster/my-cicd/releases/download/v${CICD_VERSION}/cicd${CICD_VERSION}-linux-x86_64.tgz" ;
tar zxf cicd${CICD_VERSION}-linux-x86_64.tgz -C /usr/local;
ln -snf /usr/local/my-cicd/bin/cicd /usr/local/bin/cicd;
```

### rpm

Coming soon.

### deb

Coming soon.

## Mac

Recommend installing by `.pkg` package.

### pkg

It's a macOS standard installer packages, now available. 

Run an interactive installation using the macOS pkg installer. Follow the installation guides, step by step to be done:
1. [Download the `.pkg` file](https://github.com/Againster/my-cicd/releases/download/v1.1.1b2/cicd1.1.1-darwin-x86_64.pkg).
2. Select installation target volume.
3. Confirm storage space.
4. Use touch ID or input password to allow installing.
5. Install

A silent installation allows you to install the macOS package without user interaction, which can be useful for wide scale deployment. You must have administrator privileges. Follow these steps:

1.  Download the `.pkg` file.
2.  Launch the terminal app (`terminal.app`).
3.  Run the following command:
    ```bash
    CICD_VERSION=1.1.1;
    sudo installer -pkg cicd${CICD_VERSION}-darwin-x86_64.pkg -target /
    ```
4. Enter the administrator password.

### sea

Go to download page, select and download any version of package, furthermore install it.

```bash
CICD_VERSION=1.1.1;
curl -LO "https://github.com/Againster/my-cicd/releases/download/v${CICD_VERSION}/cicd${CICD_VERSION}-darwin-x86_64.sea";
chmod +x cicd${CICD_VERSION}-darwin-x86_64.sea;
sudo ./cicd${CICD_VERSION}-darwin-x86_64.sea;
```

### tgz

It's a `tar.gz` archive that you could install to anywhere by `tar` command. Run these scripts:

```bash
CICD_VERSION=1.1.1;
curl -LO "https://github.com/Againster/my-cicd/releases/download/v${CICD_VERSION}/cicd${CICD_VERSION}-darwin-x86_64.tgz" ;
tar zxf cicd${CICD_VERSION}-darwin-x86_64.tgz -C /usr/local;
ln -snf /usr/local/my-cicd/bin/cicd /usr/local/bin/cicd;
```

## Windows

Coming soon.
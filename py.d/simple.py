# -*- coding: utf-8 -*-
import base64
import copy
import errno
import urllib
import ssl
import os.path
import threading
from pprint import pformat
from comm import *
from jsonschema import validate
import _version

class Var:
    @staticmethod
    def mask_set(spec):
        if hasattr(spec, "var") and hasattr(spec.var, "ctl_panel") and hasattr(spec.var.ctl_panel, "mask"):
            m = set(spec.var.ctl_panel.mask)
            m.discard('')
            if len(m) == 0:
                return None
            return {"mask": m}
        return None


class SimpleInit:
    def __init__(self, spec):
        self.__spec = spec
        self.__cmd = []
        self.__building_tree = dict(dict())  # dict(key:filepath, dict(key:section, value))
        self.__mask = Var.mask_set(self.__spec)
        outdir, workdir, srcdir = self.__spec.init.ctl_panel.outdir, self.__spec.init.ctl_panel.workdir, \
                                  self.__spec.init.ctl_panel.workdir  # srcdir default is workdir
        if hasattr(self.__spec.init.ctl_panel, "srcdir"):
            srcdir = self.__spec.init.ctl_panel.srcdir
        if not os.path.isabs(os.path.expanduser(outdir)):
            outdir = os.path.join(os.getcwd(), outdir)
        if not os.path.isabs(os.path.expanduser(workdir)):
            workdir = os.path.join(os.getcwd(), workdir)
        if not os.path.isabs(os.path.expanduser(srcdir)):
            srcdir = os.path.join(os.getcwd(), srcdir)
        self.__outdir, self.__workdir, self.__srcdir = outdir, workdir, srcdir

    def lint(self):
        s = find_tpl_read("init.json", [self.__spec.var.schema_model_home])
        schema = json.loads(s)
        try:
            validate(self.__spec.to_dict(), schema)
        except Exception as e:
            logging.error("validate spec error: %s", e, extra=self.__mask)
            return False
        return True

    def gen(self):
        self.__gen_build_tree()
        gen_cmd = self.__gen_script_by_building_tree()
        if gen_cmd:
            self.__cmd = [[get_make(), "-I", self.__spec.var.make_home, "init"],
                          [get_make(), "-I", self.__spec.var.make_home, "install"]]

        cmds = []
        for _, cmd in enumerate(self.__cmd):
            cmds.append(" ".join(cmd))
        if len(cmds) > 0:
            logging.debug("run cmd:\n%s", " && ".join(cmds), extra=self.__mask)

    def run(self):
        workdir = self.__workdir

        for _, cmd in enumerate(self.__cmd):
            if run_cmd(cmd, workdir, self.__mask) != 0:
                sys.exit(1)

    def __gen_build_tree(self):
        self.__create_build_tree()
        return self.__building_tree

    def __gen_script_by_building_tree(self):
        var = self.__spec.var
        workdir = self.__workdir
        gen_script = False
        for sub_dir, section in self.__building_tree.items():
            script = self.__gen_script(sub_dir, section)
            path = os.path.join(workdir, sub_dir, var.makefile_name)
            logging.debug("gen script file: %s, script:\n%s", path, script, extra=self.__mask)
            force_write_file_all(path, script)
            gen_script = True
        return gen_script

    def __create_build_tree(self):
        init = self.__spec.init
        if hasattr(init, "git") and init.git is not None:
            self.__create_git_build_tree(init.git, "git")
        logging.debug("build tree:\n%s", pformat(self.__building_tree), extra=self.__mask)
        return

    def __create_git_build_tree(self, git, section):
        if isinstance(git, list):
            for c in git:
                if hasattr(c, "dir"):
                    dir_name = c.dir
                    if dir_name not in self.__building_tree:
                        self.__add_path_to_parent_subdirs(dir_name)
                        self.__building_tree[dir_name] = dict()
                    self.__building_tree[dir_name][section] = c
        return

    def __add_path_to_parent_subdirs(self, path):
        parent = os.path.dirname(path)
        subdir = os.path.basename(path)
        if subdir == "":
            return
        if parent not in self.__building_tree:
            self.__building_tree[parent] = dict()
        if "subdirs" not in self.__building_tree[parent]:
            self.__building_tree[parent]["subdirs"] = set()
        self.__building_tree[parent]["subdirs"].add(subdir)
        self.__add_path_to_parent_subdirs(parent)

    def __gen_script(self, sub_dir, section):
        outdir, workdir, srcdir = self.__outdir, self.__workdir, self.__srcdir
        sdir = os.path.realpath(os.path.expanduser(os.path.join(srcdir, sub_dir)))
        wdir = os.path.realpath(os.path.expanduser(os.path.join(workdir, sub_dir)))
        reldir = os.path.relpath(sdir, wdir)

        all_script = []
        script = self.__gen_script_install(reldir, outdir).strip('\n')
        all_script.append(script)
        if "git" in section:
            script = self.__gen_script_git(section["git"]).strip('\n')
            all_script.append(script)
        if "subdirs" in section:
            script = self.__gen_script_subdirs(DictObj({"subdirs": section["subdirs"]})).strip('\n')
            all_script.append(script)
        return "\n\n".join(all_script)

    def __gen_script_git(self, git_spec):
        mk = self.__spec.init.ctl_panel.git.git_mk
        dirs = self.__spec.init.ctl_panel.j2dir
        ext_dict = overwrite_dict(self.__spec.init.ctl_panel.git.to_dict(), git_spec.to_dict())
        return find_tpl_read_render(mk, dirs, ext_dict)

    def __gen_script_subdirs(self, subdirs_spec):
        mk = "subdirs.mk.j2"
        dirs = self.__spec.init.ctl_panel.j2dir
        return find_tpl_read_render(mk, dirs, subdirs_spec.to_dict())

    def __gen_script_install(self, src, dst):
        mk = "install.mk.j2"
        dst = os.path.expanduser(dst)
        dirs = self.__spec.init.ctl_panel.j2dir
        return find_tpl_read_render(mk, dirs, {"srcdir": src, "destdir": dst})


class SimpleBuildTree:
    def __init__(self, spec):
        self.__spec = spec
        self.__cmd = []
        self.__building_tree = dict(dict())  # dict(key:filepath, dict(key:section, value))
        self.__mask = Var.mask_set(self.__spec)
        outdir, workdir, srcdir = self.__spec.build.ctl_panel.outdir, self.__spec.build.ctl_panel.workdir, \
                                  self.__spec.build.ctl_panel.workdir  # srcdir default is workdir
        if hasattr(self.__spec.build.ctl_panel, "srcdir"):
            srcdir = self.__spec.build.ctl_panel.srcdir
        if not os.path.isabs(os.path.expanduser(outdir)):
            outdir = os.path.join(os.getcwd(), outdir)
        if not os.path.isabs(os.path.expanduser(workdir)):
            workdir = os.path.join(os.getcwd(), workdir)
        if not os.path.isabs(os.path.expanduser(srcdir)):
            srcdir = os.path.join(os.getcwd(), srcdir)
        self.__outdir, self.__workdir, self.__srcdir = outdir, workdir, srcdir

    def lint(self):
        s = find_tpl_read("build.json", [self.__spec.var.schema_model_home])
        schema = json.loads(s)
        try:
            validate(self.__spec.to_dict(), schema)
        except Exception as e:
            logging.error("validate spec error: %s", e, extra=self.__mask)
            return False
        return True

    def gen(self):
        self.__gen_build_tree()
        gen_cmd = self.__gen_script_by_building_tree()
        if gen_cmd:
            self.__cmd = [[get_make(), "-I", self.__spec.var.make_home],
                          [get_make(), "-I", self.__spec.var.make_home, "install"]]

        cmds = []
        for _, cmd in enumerate(self.__cmd):
            cmds.append(" ".join(cmd))
        if len(cmds) > 0:
            logging.debug("run cmd:\n%s", " && ".join(cmds), extra=self.__mask)

    def run(self):
        workdir = self.__workdir

        for _, cmd in enumerate(self.__cmd):
            if run_cmd(cmd, workdir, self.__mask) != 0:
                sys.exit(1)

    def __gen_build_tree(self):
        self.__create_build_tree()
        return self.__building_tree

    def __gen_script_by_building_tree(self):
        var = self.__spec.var
        workdir = self.__workdir
        gen_script = False
        for sub_dir, section in self.__building_tree.items():
            script = self.__gen_script(sub_dir, section)
            path = os.path.join(workdir, sub_dir, var.makefile_name)
            logging.debug("gen script file: %s, script:\n%s", path, script, extra=self.__mask)
            force_write_file_all(path, script)
            gen_script = True
        return gen_script

    def __create_build_tree(self):
        build = self.__spec.build
        if hasattr(build, "proto") and build.proto is not None:
            self.__create_proto_build_tree(build.proto, "proto")
        if hasattr(build, "conf") and build.conf is not None:
            self.__create_conf_build_tree(build.conf, "conf")
        if hasattr(build, "sh") and build.sh is not None:
            self.__create_sh_build_tree(build.sh, "sh")
        if hasattr(build, "j2") and build.j2 is not None:
            self.__create_j2_build_tree(build.j2, "j2")
        if hasattr(build, "go") and build.go is not None:
            self.__create_go_build_tree(build.go, "go")
        if hasattr(build, "npm") and build.npm is not None:
            self.__create_npm_build_tree(build.npm, "npm")
        logging.debug("build tree:\n%s", pformat(self.__building_tree), extra=self.__mask)
        return

    def __create_sh_build_tree(self, sh, section):
        if isinstance(sh, list):
            for c in sh:
                if hasattr(c, "dir"):
                    dir_name = c.dir
                    if dir_name not in self.__building_tree:
                        self.__add_path_to_parent_subdirs(dir_name)
                        self.__building_tree[dir_name] = dict()
                    self.__building_tree[dir_name][section] = c
        return

    def __create_conf_build_tree(self, conf, section):
        if isinstance(conf, list):
            for c in conf:
                if hasattr(c, "dir"):
                    dir_name = c.dir
                    if dir_name not in self.__building_tree:
                        self.__add_path_to_parent_subdirs(dir_name)
                        self.__building_tree[dir_name] = dict()
                    self.__building_tree[dir_name][section] = c
        return

    def __create_j2_build_tree(self, conf, section):
        if isinstance(conf, list):
            for c in conf:
                if hasattr(c, "dir"):
                    dir_name = c.dir
                    if dir_name not in self.__building_tree:
                        self.__add_path_to_parent_subdirs(dir_name)
                        self.__building_tree[dir_name] = dict()
                    self.__building_tree[dir_name][section] = c
        return

    def __create_go_build_tree(self, go, section):
        if isinstance(go, list):
            for g in go:
                if hasattr(g, "dir"):
                    dir_name = g.dir
                    if dir_name not in self.__building_tree:
                        self.__add_path_to_parent_subdirs(dir_name)
                        self.__building_tree[dir_name] = dict()
                    self.__building_tree[dir_name][section] = g

    def __create_npm_build_tree(self, npm, section):
        if isinstance(npm, list):
            for n in npm:
                if hasattr(n, "dir"):
                    dir_name = n.dir
                    if dir_name not in self.__building_tree:
                        self.__add_path_to_parent_subdirs(dir_name)
                        self.__building_tree[dir_name] = dict()
                    self.__building_tree[dir_name][section] = n

    def __create_proto_build_tree(self, proto, section):
        if isinstance(proto, list):
            for p in proto:
                if hasattr(p, "dir"):
                    dir_name = p.dir
                    if dir_name not in self.__building_tree:
                        self.__add_path_to_parent_subdirs(dir_name)
                        self.__building_tree[dir_name] = dict()
                    self.__building_tree[dir_name][section] = p

    def __add_path_to_parent_subdirs(self, path):
        parent = os.path.dirname(path)
        subdir = os.path.basename(path)
        if subdir == "":
            return
        if parent not in self.__building_tree:
            self.__building_tree[parent] = dict()
        if "subdirs" not in self.__building_tree[parent]:
            self.__building_tree[parent]["subdirs"] = set()
        self.__building_tree[parent]["subdirs"].add(subdir)
        self.__add_path_to_parent_subdirs(parent)

    def __gen_script(self, sub_dir, section):
        outdir, workdir, srcdir = self.__outdir, self.__workdir, self.__srcdir
        sdir = os.path.realpath(os.path.expanduser(os.path.join(srcdir, sub_dir)))
        wdir = os.path.realpath(os.path.expanduser(os.path.join(workdir, sub_dir)))
        reldir = os.path.relpath(sdir, wdir)

        all_script = []
        script = self.__gen_script_install(reldir, outdir).strip('\n')
        all_script.append(script)
        if "conf" in section:
            script = self.__gen_script_conf(section["conf"]).strip('\n')
            all_script.append(script)
        if "sh" in section:
            script = self.__gen_script_sh(section["sh"]).strip('\n')
            all_script.append(script)
        if "j2" in section:
            script = self.__gen_script_j2(section["j2"]).strip('\n')
            all_script.append(script)
        if "proto" in section:
            script = self.__gen_script_proto(section["proto"]).strip('\n')
            all_script.append(script)
        if "go" in section:
            script = self.__gen_script_go(section["go"]).strip('\n')
            all_script.append(script)
        if "npm" in section:
            script = self.__gen_script_npm(section["npm"]).strip('\n')
            all_script.append(script)
        if "subdirs" in section:
            script = self.__gen_script_subdirs(DictObj({"subdirs": section["subdirs"]})).strip('\n')
            all_script.append(script)
        return "\n\n".join(all_script)

    def __gen_script_proto(self, proto_spec):
        mk = self.__spec.build.ctl_panel.proto.proto_mk
        dirs = self.__spec.build.ctl_panel.j2dir
        ext_dict = overwrite_dict(self.__spec.build.ctl_panel.proto.to_dict(), proto_spec.to_dict())
        return find_tpl_read_render(mk, dirs, ext_dict)

    def __gen_script_go(self, go_spec):
        mk = self.__spec.build.ctl_panel.go.go_mk
        dirs = self.__spec.build.ctl_panel.j2dir
        ext_dict = overwrite_dict(self.__spec.build.ctl_panel.go.to_dict(), go_spec.to_dict())
        return find_tpl_read_render(mk, dirs, ext_dict)

    def __gen_script_npm(self, npm_spec):
        mk = self.__spec.build.ctl_panel.npm.npm_mk
        dirs = self.__spec.build.ctl_panel.j2dir
        ext_dict = overwrite_dict(self.__spec.build.ctl_panel.npm.to_dict(), npm_spec.to_dict())
        return find_tpl_read_render(mk, dirs, ext_dict)

    def __gen_script_conf(self, conf_spec):
        mk = self.__spec.build.ctl_panel.conf.conf_mk
        dirs = self.__spec.build.ctl_panel.j2dir
        ext_dict = overwrite_dict(self.__spec.build.ctl_panel.conf.to_dict(), conf_spec.to_dict())
        return find_tpl_read_render(mk, dirs, ext_dict)

    def __gen_script_sh(self, sh_spec):
        mk = self.__spec.build.ctl_panel.sh.sh_mk
        dirs = self.__spec.build.ctl_panel.j2dir
        ext_dict = overwrite_dict(self.__spec.build.ctl_panel.sh.to_dict(), sh_spec.to_dict())
        return find_tpl_read_render(mk, dirs, ext_dict)

    def __gen_script_j2(self, j2_spec):
        mk = self.__spec.build.ctl_panel.j2.j2_mk
        dirs = self.__spec.build.ctl_panel.j2dir
        ext_dict = overwrite_dict(self.__spec.build.ctl_panel.j2.to_dict(), j2_spec.to_dict())
        return find_tpl_read_render(mk, dirs, ext_dict)

    def __gen_script_subdirs(self, subdirs_spec):
        mk = "subdirs.mk.j2"
        dirs = self.__spec.build.ctl_panel.j2dir
        return find_tpl_read_render(mk, dirs, subdirs_spec.to_dict())

    def __gen_script_install(self, src, dst):
        mk = "install.mk.j2"
        dst = os.path.expanduser(dst)
        dirs = self.__spec.build.ctl_panel.j2dir
        return find_tpl_read_render(mk, dirs, {"srcdir": src, "destdir": dst})


class SimplePacker:
    def __init__(self, spec):
        self.__spec = spec
        self.__cmd = []
        self.__tree = {}
        self.__mask = Var.mask_set(spec)
        self.__sub = [TarPacker(spec), ImagePacker(spec)]
        outdir, workdir, srcdir = self.__spec.pack.ctl_panel.outdir, self.__spec.pack.ctl_panel.workdir, \
                                  self.__spec.pack.ctl_panel.workdir  # srcdir default is workdir
        if hasattr(self.__spec.pack.ctl_panel, "srcdir"):
            srcdir = self.__spec.pack.ctl_panel.srcdir
        if not os.path.isabs(os.path.expanduser(outdir)):
            outdir = os.path.join(os.getcwd(), outdir)
        if not os.path.isabs(os.path.expanduser(workdir)):
            workdir = os.path.join(os.getcwd(), workdir)
        if not os.path.isabs(os.path.expanduser(srcdir)):
            srcdir = os.path.join(os.getcwd(), srcdir)
        self.__outdir, self.__workdir, self.__srcdir = outdir, workdir, srcdir

    def lint(self):
        s = find_tpl_read("pack.json", [self.__spec.var.schema_model_home])
        schema = json.loads(s)
        try:
            validate(self.__spec.to_dict(), schema)
        except Exception as e:
            logging.error("validate spec error: %s", e, extra=self.__mask)
            return False
        return True

    def gen(self):
        uk_cmd = set()
        for sub in self.__sub:
            for cmd in sub.cmd():
                cmd_str = cmd.__str__()
                if cmd_str not in uk_cmd:
                    self.__cmd.append(cmd)
                    uk_cmd.add(cmd_str)
            for k, v in sub.tree().items():
                if k in self.__tree:
                    self.__tree[k] = self.__tree[k] + "\n" + v
                else:
                    self.__tree[k] = v

        for k, v in self.__tree.items():
            force_write_file_all(k, v)
            logging.debug("gen file: %s\n%s", k, v, extra=self.__mask)

        cmds = []
        for _, cmd in enumerate(self.__cmd):
            cmds.append(" ".join(cmd))
        if len(cmds) > 0:
            logging.debug("run cmd: %s", " && ".join(cmds), extra=self.__mask)

    def run(self):
        workdir = self.__workdir

        for cmd in self.__cmd:
            if run_cmd(cmd, workdir, self.__mask) != 0:
                sys.exit(1)


class TarPacker:
    def __init__(self, spec):
        self.__spec = spec
        outdir, workdir, srcdir = self.__spec.pack.ctl_panel.outdir, self.__spec.pack.ctl_panel.workdir, \
                                  self.__spec.pack.ctl_panel.workdir  # srcdir default is workdir
        if hasattr(self.__spec.pack.ctl_panel, "srcdir"):
            srcdir = self.__spec.pack.ctl_panel.srcdir
        if not os.path.isabs(os.path.expanduser(outdir)):
            outdir = os.path.join(os.getcwd(), outdir)
        if not os.path.isabs(os.path.expanduser(workdir)):
            workdir = os.path.join(os.getcwd(), workdir)
        if not os.path.isabs(os.path.expanduser(srcdir)):
            srcdir = os.path.join(os.getcwd(), srcdir)
        self.__outdir, self.__workdir, self.__srcdir = outdir, workdir, srcdir

    def lint(self):
        pass

    def cmd(self):
        if not self.__has_section():
            return []
        return [[get_make(), "-I", self.__spec.var.make_home, "pack"],
                [get_make(), "-I", self.__spec.var.make_home, "install"]]

    def tree(self):
        if not self.__has_section():
            return {}
        workdir = self.__workdir
        var = self.__spec.var
        ext_spec = DictObj(overwrite_dict(self.__spec.pack.ctl_panel.tar.to_dict(), self.__spec.pack.tar.to_dict()))

        script_path = os.path.join(workdir, var.makefile_name)
        script = self.__gen_script(ext_spec)

        tarfile_path = os.path.join(workdir, ext_spec.tarfile_name)
        tarfile = self.__gen_tarfile(ext_spec)

        tarignore_path = os.path.join(workdir, ext_spec.tarignore_name)
        tarignore = self.__gen_tarignore(ext_spec)
        return {script_path: script, tarfile_path: tarfile, tarignore_path: tarignore}

    def __has_section(self):
        return hasattr(self.__spec, "pack") and hasattr(self.__spec.pack, "tar")

    def __gen_script(self, tar_spec):
        outdir, workdir, srcdir = self.__outdir, self.__workdir, self.__srcdir
        sdir = os.path.realpath(os.path.expanduser(srcdir))
        wdir = os.path.realpath(os.path.expanduser(workdir))
        reldir = os.path.relpath(sdir, wdir)

        all_script = []
        script = self.__gen_script_install(reldir, outdir).strip('\n')
        all_script.append(script)
        script = self.__gen_script_tar(tar_spec).strip('\n')
        all_script.append(script)
        return "\n\n".join(all_script)

    def __gen_script_tar(self, tar_spec):
        mk = self.__spec.pack.ctl_panel.tar.tar_pack_mk
        dirs = self.__spec.pack.ctl_panel.j2dir
        return find_tpl_read_render(mk, dirs, tar_spec.to_dict())

    def __gen_script_install(self, src, dst):
        mk = "install.mk.j2"
        dst = os.path.expanduser(dst)
        dirs = self.__spec.pack.ctl_panel.j2dir
        return find_tpl_read_render(mk, dirs, {"srcdir": src, "destdir": dst})

    def __gen_tarfile(self, tar_spec):
        tarfile = self.__spec.pack.ctl_panel.tar.tarfile
        dirs = self.__spec.pack.ctl_panel.j2dir
        return find_tpl_read_render(tarfile, dirs, tar_spec.to_dict())

    def __gen_tarignore(self, tar_spec):
        tarignore = self.__spec.pack.ctl_panel.tar.tarignore
        dirs = self.__spec.pack.ctl_panel.j2dir
        return find_tpl_read_render(tarignore, dirs, tar_spec.to_dict())


class ImagePacker:
    def __init__(self, spec):
        self.__spec = spec
        outdir, workdir, srcdir = self.__spec.pack.ctl_panel.outdir, self.__spec.pack.ctl_panel.workdir, \
                                  self.__spec.pack.ctl_panel.workdir  # srcdir default is workdir
        if hasattr(self.__spec.pack.ctl_panel, "srcdir"):
            srcdir = self.__spec.pack.ctl_panel.srcdir
        if not os.path.isabs(os.path.expanduser(outdir)):
            outdir = os.path.join(os.getcwd(), outdir)
        if not os.path.isabs(os.path.expanduser(workdir)):
            workdir = os.path.join(os.getcwd(), workdir)
        if not os.path.isabs(os.path.expanduser(srcdir)):
            srcdir = os.path.join(os.getcwd(), srcdir)
        self.__outdir, self.__workdir, self.__srcdir = outdir, workdir, srcdir

    def lint(self):
        pass

    def cmd(self):
        if not self.__has_section():
            return []
        return [[get_make(), "-I", self.__spec.var.make_home, "pack"],
                [get_make(), "-I", self.__spec.var.make_home, "install"]]

    def tree(self):
        if not self.__has_section():
            return {}
        var = self.__spec.var
        workdir = self.__workdir
        ext_spec = DictObj(overwrite_dict(self.__spec.pack.ctl_panel.image.to_dict(), self.__spec.pack.image.to_dict()))

        script_path = os.path.join(workdir, var.makefile_name)
        docker_path = os.path.join(workdir, ext_spec.dockerfile_name)
        ignore_path = os.path.join(workdir, ext_spec.dockerignore_name)

        script = self.__gen_script(ext_spec)
        dockerfile = self.__gen_dockerfile_image(ext_spec)
        dockerignorefile = self.__gen_dockerignore_image(ext_spec)

        return {script_path: script, docker_path: dockerfile, ignore_path: dockerignorefile}

    def __has_section(self):
        return hasattr(self.__spec, "pack") and hasattr(self.__spec.pack, "image")

    def __gen_script(self, image_spec):
        outdir, workdir, srcdir, = self.__outdir, self.__workdir, self.__srcdir
        sdir = os.path.realpath(os.path.expanduser(srcdir))
        wdir = os.path.realpath(os.path.expanduser(workdir))
        reldir = os.path.relpath(sdir, wdir)

        all_script = []
        script = self.__gen_script_install(reldir, outdir).strip('\n')
        all_script.append(script)
        script = self.__gen_script_image(image_spec).strip('\n')
        all_script.append(script)
        return "\n\n".join(all_script)

    def __gen_script_image(self, image_spec):
        mk = self.__spec.pack.ctl_panel.image.pack_mk
        dirs = self.__spec.pack.ctl_panel.j2dir
        return find_tpl_read_render(mk, dirs, image_spec.to_dict())

    def __gen_script_install(self, src, dst):
        mk = "install.mk.j2"
        dst = os.path.expanduser(dst)
        dirs = self.__spec.pack.ctl_panel.j2dir
        return find_tpl_read_render(mk, dirs, {"srcdir": src, "destdir": dst})

    def __gen_dockerfile_image(self, image_spec):
        dockerfile = self.__spec.pack.ctl_panel.image.dockerfile
        dirs = self.__spec.pack.ctl_panel.j2dir
        return find_tpl_read_render(dockerfile, dirs, image_spec.to_dict())

    def __gen_dockerignore_image(self, image_spec):
        dockerignore = self.__spec.pack.ctl_panel.image.dockerignore
        dirs = self.__spec.pack.ctl_panel.j2dir
        return find_tpl_read_render(dockerignore, dirs, image_spec.to_dict())


class SimplePusher:
    def __init__(self, spec):
        self.__spec = spec
        self.__cmd = []
        self.__mask = Var.mask_set(spec)
        outdir, workdir, srcdir = self.__spec.push.ctl_panel.outdir, self.__spec.push.ctl_panel.workdir, \
                                  self.__spec.push.ctl_panel.workdir  # srcdir default is workdir
        if hasattr(self.__spec.push.ctl_panel, "srcdir"):
            srcdir = self.__spec.push.ctl_panel.srcdir
        if not os.path.isabs(os.path.expanduser(outdir)):
            outdir = os.path.join(os.getcwd(), outdir)
        if not os.path.isabs(os.path.expanduser(workdir)):
            workdir = os.path.join(os.getcwd(), workdir)
        if not os.path.isabs(os.path.expanduser(srcdir)):
            srcdir = os.path.join(os.getcwd(), srcdir)
        self.__outdir, self.__workdir, self.__srcdir = outdir, workdir, srcdir

    def lint(self):
        s = find_tpl_read("push.json", [self.__spec.var.schema_model_home])
        schema = json.loads(s)
        try:
            validate(self.__spec.to_dict(), schema)
        except Exception as e:
            logging.error("validate spec error: %s", e, extra=self.__mask)
            return False
        return True

    def gen(self):
        gen_cmd = False
        if hasattr(self.__spec, "push") and hasattr(self.__spec.push, "image"):
            ext_spec = DictObj(
                overwrite_dict(self.__spec.push.ctl_panel.image.to_dict(), self.__spec.push.image.to_dict()))
            self.__gen_image(ext_spec)
            gen_cmd = True

        if gen_cmd:
            self.__cmd = [[get_make(), "-I", self.__spec.var.make_home, "push"]]

        cmds = []
        for _, cmd in enumerate(self.__cmd):
            cmds.append(" ".join(cmd))
        if len(cmds) > 0:
            logging.debug("run cmd:\n%s", " && ".join(cmds), extra=self.__mask)

    def run(self):
        workdir = self.__workdir

        for cmd in self.__cmd:
            if run_cmd(cmd, workdir, self.__mask) != 0:
                sys.exit(1)

    def __gen_image(self, image_spec):
        var = self.__spec.var
        outdir, workdir, srcdir = self.__outdir, self.__workdir, self.__srcdir
        sdir = os.path.realpath(os.path.expanduser(srcdir))
        wdir = os.path.realpath(os.path.expanduser(workdir))
        reldir = os.path.relpath(sdir, wdir)

        all_script = []
        script = self.__gen_script_install(reldir, outdir).strip('\n')
        all_script.append(script)

        script = self.__gen_script_image(image_spec).strip('\n')
        all_script.append(script)

        script = "\n\n".join(all_script)

        path = os.path.join(workdir, var.makefile_name)
        logging.debug("gen script file: %s, script:\n%s", path, script, extra=self.__mask)
        force_write_file_all(path, script)

    def __gen_script_image(self, image_spec):
        mk = self.__spec.push.ctl_panel.image.push_mk
        dirs = self.__spec.push.ctl_panel.j2dir
        return find_tpl_read_render(mk, dirs, image_spec.to_dict())

    def __gen_script_install(self, src, dst):
        mk = "install.mk.j2"
        dst = os.path.expanduser(dst)
        dirs = self.__spec.push.ctl_panel.j2dir
        return find_tpl_read_render(mk, dirs, {"srcdir": src, "destdir": dst})


class SimpleDeployer:
    def __init__(self, spec):
        self.__spec = spec
        self.__cmd = []
        self.__tree = {}
        self.__mask = Var.mask_set(spec)
        self.__sub = [DockerDeployer(spec), DockerDomposeDeployer(spec), KubectlDeployer(spec)]
        outdir, workdir, srcdir = self.__spec.deploy.ctl_panel.outdir, self.__spec.deploy.ctl_panel.workdir, \
                                  self.__spec.deploy.ctl_panel.workdir  # srcdir default is workdir
        if hasattr(self.__spec.deploy.ctl_panel, "srcdir"):
            srcdir = self.__spec.deploy.ctl_panel.srcdir
        if not os.path.isabs(os.path.expanduser(outdir)):
            outdir = os.path.join(os.getcwd(), outdir)
        if not os.path.isabs(os.path.expanduser(workdir)):
            workdir = os.path.join(os.getcwd(), workdir)
        if not os.path.isabs(os.path.expanduser(srcdir)):
            srcdir = os.path.join(os.getcwd(), srcdir)
        self.__outdir, self.__workdir, self.__srcdir = outdir, workdir, srcdir

    def lint(self):
        s = find_tpl_read("deploy.json", [self.__spec.var.schema_model_home])
        schema = json.loads(s)
        try:
            validate(self.__spec.to_dict(), schema)
        except Exception as e:
            logging.error("validate spec error: %s", e, extra=self.__mask)
            return False
        return True

    def gen(self):
        uk_cmd = set()
        for sub in self.__sub:
            for cmd in sub.cmd():
                cmd_str = cmd.__str__()
                if cmd_str not in uk_cmd:
                    self.__cmd.append(cmd)
                    uk_cmd.add(cmd_str)
            for k, v in sub.tree().items():
                if k in self.__tree:
                    self.__tree[k] = self.__tree[k] + "\n" + v
                else:
                    self.__tree[k] = v

        for k, v in self.__tree.items():
            force_write_file_all(k, v)
            logging.debug("gen file: %s\n%s", k, v, extra=self.__mask)

        cmds = []
        for _, cmd in enumerate(self.__cmd):
            cmds.append(" ".join(cmd))
        if len(cmds) > 0:
            logging.debug("run cmd: %s", " && ".join(cmds), extra=self.__mask)

    def run(self):
        workdir = self.__workdir

        for cmd in self.__cmd:
            if run_cmd(cmd, workdir, self.__mask) != 0:
                sys.exit(1)


class DockerDeployer:
    def __init__(self, spec):
        self.__spec = spec
        outdir, workdir, srcdir = self.__spec.deploy.ctl_panel.outdir, self.__spec.deploy.ctl_panel.workdir, \
                                  self.__spec.deploy.ctl_panel.workdir  # srcdir default is workdir
        if hasattr(self.__spec.deploy.ctl_panel, "srcdir"):
            srcdir = self.__spec.deploy.ctl_panel.srcdir
        if not os.path.isabs(os.path.expanduser(outdir)):
            outdir = os.path.join(os.getcwd(), outdir)
        if not os.path.isabs(os.path.expanduser(workdir)):
            workdir = os.path.join(os.getcwd(), workdir)
        if not os.path.isabs(os.path.expanduser(srcdir)):
            srcdir = os.path.join(os.getcwd(), srcdir)
        self.__outdir, self.__workdir, self.__srcdir = outdir, workdir, srcdir

    def lint(self):
        pass

    def cmd(self):
        if not self.__has_section():
            return []
        return [[get_make(), "-I", self.__spec.var.make_home, "deploy"]]

    def tree(self):
        if not self.__has_section():
            return {}
        ext_spec = DictObj(overwrite_dict(self.__spec.deploy.ctl_panel.docker.to_dict(),
                                          self.__spec.deploy.docker.to_dict()))
        p, s = self.__gen_docker(ext_spec)
        return {p: s}

    def __has_section(self):
        return hasattr(self.__spec, "deploy") and hasattr(self.__spec.deploy, "docker")

    def __gen_docker(self, docker_spec):
        var = self.__spec.var
        outdir, workdir, srcdir = self.__outdir, self.__workdir, self.__srcdir
        sdir = os.path.realpath(os.path.expanduser(srcdir))
        wdir = os.path.realpath(os.path.expanduser(workdir))
        reldir = os.path.relpath(sdir, wdir)

        all_script = []
        script = self.__gen_script_install(reldir, outdir).strip('\n')
        all_script.append(script)

        script = self.__gen_script_docker(docker_spec).strip('\n')
        all_script.append(script)

        path = os.path.join(workdir, var.makefile_name)
        return path, "\n\n".join(all_script)

    def __gen_script_docker(self, docker_spec):
        mk = self.__spec.deploy.ctl_panel.docker.deploy_mk
        dirs = self.__spec.deploy.ctl_panel.j2dir
        return find_tpl_read_render(mk, dirs, docker_spec.to_dict())

    def __gen_script_install(self, src, dst):
        mk = "install.mk.j2"
        dst = os.path.expanduser(dst)
        dirs = self.__spec.deploy.ctl_panel.j2dir
        return find_tpl_read_render(mk, dirs, {"srcdir": src, "destdir": dst})


class DockerDomposeDeployer:
    def __init__(self, spec):
        self.__spec = spec
        outdir, workdir, srcdir = self.__spec.deploy.ctl_panel.outdir, self.__spec.deploy.ctl_panel.workdir, \
                                  self.__spec.deploy.ctl_panel.workdir  # srcdir default is workdir
        if hasattr(self.__spec.deploy.ctl_panel, "srcdir"):
            srcdir = self.__spec.deploy.ctl_panel.srcdir
        if not os.path.isabs(os.path.expanduser(outdir)):
            outdir = os.path.join(os.getcwd(), outdir)
        if not os.path.isabs(os.path.expanduser(workdir)):
            workdir = os.path.join(os.getcwd(), workdir)
        if not os.path.isabs(os.path.expanduser(srcdir)):
            srcdir = os.path.join(os.getcwd(), srcdir)
        self.__outdir, self.__workdir, self.__srcdir = outdir, workdir, srcdir

    def lint(self):
        pass

    def cmd(self):
        if not self.__has_section():
            return []
        return [[get_make(), "-I", self.__spec.var.make_home, "deploy"]]

    def tree(self):
        if not self.__has_section():
            return {}
        ext_spec = DictObj(overwrite_dict(self.__spec.deploy.ctl_panel.docker_compose.to_dict(),
                                          self.__spec.deploy.docker_compose.to_dict()))
        return self.__gen_docker_compose(ext_spec)

    def __has_section(self):
        return hasattr(self.__spec, "deploy") and hasattr(self.__spec.deploy, "docker_compose")

    def __gen_docker_compose(self, docker_spec):
        var = self.__spec.var
        outdir, workdir, srcdir = self.__outdir, self.__workdir, self.__srcdir
        sdir = os.path.realpath(os.path.expanduser(srcdir))
        wdir = os.path.realpath(os.path.expanduser(workdir))
        reldir = os.path.relpath(sdir, wdir)

        compose = self.__gen_compose_file(docker_spec)
        path_compose = os.path.join(workdir, docker_spec.filename)

        all_script = []
        script = self.__gen_script_install(reldir, outdir).strip('\n')
        all_script.append(script)

        script = self.__gen_script_docker_compose(docker_spec).strip('\n')
        all_script.append(script)

        path_script = os.path.join(workdir, var.makefile_name)
        return {path_script: "\n\n".join(all_script), path_compose: compose}

    def __gen_script_docker_compose(self, docker_spec):
        mk = self.__spec.deploy.ctl_panel.docker_compose.deploy_mk
        dirs = self.__spec.deploy.ctl_panel.j2dir
        return find_tpl_read_render(mk, dirs, docker_spec.to_dict())

    def __gen_compose_file(self, docker_spec):
        mk = self.__spec.deploy.ctl_panel.docker_compose.deploy_yml
        dirs = self.__spec.deploy.ctl_panel.j2dir
        return find_tpl_read_render(mk, dirs, docker_spec.to_dict())

    def __gen_script_install(self, src, dst):
        mk = "install.mk.j2"
        dst = os.path.expanduser(dst)
        dirs = self.__spec.deploy.ctl_panel.j2dir
        return find_tpl_read_render(mk, dirs, {"srcdir": src, "destdir": dst})


class KubectlDeployer:
    def __init__(self, spec):
        self.__spec = spec
        outdir, workdir, srcdir = self.__spec.deploy.ctl_panel.outdir, self.__spec.deploy.ctl_panel.workdir, \
                                  self.__spec.deploy.ctl_panel.workdir  # srcdir default is workdir
        if hasattr(self.__spec.deploy.ctl_panel, "srcdir"):
            srcdir = self.__spec.deploy.ctl_panel.srcdir
        if not os.path.isabs(os.path.expanduser(outdir)):
            outdir = os.path.join(os.getcwd(), outdir)
        if not os.path.isabs(os.path.expanduser(workdir)):
            workdir = os.path.join(os.getcwd(), workdir)
        if not os.path.isabs(os.path.expanduser(srcdir)):
            srcdir = os.path.join(os.getcwd(), srcdir)
        self.__outdir, self.__workdir, self.__srcdir = outdir, workdir, srcdir

    def lint(self):
        pass

    def cmd(self):
        if not self.__has_section():
            return []
        return [[get_make(), "-I", self.__spec.var.make_home, "deploy"],
                [get_make(), "-I", self.__spec.var.make_home, "poll"]]

    def tree(self):
        if not self.__has_section():
            return {}
        ext_spec = DictObj(overwrite_dict(self.__spec.deploy.ctl_panel.k8s.to_dict(),
                                          self.__spec.deploy.k8s.to_dict()))
        return self.__gen_k8s(ext_spec)

    def __has_section(self):
        return hasattr(self.__spec, "deploy") and hasattr(self.__spec.deploy, "k8s")

    def __gen_k8s(self, k8s_spec):
        var = self.__spec.var
        outdir, workdir, srcdir = self.__outdir, self.__workdir, self.__srcdir
        sdir = os.path.realpath(os.path.expanduser(srcdir))
        wdir = os.path.realpath(os.path.expanduser(workdir))
        reldir = os.path.relpath(sdir, wdir)

        manifest = self.__gen_manifest_k8s(k8s_spec)
        path_mani = os.path.join(workdir, k8s_spec.filename)

        all_script = []
        script = self.__gen_script_install(reldir, outdir).strip('\n')
        all_script.append(script)

        script = self.__gen_script_k8s(k8s_spec).strip('\n')
        all_script.append(script)

        script = "\n\n".join(all_script)

        path_script = os.path.join(workdir, var.makefile_name)
        return {path_script: script, path_mani: manifest}

    def __gen_script_k8s(self, k8s_spec):
        mk = self.__spec.deploy.ctl_panel.k8s.kubectl_mk
        dirs = self.__spec.deploy.ctl_panel.j2dir
        return find_tpl_read_render(mk, dirs, k8s_spec.to_dict())

    def __gen_manifest_k8s(self, k8s_spec):
        yml = self.__spec.deploy.ctl_panel.k8s.deploy_yml
        dirs = self.__spec.deploy.ctl_panel.j2dir
        return find_tpl_read_render(yml, dirs, k8s_spec.to_dict())

    def __gen_script_install(self, src, dst):
        mk = "install.mk.j2"
        dst = os.path.expanduser(dst)
        dirs = self.__spec.deploy.ctl_panel.j2dir
        return find_tpl_read_render(mk, dirs, {"srcdir": src, "destdir": dst})


class SimplerSpecRender:
    timestamp_ms = round(time.time() * 1000)

    def __init__(self):
        self.__max_render_deepth = 100
        self.cicd_home = get_cicd_home()
        self.j2_home = os.path.join(self.cicd_home, 'j2.d')
        self.make_home = os.path.join(self.cicd_home, 'mk.d')
        self.schema_home = os.path.join(self.cicd_home, 'schema.d')
        return

    # render from vars section to all
    # return rendered dict
    def __render_final_cicd(self, render_dict):
        tmp_vars = dict()
        tmp_main = dict()
        if "var" in render_dict:
            tmp_vars = render_dict["var"]
            if not isinstance(tmp_vars, dict):
                logging.error("cicd file section vars must be json object")
                sys.exit(1)
            tmp_main = render_dict.copy()
            tmp_main.pop("var", None)

        vars = self.__loop_render(tmp_vars, tmp_vars)
        main = self.__loop_render(tmp_main, vars)

        main["var"] = vars
        return main

    def __loop_render(self, dst, src):
        s = json.dumps(dst)
        ret = dict()
        i = 0
        while i < self.__max_render_deepth:
            i += 1
            tpl = jinja2.Template(s)
            tmp_str = tpl.render(src)
            if s == tmp_str:
                ret = json.loads(tmp_str)
                break
            s = tmp_str
        if i >= self.__max_render_deepth:
            logging.error("render reach max depth %d, please check loop reference.", self.__max_render_deepth)
            sys.exit(1)

        return ret

    def __load_cicd_file(self, cicd_file):
        cicd_dict = dict()
        file = os.path.expanduser(cicd_file)
        if os.path.isfile(file):
            with open(file) as f:
                try:
                    cicd_dict = json.load(f)
                except Exception as e:
                    logging.error("response is not a json file: %s, %s", cicd_file, e)
                    sys.exit(errno.EPERM)
        else:
            logging.error("no such file or directory: %s", cicd_file)
            sys.exit(errno.ENOENT)

        return cicd_dict

    def __load_cicd_http(self, cicd_http):
        ssl._create_default_https_context = ssl._create_unverified_context
        cicd_dict = dict()
        u = urllib.parse.urlparse(cicd_http)
        username = u.username
        password = u.password
        if username is not None or password is not None:
            auth = u.netloc.split("@", 1)
            n = urllib.parse.ParseResult(u.scheme, auth[1], u.path, u.params, u.query, u.fragment)
            cicd_http = urllib.parse.urlunparse(n)
        content = ""
        try:
            req = urllib.request.Request(cicd_http)
            if username is not None or password is not None:
                base64string = base64.b64encode(bytes('{}:{}'.format(username, password), 'ascii'))
                req.add_header('Authorization', 'Basic {}'.format(base64string.decode('utf-8')))

            resp = urllib.request.urlopen(req, timeout=3)
            content = resp.read()
        except Exception as e:
            logging.error("http get error: %s, %s", cicd_http, e)
            sys.exit(errno.EPERM)
        try:
            if len(content) > 0:
                c = content.decode('utf-8')
                cicd_dict = json.loads(c)
        except Exception as e:
            logging.error("response is not a json file: %s, %s", cicd_http, e)
            sys.exit(errno.EPERM)
        return cicd_dict

    def __multiply_runtime_loop(self, prev, name, curr):
        ret = list()
        for _, c in enumerate(curr):
            if len(prev) == 0:
                tmp = {name: c}
                ret.append(tmp)
                continue
            for _, p in enumerate(prev):
                newp = copy.deepcopy(p)
                newp[name] = c
                ret.append(newp)
        return ret

    def __runtime_loop(self, curr):
        ret = list()
        for name, val in curr.items():
            # https://stackoverflow.com/questions/9943504/right-to-left-string-replace-in-python
            ret = self.__multiply_runtime_loop(ret, name[::-1].replace("s", "", 1)[::-1], val)
        return ret

    def runtime_loop(self, uspec, stage_name):
        loop_dimension = []
        if "loop" in uspec[stage_name]["ctl_panel"]:
            loop_dimension = uspec[stage_name]["ctl_panel"]["loop"]
        loop_vector = dict()
        for _, dimension in enumerate(loop_dimension):
            loop_vector[dimension] = uspec["var"][dimension]
        rt_loop = self.__runtime_loop(loop_vector)
        return rt_loop

    def overwrite_runtime(self, uspec, rt):
        # render
        overwrite_dic = [{"runtime": {"var": rt}}]
        spec_dict = uspec
        for dic in overwrite_dic:
            for name, a_dic in dic.items():
                spec_dict = overwrite_dict(spec_dict, a_dic)
                logging.debug("merged %s: %s", name, json.dumps(spec_dict, sort_keys=True),
                              extra=Var.mask_set(DictObj(self.__render_final_cicd(spec_dict))))

        return spec_dict

    def overwrite_base(self, args):
        # cicd file
        cicd_list = []
        for name in args.cicd_file:
            if is_http_url(name):
                cicd_list.append(self.__load_cicd_http(name))
                continue
            cicd_list.append(self.__load_cicd_file(name))

        # sys spec
        sys_dict = self.__sys_dict(args)

        # preset spec
        preset_dict = self.__load_preset_file(args.model)

        # args spec
        args_dict = self.__args_dict(args)

        # defined spec
        define_dict = self.__define_dict(args.define_var)

        # render
        overwrite_name = ["sys", "preset", "cicd", "args", "define"]
        overwrite_dic = [sys_dict, preset_dict, cicd_list, args_dict, define_dict]
        spec_dict = dict()
        for i, name in enumerate(overwrite_name):
            a_dict = overwrite_dic[i]
            if isinstance(a_dict, list):
                for j, cname in enumerate(args.cicd_file):
                    spec_dict = overwrite_dict(spec_dict, cicd_list[j])
                    logging.debug("merged %s %s: %s", name, cname, json.dumps(spec_dict, sort_keys=True),
                                  extra=Var.mask_set(DictObj(self.__render_final_cicd(spec_dict))))
                continue

            spec_dict = overwrite_dict(spec_dict, a_dict)
            logging.debug("merged %s: %s", name, json.dumps(spec_dict, sort_keys=True),
                          extra=Var.mask_set(DictObj(self.__render_final_cicd(spec_dict))))

        return spec_dict

    def render(self, uspec_dict):
        rendered_spec = self.__render_final_cicd(uspec_dict)

        spec = DictObj(rendered_spec)
        logging.info("spec: %s", json.dumps(spec.to_dict(), sort_keys=True), extra=Var.mask_set(spec))
        return spec

    def __load_preset_file(self, model):
        preset_dict = dict()
        model_path = os.path.join(self.j2_home, model)
        preset_file = find_jj_read('preset.json.j2', [model_path])
        if len(preset_file) != 0:
            preset_dict = json.loads(preset_file)
        else:
            logging.debug("preset file no found")

        return preset_dict

    def __define_dict(self, define_var):
        define_dict = dict()
        if define_var != "":
            define_dict = {
                "var": json.loads(define_var)
            }
        return define_dict

    def __args_dict(self, args):
        ret = {
            "var": {
                "model": args.model,
                "cicd_file": args.cicd_file,
                "stages": args.stages,
                "dry_run": args.dry_run,
                "j2_model_home": os.path.join(self.j2_home, "{{model}}"),
                "schema_model_home": os.path.join(self.schema_home, "{{model}}"),
            },
        }
        return ret

    def __sys_dict(self, args):
        stages = SimpleCicd().select_stage(args.stages)

        ret = {
            "var": {
                "timestamp_ms": SimplerSpecRender.timestamp_ms,
                "makefile_name": "makefile",
                "os_sep": os.sep,
                "cicd_home": self.cicd_home,
                "j2_home": self.j2_home,
                "make_home": self.make_home,
                "schema_home": self.schema_home,
                "run_stages": stages,
                "version": _version.__version__,
            }
        }
        return ret


class SimpleCicd:
    @staticmethod
    def model():
        return 'simple'

    @staticmethod
    def add_argument(sub_parser):
        # sub_parser = subparser.add_parser(SimpleCicd.model())
        sub_parser.add_argument("--dry-run", dest="dry_run", help="only generate script, don't run",
                                action='store_true')
        sub_parser.add_argument("-f", "--cicd-file", dest="cicd_file", help="cicd spec file", required=True,
                                default=[], action="append")
        sub_parser.add_argument("-d", "--define-var", dest="define_var",
                                help="define runtime json var, wrap it with single quotes", default="")
        sub_parser.add_argument("-s", "--stages", dest="stages", help="cicd stages", default=[], action="append")
        sub_parser.add_argument("-v", "--verbose", dest="verbose", help="verbose print", action='store_true')

    def __normalize_cmdline(self, args):
        args.stages = self.__split_comma_in_list(args.stages)
        args.cicd_file = self.__split_comma_in_list(args.cicd_file)

    def __terminal_width(self):
        try:
            c, _ = os.get_terminal_size()
            return c
        finally:
            return 40

    def __init__(self):
        self.__args = None
        self.__spec = None
        return

    def do_cicd(self, args):
        set_log_verbose(args.verbose)
        set_log_mask(True)
        self.__normalize_cmdline(args)

        self.__args = args

        stage_list_map = self.__select_stage(self.__args.stages)
        for a_index, a_stage in enumerate(stage_list_map):
            for stage_name, stage_fn in a_stage.items():
                logging.info(stage_name.center(self.__terminal_width(), "="))
                stage_fn(stage_name)

        return 0

    def __stage_spec(self):
        return [
            {"init": self.__init_stage},
            {"build": self.__build_stage},
            {"pack": self.__pack_stage},
            {"push": self.__push_stage},
            {"deploy": self.__deploy_stage},
            {"done": self.__done_stage}
        ]

    def select_stage(self, stages):
        ret = []
        stage_list_map = self.__select_stage(stages)
        for _, a_stage in enumerate(stage_list_map):
            for stage_name, _ in a_stage.items():
                ret.append(stage_name)
        return ret

    def __select_stage(self, stages):
        stage_spec = self.__stage_spec()
        if len(stages) == 0:
            return stage_spec

        input_stages_set = set(stages)
        # select stages
        ret_stage = []
        if len(input_stages_set) != 0:
            for x in stage_spec:
                for key, fun in x.items():
                    if key in input_stages_set:
                        ret_stage.append(x)
        else:
            ret_stage = stage_spec

        return ret_stage

    @staticmethod
    def __select_stage_pretty_print(meta):
        exec_stage = []
        for stage in meta:
            for key, fn in stage.items():
                exec_stage.append(key)
        return exec_stage

    def __init_stage(self, stage_name):
        s = SimplerSpecRender()
        uspec = s.overwrite_base(self.__args)
        rt_loop = s.runtime_loop(uspec, stage_name)
        for idx, rt_ctx in enumerate(rt_loop):
            c = self.__terminal_width()
            logging.info("{0}[{1}/{2}]".format(stage_name, idx + 1, len(rt_loop)).center(c, "-"))
            uspec_rt = s.overwrite_runtime(copy.deepcopy(uspec), rt_ctx)
            spec = s.render(uspec_rt)
            init_executor = SimpleInit(spec)
            if not init_executor.lint():
                sys.exit(1)
            init_executor.gen()
            if not spec.var.dry_run:
                init_executor.run()
        return

    def __build_stage(self, stage_name):
        s = SimplerSpecRender()
        uspec = s.overwrite_base(self.__args)
        rt_loop = s.runtime_loop(uspec, stage_name)
        for idx, rt_ctx in enumerate(rt_loop):
            c = self.__terminal_width()
            logging.info("{0}[{1}/{2}]".format(stage_name, idx + 1, len(rt_loop)).center(c, "-"))
            uspec_rt = s.overwrite_runtime(copy.deepcopy(uspec), rt_ctx)
            spec = s.render(uspec_rt)
            build_executor = SimpleBuildTree(spec)
            if not build_executor.lint():
                sys.exit(1)
            build_executor.gen()
            if not spec.var.dry_run:
                build_executor.run()
        return

    def __pack_stage(self, stage_name):
        s = SimplerSpecRender()
        uspec = s.overwrite_base(self.__args)
        rt_loop = s.runtime_loop(uspec, stage_name)
        for idx, rt_ctx in enumerate(rt_loop):
            c = self.__terminal_width()
            logging.info("{0}[{1}/{2}]".format(stage_name, idx + 1, len(rt_loop)).center(c, "-"))
            uspec_rt = s.overwrite_runtime(copy.deepcopy(uspec), rt_ctx)
            spec = s.render(uspec_rt)

            pack_executor = SimplePacker(spec)
            if not pack_executor.lint():
                sys.exit(1)
            pack_executor.gen()
            if not spec.var.dry_run:
                pack_executor.run()
        return

    def __push_stage(self, stage_name):
        s = SimplerSpecRender()
        uspec = s.overwrite_base(self.__args)
        rt_loop = s.runtime_loop(uspec, stage_name)
        for idx, rt_ctx in enumerate(rt_loop):
            c = self.__terminal_width()
            logging.info("{0}[{1}/{2}]".format(stage_name, idx + 1, len(rt_loop)).center(c, "-"))
            uspec_rt = s.overwrite_runtime(copy.deepcopy(uspec), rt_ctx)
            spec = s.render(uspec_rt)

            push_executor = SimplePusher(spec)
            if not push_executor.lint():
                sys.exit(1)
            push_executor.gen()
            if not spec.var.dry_run:
                push_executor.run()
        return

    def __deploy_stage(self, stage_name):
        s = SimplerSpecRender()
        uspec = s.overwrite_base(self.__args)
        rt_loop = s.runtime_loop(uspec, stage_name)
        thread_list = []
        for idx, rt_ctx in enumerate(rt_loop):
            c = self.__terminal_width()
            logging.info("{0}[{1}/{2}]".format(stage_name, idx + 1, len(rt_loop)).center(c, "-"))
            uspec_rt = s.overwrite_runtime(copy.deepcopy(uspec), rt_ctx)
            spec = s.render(uspec_rt)

            deploy_executor = SimpleDeployer(spec)
            if not deploy_executor.lint():
                sys.exit(1)
            deploy_executor.gen()
            if not spec.var.dry_run:
                t = threading.Thread(target=deploy_executor.run)
                t.start()
                thread_list.append(t)
        for t in thread_list:
            t.join()
        return

    def __done_stage(self, stage_name):
        return

    @staticmethod
    def __split_comma_in_list(ls):
        tmp, ret, st = [], [], set()
        if isinstance(ls, list):
            for x in ls:
                tmp.extend(x.split(","))
        for i, x in enumerate(tmp):
            s = x.strip()
            if s == "" or s in st:
                continue
            ret.append(s)
            st.add(s)

        return ret

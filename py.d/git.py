# -*- coding: utf-8 -*-
import logging
import subprocess
import sys
import os
import re


def is_git_dirty(githome):
    cmd = ["git", "diff-index", "--quiet", "HEAD"]
    logging.info("$ %s", " ".join(cmd))
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=githome)
    p.wait()
    if p.returncode != 0:
        logging.info(p.stderr.read().decode("utf-8").strip())
    return p.returncode


def is_git_dir(githome):
    cmd = ["git", "rev-parse", "--is-inside-work-tree"]
    logging.info("$ %s", " ".join(cmd))
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=githome)
    p.wait()
    if p.returncode != 0:
        logging.warning(p.stderr.read().decode("utf-8").strip())
    return p.returncode


def is_exist_git_local_branch(githome, branch):
    prefix_heads = "refs/heads/origin/"
    if not branch.startswith(prefix_heads):
        branch = prefix_heads + branch
    cmd = ["git", "show-ref", "--verify", "--quiet", branch]
    logging.info("$ %s", " ".join(cmd))
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=githome)
    p.wait()
    if p.returncode != 0:
        logging.warning(p.stderr.read().decode("utf-8").strip())
    else:
        logging.debug(p.stdout.read().decode("utf-8").strip())
    return p.returncode


def get_git_branch_commit_id(githome, branch):
    prefix_remote = "refs/remotes/origin/"
    prefix_heads = "refs/heads/"
    if branch.startswith(prefix_heads):
        branch = branch[len(prefix_heads):]
    if not branch.startswith(prefix_remote):
        branch = prefix_remote + branch
    branch += "^{commit}"
    cmd = ["git", "rev-parse", branch]
    logging.info("$ %s", " ".join(cmd))
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=githome)
    p.wait(10)
    if p.returncode != 0:
        logging.warning(p.stderr.read().decode("utf-8").strip())
        return ""
    else:
        return p.stdout.read().decode("utf-8").strip()


def get_git_current_branch(githome):
    cmd = ["git", "branch", "--show-current"]
    logging.info("$ %s", " ".join(cmd))
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=githome)
    p.wait()
    if p.returncode != 0:
        logging.warning(p.stderr.read().decode("utf-8").strip())
        return ""
    else:
        return p.stdout.read().decode("utf-8").strip()


def get_git_remote_origin_url(githome):
    cmd = ["git", "config", "remote.origin.url"]
    logging.info("$ %s", " ".join(cmd))
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=githome)
    p.wait()
    if p.returncode != 0:
        logging.warning(p.stderr.read().decode("utf-8").strip())
        return ""
    else:
        return p.stdout.read().decode("utf-8").strip()


def set_git_remote_origin_url(repo, githome):
    cmd = ["git", "config", "remote.origin.url", repo]
    logging.info("$ %s", " ".join(cmd))
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=githome)
    p.wait()
    if p.returncode != 0:
        logging.warning(p.stderr.read().decode("utf-8").strip())
    else:
        logging.debug(p.stdout.read().decode("utf-8").strip())
    return p.returncode


def get_git_last_commit_message(githome):
    cmd = ["git", "log" "-1", "--format=%ae", "HEAD"]
    logging.info("$ %s", " ".join(cmd))
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=githome)
    p.wait()
    if p.returncode != 0:
        logging.error(p.stderr.read().decode("utf-8").strip())
        return ""
    else:
        return p.stdout.read().decode("utf-8").strip()


def clear_stash_git_dir(githome):
    cmd = ["git", "stash", "clear"]
    logging.info("$ %s", " ".join(cmd))
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=githome)
    p.wait()
    if p.returncode != 0:
        logging.error(p.stderr.read().decode("utf-8").strip())
    else:
        logging.debug(p.stdout.read().decode("utf-8").strip())
    return p.returncode


def stash_git_dir(githome):
    cmd = ["git", "stash", "--include-untracked"]
    logging.info("$ %s", " ".join(cmd))
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=githome)
    p.wait()
    if p.returncode != 0:
        logging.error(p.stderr.read().decode("utf-8").strip())
    else:
        logging.debug(p.stdout.read().decode("utf-8").strip())
    return p.returncode


def force_clean_git_dir(githome):
    cmd = ["git", "clean", "-d", "-f"]
    logging.info("$ %s", " ".join(cmd))
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=githome)
    p.wait()
    if p.returncode != 0:
        logging.error(p.stderr.read().decode("utf-8").strip())
    else:
        logging.debug(p.stdout.read().decode("utf-8").strip())
    return p.returncode


def fetch_git_dir(githome):
    cmd = ["git", "fetch", "--tags", "--progress", "--prune"]
    logging.info("$ %s", " ".join(cmd))
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=githome)
    p.wait()
    if p.returncode != 0:
        logging.error(p.stderr.read().decode("utf-8").strip())
    else:
        logging.debug(p.stdout.read().decode("utf-8").strip())
    return p.returncode


def clone_git_dir(githome, repo):
    cmd = ["git", "clone", repo, "."]
    logging.info("$ %s", " ".join(cmd))
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=githome)
    p.wait()
    if p.returncode != 0:
        logging.error(p.stderr.read().decode("utf-8").strip())
    else:
        logging.debug(p.stdout.read().decode("utf-8").strip())
    return p.returncode


def force_delete_local_git_branch(githome, branch):
    prefix = "origin/"
    if not branch.startswith(prefix):
        branch = prefix + branch
    cmd = ["git", "branch", "-D", branch]
    logging.info("$ %s", " ".join(cmd))
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=githome)
    p.wait()
    if p.returncode != 0:
        logging.error(p.stderr.read().decode("utf-8").strip())
    else:
        logging.debug(p.stdout.read().decode("utf-8").strip())
    return p.returncode


def checkout_git_branch_with_commit_id(githome, branch, commit_id):
    prefix = "origin/"
    if not branch.startswith(prefix):
        branch = prefix + branch
    cmd = ["git", "checkout", "-b", branch, commit_id]
    logging.info("$ %s", " ".join(cmd))
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=githome)
    p.wait()
    if p.returncode != 0:
        logging.error(p.stderr.read().decode("utf-8").strip())
    else:
        logging.debug(p.stdout.read().decode("utf-8").strip())
    return p.returncode


def checkout_git_branch(githome, branch="master"):
    cmd = ["git", "checkout", "-f", branch]
    logging.info("$ %s", " ".join(cmd))
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=githome)
    p.wait()
    if p.returncode != 0:
        logging.error(p.stderr.read().decode("utf-8").strip())
    else:
        logging.debug(p.stdout.read().decode("utf-8").strip())
    return p.returncode


def get_git_latest_repo(git_dir, git_repo, git_branch):
    logging.info("get latest git repo: %s, branch: %s, dir: %s", git_repo, git_branch, git_dir)
    os.makedirs(git_dir, exist_ok=True)
    if is_git_dir(git_dir) == 0:
        if clear_stash_git_dir(git_dir) != 0:
            logging.error("git stash clear err: %s", git_dir)
            sys.exit(1)
        if stash_git_dir(git_dir) != 0:
            logging.error("git stash err: %s", git_dir)
            sys.exit(1)
        if force_clean_git_dir(git_dir) != 0:
            logging.error("git force clean err: %s", git_dir)
            sys.exit(1)
    else:
        if len(os.listdir(git_dir)) == 0:
            if clone_git_dir(git_dir, git_repo) != 0:
                logging.error("git clone err: %s", git_dir)
                sys.exit(1)
        else:
            logging.error("git dir isn't empty: %s, cannot clone git repo", git_dir)
            sys.exit(1)

    if fetch_git_dir(git_dir) != 0:
        logging.error("get fetch err, git dir: %s, branch: %s", git_dir, git_branch)
        sys.exit(1)
    commit_id = get_git_branch_commit_id(git_dir, git_branch)
    if commit_id == "":
        logging.error("get git branch commit id err, git dir: %s, branch: %s", git_dir, git_branch)
        sys.exit(1)

    if checkout_git_branch(git_dir, commit_id) != 0:
        logging.error("get force delete err, git dir: %s, branch: %s, commit id: %s", git_dir, git_branch, commit_id)
        sys.exit(1)

    if is_exist_git_local_branch(git_dir, git_branch) == 0:
        if force_delete_local_git_branch(git_dir, git_branch) != 0:
            logging.error("get force delete new branch err, git dir: %s, branch: %s, commit id: %s", git_dir,
                          git_branch, commit_id)
            sys.exit(1)

    if checkout_git_branch_with_commit_id(git_dir, git_branch, commit_id) != 0:
        logging.error("get checkout new branch err, git dir: %s, branch: %s, commit id: %s", git_dir, git_branch,
                      commit_id)
        sys.exit(1)

    get_git_repo = get_git_remote_origin_url(git_dir)
    get_git_branch = get_git_current_branch(git_dir)
    logging.debug("got latest git repo done: %s, branch: %s", get_git_repo, get_git_branch)


def is_git_url(url):
    reg1 = re.match(r'(\w+://)(.+@)*([\w\d\.]+)(:[\d]+){0,1}/*(.*)', url)
    if reg1 is not None:
        return True
    reg2 = re.match(r'file://(.*)', url)
    if reg2 is not None:
        return True
    reg3 = re.match(r'(.+@)*([\w\d\.]+):(.*)', url)
    if reg3 is not None:
        return True
    return False

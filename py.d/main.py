# -*- coding: utf-8 -*-

import argparse
from simple import *
from _version import __version__


def main():
    logging.basicConfig(format="%(tplus)s [%(levelname)s] %(message)s", stream=sys.stdout, level=logging.INFO)
    logging.setLogRecordFactory(TPlus.do)
    sys.excepthook = set_except_hook
    ret_code = exec_cicd()
    logging.info("execution time: %.03f s", time.time() - TPlus.start)
    return ret_code


def add_argument(top_parser):
    top_parser.add_argument("--version", dest="version", help="print version", action='store_true')


def do_cmdline(args):
    if args.version:
        print(__version__)
        sys.exit(0)


def exec_cicd():
    switcher = {
        SimpleCicd.model(): SimpleCicd(),
    }

    top_parser = argparse.ArgumentParser()
    add_argument(top_parser)
    subparsers = top_parser.add_subparsers(title="model", dest="model")
    for name, cicd in switcher.items():
        sub_parser = subparsers.add_parser(name)
        cicd.add_argument(sub_parser)

    args = top_parser.parse_args()
    do_cmdline(args)

    cicd = switcher.get(args.model)
    if cicd is None:
        logging.error("not found model: %s", args.model)
        return 1
    return cicd.do_cicd(args)

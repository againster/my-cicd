# -*- coding: utf-8 -*-
import ctypes
import ctypes.util
import selectors
import subprocess
import logging
import sys
import os
import time
import json
import jinja2
import re


def read_file_all(path_str):
    with open(path_str, 'r') as rfile:
        c = rfile.read()
        rfile.close()
    return c


def write_file_all(path_str, str):
    with open(path_str, 'w') as wfile:
        wfile.write(str)
        wfile.close()


def force_write_file_all(path_str, str):
    path_str = os.path.expanduser(path_str)
    dir = os.path.dirname(path_str)
    if dir != "" and not os.path.isdir(dir):
        os.makedirs(dir, exist_ok=True)
    with open(path_str, 'w') as wfile:
        wfile.write(str)
        wfile.close()


def append_file_all(path_str, str):
    with open(path_str, 'a') as wfile:
        wfile.write(str)
        wfile.close()


def run_cmd_no_mask(cmd, cwd):
    logging.debug("exec cmd: cd %s && %s", cwd, " ".join(cmd))
    p = subprocess.Popen(cmd, stdout=sys.stdout, stderr=sys.stderr, cwd=os.path.expanduser(cwd))
    p.wait()
    return p.returncode


def run_cmd(cmd, cwd, mask=None):
    cwd_expend = os.path.expanduser(cwd)
    if mask is None or "mask" not in mask:
        return run_cmd_no_mask(cmd, cwd)
    logging.debug("exec cmd with mask: cd %s && %s", cwd, " ".join(cmd), extra=mask)
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True, cwd=cwd_expend)
    sel = selectors.DefaultSelector()
    sel.register(p.stdout, selectors.EVENT_READ)
    sel.register(p.stderr, selectors.EVENT_READ)

    ok, masked = True, "[MASKED]"
    while ok:
        for key, _ in sel.select():
            data = key.fileobj.readline()
            if not data:
                ok = False
                break
            for m in mask["mask"]:
                data = data.replace(m, masked)
            if key.fileobj is p.stdout:
                # print(data, end="", file=sys.stdout)
                logging.info(data.rstrip())
            else:
                # print(data, end="", file=sys.stderr)
                logging.warning(data.rstrip())

    p.wait()
    return p.returncode


# dict dst overwrite src
def overwrite_list(src, dst):
    ret = src.copy()
    for i, v in enumerate(dst):
        srcv = None
        if i < len(src):
            srcv = src[i]
        if isinstance(v, dict) and isinstance(srcv, dict):
            ret[i] = overwrite_dict(srcv, v)
        elif isinstance(v, list) and isinstance(srcv, list):
            ret[i] = overwrite_list(srcv, v)
        else:
            if i >= len(src):
                ret.append(v)
            else:
                ret[i] = v
    return ret


# dict dst overwrite src
def overwrite_dict(src, dst):
    ret = src.copy()
    for k, v in dst.items():
        if k not in src:
            ret[k] = dst[k]
            continue
        srcv = src[k]
        if isinstance(v, dict) and isinstance(srcv, dict):
            ret[k] = overwrite_dict(srcv, v)
        elif isinstance(v, list) and isinstance(srcv, list):
            ret[k] = overwrite_list(srcv, v)
        else:
            ret[k] = v
    return ret


class DictObj:
    def __init__(self, in_dict: dict):
        assert isinstance(in_dict, dict)
        for key, val in in_dict.items():
            if isinstance(val, (list, tuple)):
                setattr(self, key, [DictObj(x) if isinstance(x, dict) else x for x in val])
            else:
                setattr(self, key, DictObj(val) if isinstance(val, dict) else val)

    def to_json(self):
        return json.dumps(self, default=lambda o: o.__dict__)

    def to_dict(self):
        ret = dict()
        for key, val in self.__dict__.items():
            if isinstance(val, self.__class__):
                ret[key] = val.to_dict()
            elif isinstance(val, (list, tuple)):
                ret[key] = [x.to_dict() if isinstance(x, self.__class__) else x for x in val]
            else:
                ret[key] = val
        return ret

    def __str__(self):
        return self.__repr__()

    def __repr__(self):
        return self.to_json()


# define T+ time format. usage: %(tplus)s in logging formatter
# https://stackoverflow.com/questions/17558552/how-do-i-add-custom-field-to-python-log-format-string
class TPlus:
    start = time.time()
    rf = logging.getLogRecordFactory()

    @staticmethod
    def do(*args, **kwargs):
        end = time.time()
        elapse = round((end - TPlus.start) * 1000)
        ms = int(elapse % 1000)
        ss = int(elapse / 1000 % 60)
        mm = int(elapse / 60000 % 60)
        hh = int(elapse / 3600000)
        record = TPlus.rf(*args, **kwargs)
        record.tplus = "T%+d:%02d:%02d.%03d" % (hh, mm, ss, ms)
        return record


def set_except_hook(exception_type, exception, traceback):
    # https://stackoverflow.com/questions/27674602/hide-traceback-unless-a-debug-flag-is-set
    logging.error("%s: %s" % (exception_type.__name__, exception))


def set_log_verbose(verbose):
    if verbose is True:
        log = logging.getLogger()
        log.setLevel(logging.DEBUG)
        handler = log.handlers[0]
        log_format_detail = logging.Formatter("%(tplus)s [%(levelname)s] %(funcName)s:%(lineno)d %(message)s")
        handler.setFormatter(log_format_detail)


def set_log_mask(mask):
    if mask is False:
        return

    class MaskFormatter(logging.Formatter):
        def format(self, record):
            res = super(MaskFormatter, self).format(record)

            if hasattr(record, 'mask'):
                for v in record.mask:
                    res = res.replace(v, "[MASKED]")
            return res

    log = logging.getLogger()
    for fn in log.root.handlers:
        # This is lazy and does only the minimum alteration necessary. It'd be better to use
        # dictConfig / fileConfig to specify the full desired configuration from the start:
        # http://docs.python.org/2/library/logging.config.html#dictionary-schema-details
        fn.setFormatter(MaskFormatter(fn.formatter._fmt))


def find_file(file, find_dir_list):
    if file != "" and not os.path.isabs(file):
        for d in find_dir_list:
            g_file = os.path.join(d, file)
            if os.path.isfile(os.path.expanduser(g_file)):
                return g_file, True
    return file, False


def load_file_from_dirs(file, dirs):
    path, ok = find_file(file, dirs)
    if not ok:
        return "", False

    c = read_file_all(path)
    return c, True


def load_file_from_so(file, so):
    # https://stackoverflow.com/questions/31148387/which-paths-does-python-ctypes-module-search-for-libraries-on-mac-os
    # https://docs.python.org/3/library/ctypes.html#finding-shared-libraries
    # https://stackoverflow.com/questions/35682600/get-absolute-path-of-shared-library-in-python
    # sop = ctypes.util.find_library(so)
    # if sop is None:
    #    logging.error("no such library file: %s", so)
    #    sys.exit(2)
    fn = ctypes.CDLL(so)
    # https://docs.python.org/3/library/ctypes.html#ctypes.c_char_p
    # https://stackoverflow.com/questions/3494598/passing-a-list-of-strings-to-from-python-ctypes-to-c-function-expecting-char
    # https://stackoverflow.com/questions/18959220/how-to-pass-char-pointer-as-argument-in-ctypes-python
    # https://groups.google.com/g/comp.lang.python/c/Gw907E2YYic
    ls = [file.encode('utf-8')]
    c_arr = (ctypes.c_char_p * (len(ls) + 1))()
    c_arr[:-1] = ls
    c_arr[len(ls)] = None
    c_cmd = ctypes.c_char_p("load".encode('utf-8'))

    # fn.cicd_jj.argtypes = [ctypes.c_char_p, ctypes.c_uint, ctypes.(ctypes.c_void_p)]
    fn.cicd_jj.restype = ctypes.c_char_p

    tpl_jj = fn.cicd_jj(c_cmd, len(ls), c_arr)
    c = tpl_jj.decode('utf-8')

    # fn.free_jj(tpl_jj)
    return c


def find_jj_read(file, dirs):
    c, ok = load_file_from_dirs(file, dirs)
    if ok:
        return c

    bin_path = get_cicd_bin()
    lib_jj_so = os.path.join(bin_path, "libjj.so")
    c = load_file_from_so(file, lib_jj_so)
    return c


def find_tpl_read(file, dirs):
    return find_jj_read(file, dirs)


def find_tpl_read_render(file, dirs, ctx):
    if "CICD_ARMOR" not in ctx:  # TODO migrate to var section ctl_panel as mask
        ctx["CICD_ARMOR"] = CICD_ARMOR
        # lib_cicd_so_path = ctypes.util.find_library(lib_cicd_so)
        # if lib_cicd_so_path is None:
        #    logging.error("no such library file: %s", lib_cicd_so)
        #    sys.exit(2)
        bin_path = get_cicd_bin()
        lib_cicd_so = os.path.join(bin_path, "libcicd.so")
        ctx["LIB_CICD_SO"] = lib_cicd_so
    c = find_tpl_read(file, dirs)
    tpl = jinja2.Template(c)
    return tpl.render(ctx)


def is_http_url(url):
    pat = r'https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&\/\/=]*)'
    reg1 = re.match(pat, url)
    if reg1 is not None:
        return True
    return False


def get_cicd_home():
    return os.path.dirname(get_cicd_bin())


def get_cicd_bin():
    cicd = os.path.realpath(__file__)
    bin = os.path.dirname(cicd)
    return bin


def get_make():
    if CICD_ARMOR == 1:
        return os.path.join(get_cicd_bin(), "make")
    return "make"


CICD_ARMOR = 1

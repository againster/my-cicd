package comm

import (
	"encoding/base64"
)

type UnderArmor struct {
	Name   string
	Kit    string
	Target []byte
}

type ArmKit interface {
	Arm([]byte) ([]byte, error)
	Disarm([]byte) ([]byte, error)
	Kit() string
}

func NewArmKit(s string) ArmKit {
	kitBase64 := ArmKitBase64{}
	switch s {
	case (kitBase64.Kit()):
		return &kitBase64
	}
	return nil
}

type ArmKitBase64 struct {
}

func (a *ArmKitBase64) Arm(src []byte) ([]byte, error) {
	ret := make([]byte, base64.URLEncoding.EncodedLen(len(src)))
	base64.URLEncoding.Encode(ret, src)
	return ret, nil
}

func (a *ArmKitBase64) Disarm(src []byte) ([]byte, error) {
	ret := make([]byte, base64.URLEncoding.DecodedLen(len(src)))
	_, err := base64.URLEncoding.Decode(ret, src)
	return ret, err
}

func (a *ArmKitBase64) Kit() string {
	return "base64"
}

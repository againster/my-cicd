package main

import (
	"fmt"

	"gitlab.com/againster/my-cicd/go.d/armormk"
	"gitlab.com/againster/my-cicd/go.d/comm"
)

type Disarm struct{}

func (f *Disarm) Key() string {
	return "disarm"
}

func (f *Disarm) Cmd(vv []string) (*string, error) {
	if len(vv) <= 0 || len(vv[0]) == 0 {
		return nil, fmt.Errorf("missing disarm target")
	}
	if a, ok := armormk.ArmorMap[vv[0]]; ok {
		kit := comm.NewArmKit(a.Kit)
		if kit == nil {
			return nil, fmt.Errorf("no such disarm kit %s", a.Kit)
		}
		rr, err := kit.Disarm(a.Target)
		if err != nil {
			return nil, fmt.Errorf("disarm error %s", vv[0])
		}
		gmk_eval(string(rr))
	} else {
		return nil, fmt.Errorf("no such disarm target %s", vv[0])
	}
	return nil, nil
}

func (f *Disarm) Arg() (uint, uint, uint) {
	return 1, 1, GMK_FUNC_DEFAULT()
}

package main

/*
// https://www.gnu.org/software/make/manual/html_node/Loaded-Object-Example.html
// https://www.gnu.org/software/make/manual/html_node/Loaded-Object-API.html

#ifndef _GNUMAKE_H_
#define _GNUMAKE_H_

typedef struct
{
const char *filenm;
unsigned long lineno;
} gmk_floc;

typedef char *(*gmk_func_ptr)(const char *nm, unsigned int argc, char **argv);

#ifdef _WIN32
# ifdef GMK_BUILDING_MAKE
#  define GMK_EXPORT  __declspec(dllexport)
# else
#  define GMK_EXPORT  __declspec(dllimport)
# endif
#else
# define GMK_EXPORT
#endif

GMK_EXPORT void gmk_free (char *str);
GMK_EXPORT char *gmk_alloc (unsigned int len);
GMK_EXPORT void gmk_eval (const char *buffer, const gmk_floc *floc);
GMK_EXPORT char *gmk_expand (const char *str);
GMK_EXPORT void gmk_add_function (const char *name, gmk_func_ptr func,
unsigned int min_args, unsigned int max_args,
unsigned int flags);


#define GMK_FUNC_DEFAULT    0x00
#define GMK_FUNC_NOEXPAND   0x01

#include <string.h>
#include <stdlib.h>
char* cicd_cmd(char*, unsigned int, char**);
#endif

// https://stackshare.io/go-packages-jemalloc-go
// https://medium.com/@liamkelly17/working-with-packed-c-structs-in-cgo-224a0a3b708b
// https://pkg.go.dev/cmd/cgo#hdr-Go_references_to_C
// // https://github.com/golang/go/issues/14985
#cgo darwin LDFLAGS: -Wl,-undefined,dynamic_lookup
#cgo !darwin LDFLAGS: -Wl,-unresolved-symbols=ignore-all
*/
import "C"
import (
	"fmt"
	"unsafe"
)

//export plugin_is_GPL_compatible
func plugin_is_GPL_compatible() C.int { //https://stackoverflow.com/questions/52036061/export-a-variable-when-building-a-shared-object-using-cgo
	return 0
}

//export cicd_cmd
func cicd_cmd(nm *C.char, argc C.uint, argv **C.char) *C.char {
	nms := C.GoString(nm) // https://groups.google.com/g/golang-nuts/c/H77hcVt3AAI
	/*
		argcn := uint(argc)               // https://stackoverflow.com/questions/54813015/how-can-i-change-ctype-int-to-int-in-go
		argvs := make([]string, 0, argcn) // https://github.com/golang/go/wiki/cgo#Turning_C_arrays_into_Go_slices
		for _, v := range unsafe.Slice(argv, uint(argc)) {
			argvs = append(argvs, C.GoString(v))
		}
	*/
	argvs := slice(argv, argc)
	cmdRet, err := cmd_router(nms, argvs)
	if err != nil {
		gmk_eval(fmt.Sprintf("$(error %s)", err))
		return (*C.char)(unsafe.Pointer(nil))
	}
	if cmdRet == nil {
		return (*C.char)(unsafe.Pointer(nil))
	}
	// https://stackoverflow.com/questions/47192652/how-do-i-release-the-memory-allocated-by-c-cstring
	// call free_jj to release the string memory
	cret := C.CString(*cmdRet)
	defer C.free(unsafe.Pointer(cret))
	clen := len(*cmdRet) + 1
	ret := C.gmk_alloc(C.uint(clen))
	C.strcpy(ret, cret)
	return ret
}

//export libcicd_gmk_setup
func libcicd_gmk_setup(floc *C.gmk_floc) C.int {
	// https://stackoverflow.com/questions/37157379/passing-function-pointer-to-the-c-code-using-cgo
	// https://golang-nuts.narkive.com/a4LiAf6J/cgo-error-cannot-use-gocallback-type-func-ctype-int-unsafe-pointer-as-type-0-byte-in-function
	for k, cc := range cmds() {
		i, j, f := cc.Arg()
		// https://stackoverflow.com/questions/47192652/how-do-i-release-the-memory-allocated-by-c-cstring
		// call free_jj to release the string memory
		// DO NOT FREE THIS CSTRING. make 4.2.1 version will not respond to this cmd
		ck := C.CString(k)
		C.gmk_add_function(ck, C.gmk_func_ptr(C.cicd_cmd), C.uint(i), C.uint(j), C.uint(f))
	}
	return 1
}

func gmk_eval(str string) {
	// https://stackoverflow.com/questions/47192652/how-do-i-release-the-memory-allocated-by-c-cstring
	// call free_jj to release the string memory
	buf := C.CString(str)
	defer C.free(unsafe.Pointer(buf))
	// C.sizeof_ https://pkg.go.dev/cmd/cgo#hdr-Go_references_to_C
	floc := C.gmk_alloc(C.sizeof_gmk_floc)
	defer C.gmk_free(floc)
	C.gmk_eval(buf, (*C.gmk_floc)(unsafe.Pointer(floc)))
	return
}

func cmd_router(n string, v []string) (*string, error) {
	cc := cmds()
	if c, ok := cc[n]; ok {
		return c.Cmd(v)
	}
	return nil, nil
}

func cmds() map[string]CicdCmd {
	fn := []CicdCmd{&Includes{}, &Disarm{}}
	s := make(map[string]CicdCmd, 0)
	for _, f := range fn {
		s[f.Key()] = f
	}
	return s
}

func GMK_FUNC_DEFAULT() uint {
	return 0
}

func GMK_FUNC_NOEXPAND() uint {
	return 1
}

type CicdCmd interface {
	Key() string
	Cmd([]string) (*string, error)
	Arg() (uint, uint, uint)
}

func slice(argv **C.char, argc C.uint) []string {
	// https://groups.google.com/g/golang-nuts/c/bI17Bpck8K4
	// https://pkg.go.dev/unsafe#Slice
	offset := unsafe.Sizeof(unsafe.Pointer(nil))
	n := uint(argc)
	out := make([]string, 0, n)
	for index := C.uint(0); index < argc; index++ {
		out = append(out, C.GoString(*argv))
		argv = (**C.char)(unsafe.Pointer(uintptr(unsafe.Pointer(argv)) + offset))
	}
	return out
}

func main() {}

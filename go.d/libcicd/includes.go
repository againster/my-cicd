package main

type Includes struct{}

func (f *Includes) Key() string {
	return "includes"
}

func (f *Includes) Cmd(vv []string) (*string, error) {
	if len(vv) <= 0 {
		return nil, nil
	}
	inc := "include "
	for _, v := range vv {
		cmd := inc + v
		gmk_eval(cmd)
	}
	return nil, nil
}

func (f *Includes) Arg() (uint, uint, uint) {
	return 1, 1, GMK_FUNC_DEFAULT()
}

package main

import (
	"fmt"

	"gitlab.com/againster/my-cicd/go.d/armorjj"
	"gitlab.com/againster/my-cicd/go.d/comm"
)

type Load struct{}

func (f *Load) Key() string {
	return "load"
}

func (f *Load) Cmd(vv []string) (*string, error) {
	if len(vv) <= 0 || len(vv[0]) == 0 {
		return nil, fmt.Errorf("missing load target")
	}
	if a, ok := armorjj.ArmorMap[vv[0]]; ok {
		kit := comm.NewArmKit(a.Kit)
		if kit == nil {
			return nil, fmt.Errorf("no such load kit %s", a.Kit)
		}
		rr, err := kit.Disarm(a.Target)
		if err != nil {
			return nil, fmt.Errorf("load error %s", vv[0])
		}
		ret := string(rr)
		return &ret, nil
	} else {
		return nil, fmt.Errorf("no such load target %s", vv[0])
	}
	return nil, nil
}

func (f *Load) Arg() (uint, uint, uint) {
	return 0, 0, 0
}

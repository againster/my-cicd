package main

/*
#ifndef _LIBJJ_H_
#define _LIBJJ_H_

#ifdef _WIN32
# ifdef CICD_BUILDING
#  define CICD_EXPORT  __declspec(dllexport)
# else
#  define CICD_EXPORT  __declspec(dllimport)
# endif
#else
# define CICD_EXPORT
#endif

CICD_EXPORT char* cicd_jj(char*, unsigned int, char**);
CICD_EXPORT void free_jj(char *str);

#include <string.h>
#include <stdlib.h>
#endif

// https://stackshare.io/go-packages-jemalloc-go
// https://medium.com/@liamkelly17/working-with-packed-c-structs-in-cgo-224a0a3b708b
// https://pkg.go.dev/cmd/cgo#hdr-Go_references_to_C
// https://github.com/golang/go/issues/14985
#cgo darwin LDFLAGS: -Wl,-undefined,dynamic_lookup
#cgo !darwin LDFLAGS: -Wl,-unresolved-symbols=ignore-all
*/
import "C"
import (
	"fmt"
	"os"
	"unsafe"
)

//export cicd_jj
func cicd_jj(nm *C.char, argc C.uint, argv **C.char) *C.char {
	nms := C.GoString(nm) // https://groups.google.com/g/golang-nuts/c/H77hcVt3AAI
	/*
		argcn := uint(argc)   // https://stackoverflow.com/questions/54813015/how-can-i-change-ctype-int-to-int-in-go
		argvs := make([]string, 0, argcn) // https://github.com/golang/go/wiki/cgo#Turning_C_arrays_into_Go_slices
		for _, v := range unsafe.Slice(argv, uint(argc)) {
			argvs = append(argvs, C.GoString(v))
		}
	*/
	argvs := slice(argv, argc)
	cmdRet, err := jj_router(nms, argvs)
	if err != nil {
		_, _ = fmt.Fprintf(os.Stderr, "%s", err)
		return (*C.char)(unsafe.Pointer(nil))
	}
	if cmdRet == nil {
		_, _ = fmt.Fprintf(os.Stderr, "empty")
		return (*C.char)(unsafe.Pointer(nil))
	}
	// https://stackoverflow.com/questions/47192652/how-do-i-release-the-memory-allocated-by-c-cstring
	// call free_jj to release the string memory
	return C.CString(*cmdRet)
}

//export free_jj
func free_jj(nm *C.char) {
	C.free(unsafe.Pointer(nm))
	return
}

func jj_router(n string, v []string) (*string, error) {
	cc := jjs()
	if c, ok := cc[n]; ok {
		return c.Cmd(v)
	}
	return nil, nil
}

func jjs() map[string]CicdCmd {
	fn := []CicdCmd{&Load{}}
	s := make(map[string]CicdCmd, 0)
	for _, f := range fn {
		s[f.Key()] = f
	}
	return s
}

type CicdCmd interface {
	Key() string
	Cmd([]string) (*string, error)
	Arg() (uint, uint, uint)
}

func slice(argv **C.char, argc C.uint) []string {
	// https://groups.google.com/g/golang-nuts/c/bI17Bpck8K4
	// https://pkg.go.dev/unsafe#Slice
	offset := unsafe.Sizeof(unsafe.Pointer(nil))
	n := uint(argc)
	out := make([]string, 0, n)
	for index := C.uint(0); index < argc; index++ {
		out = append(out, C.GoString(*argv))
		argv = (**C.char)(unsafe.Pointer(uintptr(unsafe.Pointer(argv)) + offset))
	}
	return out
}

func main() {}

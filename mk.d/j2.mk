# PLEASE DO NOT CHANGE SCRIPT BELOW
# AUTHOR: ArrowLee
# DATE: 20180726

ifeq ($(CICD_ARMOR),1)
$(disarm com.mk)
else
include com.mk
endif

J2CLI ?= jinja2
HAVE_J2CLI = $(call exist_command, $(J2CLI))

j2conf_DATA ?=
# if j2conf_DATA contains folders, J2_EXT indicates which files should be render. you could define a list, eg:.j2 .txt.
# empty will match all files.
J2_EXT ?=
j2_SRC ?=
J2_SRC_EXT ?=

rootj2dir ?= etc
j2confdir ?= $(prefix)/$(rootj2dir)

# stop jinja2 rendering when reaching this value
J2_RENDER_MAX ?= 64
# some parameters pass to jinja2.
J2_RENDER_FLAGS ?= --format auto -e jinja2_ansible_filters.AnsibleCoreFiltersExtension
# if j2conf_DATA contains folders, J2_EXT indicates which files should be render. we use find command to find files.
# this variable will expand like this: -name *.go -o -name *.c. and finally be used in find command.
J2_FIND_FLAGS = $(foreach p,$(J2_EXT),$(if $(filter $p,$(lastword $(J2_EXT))),-name '*$p',-name '*$p' -o))
# if j2_SRC contains folders, J2_SRC_EXT indicates which files should be render src. we use find command to find files.
# this variable will expand like this: -name *.go -o -name *.c. and finally be used in find command.
J2_SRC_FIND_FLAGS = $(foreach p,$(J2_SRC_EXT),$(if $(filter $p,$(lastword $(J2_SRC_EXT))),-name '*$p',-name '*$p' -o))

J2_TMP_CONCATENATE_SRC_FILE ?= .j2.concatenate.src.tmp

# for [include filename] instruction, same target will be overrided. to solve this
# problem, common target dependencies should append to a makefile variable;
all:: build-j2confDATA
build:: build-j2confDATA
install:: install-j2confDATA
clean:: clean-j2confDATA
uninstall:: uninstall-j2confDATA
installdirs:: install-j2confdirs
check:: check-j2

# $$file: be rendered file, $$out: output filename, $$j2src: render from file list
# $(J2_RENDER_MAX): reaching this no. will stop render
# $(J2_RENDER_FLAGS): jinja2 render flag
am__render_j2_template_file = { \
  j2=$$file; f=$$j2; sum=$(am__md5sum); \
  no=0;for (( i=1;i<=$(J2_RENDER_MAX);i++ )); do \
    tmp="$$out.$$i"; (( no = $$i - 1 )); j2last=$$out.$$no; \
    echo " $(J2CLI) $(J2_RENDER_FLAGS) -o '$$tmp' '$$j2' < $$j2src"; \
    $(J2CLI) $(J2_RENDER_FLAGS) -o "$$tmp" "$$j2" < $$j2src || exit $$?; \
    f=$$tmp; nsum=$(am__md5sum); if test "$$sum" = "$$nsum"; then no=$$i; break; fi; \
    j2=$$tmp; sum=$$nsum; \
  done; for (( i=1;i<$$no;i++ )); do echo "$$out.$$i"; \
  done | $(am__base_list) | $(am__base_list) | while read t; do \
    echo " rm -f $$t && test -f $$out.$$no && $(MV_F) $$out.$$no $$out"; \
    rm -f $$t && test -f "$$out.$$no" && $(MV_F) "$$out.$$no" "$$out"; \
  done; \
  }


.PHONY: check-j2
check-j2:
	@c=(jinja2); t=($(call batch_exist_command,jinja2)); $(am__print_result_of_check);


.PHONY: concatenate-j2src
concatenate-j2src:
	@$(NORMAL_INSTALL);
	@test 1 -eq $(HAVE_J2CLI) || exit 0; \
	rm -f $(J2_TMP_CONCATENATE_SRC_FILE); \
	list='$(j2_SRC)'; test -n "$$list" || list=; \
	flist=; dlist=; for p in $$list; do \
	  if test -e $(srcdir)/$$p; then d=$(srcdir)/; elif test -e $$p; then d=; else \
	    echo " No such file or directory '$$p'"; exit 2; \
	  fi; \
	  if ! test -d "$$d$$p"; then flist="$$flist $$d$$p"; else dlist="$$dlist $$d$$p"; fi; \
	done; \
	for p in $$dlist; do \
	  flag=( $(J2_SRC_FIND_FLAGS) ); transform=; echo $$p | $(am__recursive_dir_files) | \
	  while read subd subf; do \
	    { echo " ( cd '$$subd' && $(CAT)" $$subf " >> '$(abs_builddir)/$(J2_TMP_CONCATENATE_SRC_FILE)' )"; \
	    ( $(am__cd) "$$subd" && $(CAT) $$subf >> "$(abs_builddir)/$(J2_TMP_CONCATENATE_SRC_FILE)" ) || exit $$?; } \
	  done; \
	done;  \
	for p in $$flist; do echo "$$p"; done | $(am__base_list) | \
	while read f; do \
	  echo " $(CAT)" $$f " >> '$(abs_builddir)/$(J2_TMP_CONCATENATE_SRC_FILE)'"; \
	  $(CAT) $$f >> "$(abs_builddir)/$(J2_TMP_CONCATENATE_SRC_FILE)" || exit $$?; \
	done;


.PHONY: build-j2confDATA
build-j2confDATA: concatenate-j2src
	@$(NORMAL_INSTALL);
	@test 1 -eq $(HAVE_J2CLI) || exit 0; \
	list='$(j2conf_DATA)'; test -n "$(j2conf_DATA)" || list=; \
	flist=; dlist=; for p in $$list; do \
	  if test -e $(srcdir)/$$p; then d=$(srcdir)/; elif test -e $$p; then d=; else \
	    echo " No such file or directory '$$p'"; exit 2; \
	  fi; \
	  if ! test -d "$$d$$p"; then flist="$$flist $$d$$p"; else dlist="$$dlist $$d$$p"; fi; \
	done;\
	for p in $$dlist; do \
	  transform="s,^$$p,,;s,^/,,;"; echo $$p | $(am__recursive_dir_dirs) | \
	  while read reldir; do \
	    test -n "$$reldir" || continue; \
	    echo " $(MKDIR_P)" $$reldir; $(MKDIR_P) $$reldir || exit $$?; \
	  done; \
	  flag=( $(J2_FIND_FLAGS) ); transform=; echo $$p | $(am__recursive_dir_files) | \
	  while read subd subf; do \
	    reldir=`echo $$subd | sed -e "s,^$$p,,;s,^/,,;"`; \
	    if test -z "$$reldir"; then d=; else d=$$reldir/; fi; \
	    j2src=$(J2_TMP_CONCATENATE_SRC_FILE); \
	    for sf in $$subf; do \
	      out="$$d"`echo $$sf | sed -e "$(PAT_STRIP_EXT);"`; file=$$subd/$$sf; \
	      $(am__render_j2_template_file); \
	    done; \
	  done; \
	done; \
	for file in $$flist; do \
	  out=`echo $$file | sed -e "$(PAT_STRIP_EXT);$(PAT_STRIP_DIR);"`; \
	  j2src=$(J2_TMP_CONCATENATE_SRC_FILE); $(am__render_j2_template_file); \
	done;


.PHONY: install-j2confDATA
install-j2confDATA:
	@$(NORMAL_INSTALL);
	@test 1 -eq $(HAVE_J2CLI) || exit 0; \
	list='$(j2conf_DATA)'; test -n "$(j2confdir)" || list=; \
	if test -n "$$list"; then \
	  echo " $(MKDIR_P) '$(DESTDIR)$(j2confdir)'"; \
	  $(MKDIR_P) "$(DESTDIR)$(j2confdir)" || exit 1; \
	fi; \
	flist=; dlist=; for p in $$list; do \
	  if test -e $(srcdir)/$$p; then d=$(srcdir)/; elif test -e $$p; then d=; else \
	    echo " No such file or directory '$$p'"; exit 2; \
	  fi; \
	  if ! test -d "$$d$$p"; then flist="$$flist $$d$$p"; else dlist="$$dlist $$d$$p"; fi; \
	done; \
	for p in $$dlist; do \
	  transform="s,^$$p,,;s,^/,,;"; echo $$p | $(am__recursive_dir_dirs) | \
	  while read reldir; do \
	    test -n "$$reldir" || continue; \
	    { echo " ( cd '$(DESTDIR)$(j2confdir)' && $(MKDIR_P)" $$reldir " )"; \
	    ( $(am__cd) "$(DESTDIR)$(j2confdir)" && $(MKDIR_P) $$reldir ) || exit $$?; } \
	  done; \
	  flag=( $(J2_FIND_FLAGS) ); transform=$(PAT_STRIP_EXT); echo $$p | $(am__recursive_dir_files) | \
	  while read subd subf; do \
	    reldir=`echo $$subd | sed -e "s,^$$p,,;s,^/,,;"`; \
	    if test -z "$$reldir"; then \
	      echo " $(INSTALL_DATA)" $$subf "'$(DESTDIR)$(j2confdir)'"; \
	      $(INSTALL_DATA) $$subf "$(DESTDIR)$(j2confdir)" || exit $$?; \
	    else \
	      { echo " ( cd '$$reldir' && $(INSTALL_DATA)" $$subf "'$(DESTDIR)$(j2confdir)/$$reldir' )"; \
	      ( $(am__cd) "$$reldir" && $(INSTALL_DATA) $$subf "$(DESTDIR)$(j2confdir)/$$reldir" ) || exit $$?; } \
        fi; \
	  done; \
	done; \
	for p in $$flist; do echo "$$p" | sed -e "$(PAT_STRIP_EXT);$(PAT_STRIP_DIR);"; done | \
	$(am__base_list) | while read files; do  \
	  echo " $(INSTALL_DATA)" $$files "'$(DESTDIR)$(j2confdir)'"; \
	  $(INSTALL_DATA) $$files "$(DESTDIR)$(j2confdir)" || exit $$?; \
	done;


.PHONY: uninstall-j2confDATA
uninstall-j2confDATA:
	@$(NORMAL_UNINSTALL)
	-@list='$(j2conf_DATA)'; test -n "$(j2conf_DATA)" || list=; \
	flist=; dlist=; for p in $$list; do \
	  if test -e $(srcdir)/$$p; then d=$(srcdir)/; elif test -e $$p; then d=; else \
	    echo " No such file or directory '$$p'"; exit 2; \
	  fi; \
	  if ! test -d "$$d$$p"; then flist="$$flist $$d$$p"; else dlist="$$dlist $$d$$p"; fi; \
	done; \
	for p in $$dlist; do \
	  flag=( $(J2_FIND_FLAGS) ); transform="$(PAT_STRIP_EXT);$(PAT_STRIP_DIR)"; \
	  echo $$p | $(am__recursive_dir_files) | while read subd subf; do \
	    reldir=`echo $$subd | sed -e "s,^$$p,,;s,^/,,;"`; \
	    if test -z "$$reldir"; then \
	      dir="$(DESTDIR)$(j2confdir)"; files=$$subf; \
	      $(am__uninstall_files_from_dir); \
	    else \
	      dir="$(DESTDIR)$(j2confdir)/$$reldir"; files=$$subf; \
	      $(am__uninstall_files_from_dir); \
        fi; \
	  done; \
	done; \
	for p in $$flist; do echo "$$p" | sed -e "$(PAT_STRIP_EXT);$(PAT_STRIP_DIR);"; done | \
	$(am__base_list) | while read files; do  \
	  dir="$(DESTDIR)$(j2confdir)/$$reldir"; \
	  $(am__uninstall_files_from_dir); \
	done;


.PHONY: clean-j2confDATA
clean-j2confDATA:
	-@list='$(j2conf_DATA)'; test -n "$(j2conf_DATA)" || list=; \
	echo " rm -f $(J2_TMP_CONCATENATE_SRC_FILE)"; rm -f $(J2_TMP_CONCATENATE_SRC_FILE); \
	flist=; dlist=; for p in $$list; do \
	  if test -e $(srcdir)/$$p; then d=$(srcdir)/; elif test -e $$p; then d=; else \
	    echo " No such file or directory '$$p'"; exit 2; \
	  fi; \
	  if ! test -d "$$d$$p"; then flist="$$flist $$d$$p"; else dlist="$$dlist $$d$$p"; fi; \
	done; \
	for p in $$dlist; do \
	  flag=( $(J2_FIND_FLAGS) ); transform="$(PAT_STRIP_EXT);$(PAT_STRIP_DIR)"; \
	  echo $$p | $(am__recursive_dir_files) | while read subd subf; do \
	    reldir=`echo $$subd | sed -e "s,^$$p,,;s,^/,,;"`; \
	    if test -z "$$reldir"; then \
	      dir="$(abs_builddir)"; files=$$subf; \
	      $(am__uninstall_files_from_dir); \
	    else \
	      dir="$(abs_builddir)/$$reldir"; files=$$subf; \
	      $(am__uninstall_files_from_dir); \
        fi; \
	  done; \
	done; \
	for p in $$flist; do echo "$$p" | sed -e "$(PAT_STRIP_EXT);$(PAT_STRIP_DIR);"; done | \
	$(am__base_list) | while read files; do  \
	  dir="$(abs_builddir)/$$reldir"; \
	  $(am__uninstall_files_from_dir); \
	done;

.PHONY: install-j2confdirs
install-j2confdirs:
	@for dir in "$(DESTDIR)$(j2confdir)"; do \
	  test -z "$$dir" || $(MKDIR_P) "$$dir"; \
	done


#TODO: Optimize performance like this. Build target will take advantage of parallel job.
#but `make --dry-run` will not generate dependencies scripts. this feature will implement in the feature:
#server check license and generate running scripts, client only run these scripts.
#
#$(J2_OUT_FILE): $(J2_SRC_FILE) $(J2_DST_FILE)
#	@test 1 -eq $(HAVE_J2CLI) || exit 0; \
#	file=$@$(J2_EXT); out=$@; j2src="$(J2_SRC_FILE)"; $(am__render_j2_template_file);

# $(filter %,) will trim extra whitespace
# $(addsuffix /,$(patsubst %/,%,$p)) will force add slash / to string end
# $(if $(J2_EXT),$(J2_EXT),*) if J2_EXT will return *
#J2_SRC_FILE ?= $(filter %,$(foreach p,$(j2_SRC),$(if $(shell test -f $p && echo "$p"),$p,$(foreach e,$(if $(J2_SRC_EXT),$(J2_SRC_EXT),*),$(call rsubfile,$(addsuffix /,$(patsubst %/,%,$p)),*$e)))))
#J2_DST_FILE ?= $(filter %,$(foreach p,$(j2conf_DATA),$(if $(shell test -f $p && echo "$p"),$p,$(foreach e,$(if $(J2_EXT),$(J2_EXT),*),$(call rsubfile,$(addsuffix /,$(patsubst %/,%,$p)),*$e)))))
#J2_OUT_FILE ?= $(basename $(J2_DST_FILE))


.PHONY: render-j2
render-j2:
	@test 1 -eq $(HAVE_J2CLI) || exit 0; \
	test -n "$(J2_SRC_FILE)" || exit 0; \
	list='$(J2_DST_FILE)'; test -n "$(J2_DST_FILE)" || exit 0; \
	for file in $$list; do \
	  out=`echo $$file | sed -e "$(PAT_STRIP_EXT)"`; \
	  j2src="$(J2_SRC_FILE)"; $(am__render_j2_template_file); \
	done;
# PLEASE DO NOT CHANGE SCRIPT BELOW
# AUTHOR: ArrowLee
# DATE: 20190923

# path definition
# first_makefile_path = $(abspath $(firstword $(MAKEFILE_LIST)))
# current_dir = $(notdir $(patsubst %/,%,$(dir $(first_makefile_path))))
# abs_current_dir = $(patsubst %/,%,$(dir $(first_makefile_path)))
#
# modulename ?= ${current_dir}
#
# pkgdatadir = ${datadir}/${modulename}
# pkgincludedir = ${includedir}/${modulename}
# pkglibdir = ${libdir}/${modulename}
# pkglibexecdir = ${libexecdir}/${modulename}

prefix ?= /usr
bindir = ${exec_prefix}/bin
datadir = ${datarootdir}
datarootdir = ${prefix}/share
docdir = ${datarootdir}/doc/${modulename}
dvidir = ${docdir}
exec_prefix = ${prefix}
htmldir = ${docdir}
includedir = ${prefix}/include
infodir = ${datarootdir}/info
libdir = ${exec_prefix}/lib
libexecdir = ${exec_prefix}/libexec
localedir = ${datarootdir}/locale
localstatedir = ${prefix}/var
runstatedir = ${localstatedir}/run
mandir = ${datarootdir}/man
man1dir = ${mandir}/man1
man2dir = ${mandir}/man2
oldincludedir = /usr/include
pdfdir = ${docdir}
psdir = ${docdir}
sbindir = ${exec_prefix}/sbin
sharedstatedir = ${prefix}/com
unitdir = ${libdir}/systemd/system
userunitdir = ${libdir}/systemd/user

srcdir ?= .
abs_srcdir = $(realpath $(srcdir))
top_srcdir =
abs_top_srcdir =

builddir ?= .
abs_builddir = $(realpath $(builddir))
top_builddir =
abs_top_builddir =

# build definition
build = x86_64-unknown-linux-gnu
build_alias = 
build_cpu = x86_64
build_os = linux-gnu
build_vendor = unknown
host = x86_64-unknown-linux-gnu
host_alias = 
host_cpu = x86_64
host_os = linux-gnu
host_vendor = unknown

transform = $(program_transform_name)
program_transform_name = s,x,x,
target_alias =

DESTDIR ?=
VERBOSE ?= 0
SHELL := /bin/bash

# tool definition
install_sh_DATA = $(install_sh) -c -m 644
install_sh_PROGRAM = $(install_sh) -c
install_sh_SCRIPT = $(install_sh) -c
install_sh = $(INSTALL)
INSTALL = $(shell command -v install) -c
INSTALL_DATA = ${INSTALL} -m 644
INSTALL_PROGRAM = ${INSTALL}
NORMAL_INSTALL = :
NORMAL_UNINSTALL = :

LIBTOOL = $(SHELL) libtool
LIBTOOLFLAGS = --quiet

AWK = $(shell command -v awk)
CAT = $(shell command -v cat)
mkdir_p = $(MKDIR_P)
MKDIR_P = $(shell command -v mkdir) -p
MV_F = mv -f
GREP = $(shell command -v grep)
EGREP = ${GREP} -E
FGREP = ${GREP} -F
SED = $(shell command -v sed)
LN_S = ln -s

# for [include filename] instruction, same target will be overrided. to solve this
# problem, common target dependencies should append to a makefile variable;
all-multi ?=
clean-multi ?=
install-multi ?=
uninstall-multi ?=
installdirs-multi ?=
test-multi ?=
pack-multi ?=
push-multi ?=

# pattern definition
# strip first word and whitespace. eg: root test.j2 demo.ini -> test.j2 demo.ini
PAT_STRIP_FIRST_WORD = s,^[^ ]* *,,
# strip second word and whitespace in front of it, and the rest. eg: root test.j2 demo.ini -> root
PAT_STRIP_REST_WORD = s, .*$$,,
# strip dir path from string. eg: /root/.test.j2 -> .test.j2
PAT_STRIP_DIR = s,^.*/,,
# strip file name from string. eg: /root/.test.j2 -> /root , /.test.j2 -> /, .test.j2 -> /
PAT_STRIP_BASENAME = s,/[^/]*$$,,;s,^$$,/,
# strip dir and file name, only keep extension. /root/.test.j2 -> .j2
PAT_STRIP_BASE = s,^.*\.,\.,
# strip extension. eg: /root/.test.j2 -> /root/.test , test.json.j2 -> test.json
PAT_STRIP_EXT = s,\.[^.]*$$,,
# strip ../ ./ etc relative prefix. ~/.test.j2 -> .test.j2; ./test.j2 -> test.j2; . -> ;
PAT_STRIP_RELATIVE_PREFIX = s,^\(\./\|\.\./\|~\|~/\)*,,;s,^\.$$,,
# split a path to dir and basename. eg: /.test.j2 -> / .test.j2 , .test.j2 -> / .test.j2
PAT_SPLIT_DIR_BASENAME = h;$(PAT_STRIP_DIR);x;$(PAT_STRIP_BASENAME);G;s,\n, ,
# strip first column and $(field_sep). eg: fool__SEP__bar__SEP__like -> bar__SEP__like
PAT_STRIP_FIRST_COLUMN = s,$(field_sep),\r,;s,^.*\r,,;
# strip second column and $(field_sep) in front of it, and the rest. eg: fool__SEP__bar__SEP__like -> fool
PAT_STRIP_REST_COLUMN = s,$(field_sep).*$$,,

am__cd = CDPATH="$${ZSH_VERSION+.}$(PATH_SEPARATOR)" && cd

# md5 sum
MD5SUM ?= $(if $(shell command -v md5sum),md5sum -b,$(if $(shell command -v md5),md5 -r))
#$$f: file name
am__md5sum = `$(MD5SUM) $$f | awk '{print $$1}'`

# see sed !N, https://blog.51cto.com/goome/1749171, egg: seq 7 | sed '$!N;$!N;s/\n/+/g;'
# replace multiline to one line with whitespace sparating, edd: seq 7 | am__base_list | ...
am__base_list = \
  sed '$$!N;$$!N;$$!N;$$!N;$$!N;$$!N;$$!N;s/\n/ /g' | \
  sed '$$!N;$$!N;$$!N;$$!N;s/\n/ /g;'


am__relativize = \
  dir0=`pwd`; \
  sed_first='s,^\([^/]*\)/.*$$,\1,'; \
  sed_rest='s,^[^/]*/*,,'; \
  sed_last='s,^.*/\([^/]*\)$$,\1,'; \
  sed_butlast='s,/*[^/]*$$,,'; \
  while test -n "$$dir1"; do \
    first=`echo "$$dir1" | sed -e "$$sed_first"`; \
    if test "$$first" != "."; then \
      if test "$$first" = ".."; then \
        dir2=`echo "$$dir0" | sed -e "$$sed_last"`/"$$dir2"; \
        dir0=`echo "$$dir0" | sed -e "$$sed_butlast"`; \
      else \
        first2=`echo "$$dir2" | sed -e "$$sed_first"`; \
        if test "$$first2" = "$$first"; then \
          dir2=`echo "$$dir2" | sed -e "$$sed_rest"`; \
        else \
          dir2="../$$dir2"; \
        fi; \
        dir0="$$dir0"/"$$first"; \
      fi; \
    fi; \
    dir1=`echo "$$dir1" | sed -e "$$sed_rest"`; \
  done; \
  reldir="$$dir2"

am__uninstall_files_from_dir = { \
  test -z "$$files" \
    || { test ! -d "$$dir" && test ! -f "$$dir" && test ! -r "$$dir"; } \
    || { echo " ( cd '$$dir' && rm -f" $$files ")"; \
         ( $(am__cd) "$$dir" && rm -f $$files; ) }; \
  }

am__install_subdir_dirs_to_dir = { \
  if test -d "$$src"; then { find "$$src" -type d | sed -e "s,^$$src,,;" | while read subdir; do \
    test -z "$$subdir" && continue; test -d "$$src$$subdir" && echo "$$dest$$subdir"; \
  done | $(am__base_list) | while read dir; do \
    test -z "$$dir" || { echo " $(MKDIR_P) $$dir"; $(MKDIR_P) $$dir || exit $$?; }; \
  done; }; fi; \
  }

am__install_subdir_files_to_dir = { \
  if test -d "$$src"; then { find "$$src" -type d | while read subdir; do \
    comdir=`echo $$subdir | sed -e "s,^$$src,,;"`; test -d "$$dest$$comdir" || exit $$?; \
    find "$$subdir" -type f -maxdepth 1 "$${flag[@]}" | sed -e "$$transform" | \
    sed -e "s,^$$subdir/,," | $(am__base_list) | while read files; do \
      { echo " ( cd '$$subdir' && $(INSTALL_DATA)" $$files "'$$dest$$comdir' )"; \
      ( $(am__cd) "$$subdir" && $(INSTALL_DATA) $$files "$$dest$$comdir" ) || exit $$?; } \
      done; \
    done; }; fi; \
  }

am__uninstall_subdir_files_from_dir = { \
  if test -d "$$src"; then { find "$$src" -type d | while read subdir; do \
    comdir=`echo $$subdir | sed -e "s,$$src,,;"`; test -d "$$dest$$comdir" || continue; \
    find "$$subdir" -type f -maxdepth 1 "$${flag[@]}" | sed -e "$$transform" | \
    sed -e "s,^$$subdir/,," | $(am__base_list) | while read files; do \
      dir=$$dest$$comdir; $(am__uninstall_files_from_dir); \
      done; \
    done; }; fi; \
  }


# deprecated
# list all sub dirs and files. pipeline to this function. eg: echo /tmp | $(am__recursive_dir_files).
# param: transform. will use sed to make some string transforming on sub files.
# output: first output is a path, second path is file list.
am__recursive_dir_files2 = { while read srcdir; do \
  find $$srcdir -type d | while read subdir; do \
    find "$$subdir" -type f -maxdepth 1 "$${flag[@]}" | sed -e "s,^$$subdir,,;s,^/,,;$$transform" | \
    $(am__base_list) | while read files; do echo "$$subdir $$files"; done; \
  done; \
  done; }

# optimized am__recursive_dir_files2. speed up about 4 times.
am__recursive_dir_files = { while read srcdir; do \
    nf=0; mx=40; EOF=false; find "$$srcdir" -type f "$${flag[@]}" | sed -e '$(PAT_SPLIT_DIR_BASENAME)' | \
    while ! $$EOF; do read -r d f || EOF=true; test -z "$$transform" || { f=`echo $$f | sed -e "$$transform"`; }; \
      $$EOF && { test $$nf -le 0 || echo "$$hd $$hf"; break; };  \
      if test "$$hd" = "$$d"; then \
        test $$nf -lt $$mx || { echo "$$hd $$hf"; hd="$$d"; hf="$$f"; nf=1; continue; }; \
        hf="$$hf $$f"; ((nf+=1)); \
      else { test $$nf -le 0 || echo "$$hd $$hf"; hd="$$d"; hf="$$f"; nf=1; }; fi; \
    done; \
  done; }

am__recursive_dir_dirs = { while read srcdir; do \
  find $$srcdir -type d | sed -e "$$transform" | $(am__base_list); \
  done; }

am__print_result_of_check = { \
  for(( i=0;i<$${\#c[@]};i++ )); do printf '%-40s%-10s\n' "$${c[$$i]}" \
  `echo "$${t[$$i]}" | sed -e 's,1,yes,;s,0,no,'`; done; \
  }

# https://stackoverflow.com/a/12959764
# recursively wildcard traverse. egg: $(call rwildcard,foo/,*.html)
# dir and filename don't include whitespace
# $1 is a dir, must be over with /
rwildcard=$(wildcard $1$2) $(foreach d,$(wildcard $1*),$(call rwildcard,$d/,$2))
rsubdir=$(filter-out $(patsubst %/,%,$1)/,$(sort $(dir $(call rwildcard,$(patsubst %/,%,$1)/,*))))
rsubfile=$(filter-out $1 $(patsubst %/,%,$(call rsubdir,$1)),$(call rwildcard,$1,$2))


PYTHON ?= $(shell command -v python3)
# see https://askubuntu.com/questions/588390/how-do-i-check-whether-a-module-is-installed-in-python-and-install-it-if-needed
# test python module is exists. egg: $(call exist_python_module,grpc_tools), exist return 1, or 0.
exist_python_module = $(shell $(PYTHON) -c  "import sys, pkgutil; print(1 if pkgutil.find_loader(sys.argv[1]) else 0)" $1)
batch_exist_python_module=$(foreach cmd,$(1),$(if $(filter 1,$(call exist_python_module,$(cmd))),1,0))

# test command is exists. egg: $(call exist_command,protoc), exist return 1, or 0.
exist_command=$(if $(shell command -v $1),1,0)
batch_exist_command=$(foreach cmd,$(1),$(if $(filter 1,$(call exist_command,$(cmd))),1,0))


# replace new line to one line. egg: $(call substline,$(space),$var)
# see https://stackoverflow.com/questions/4734985/replace-spaces-with-new-line-in-makefile/4735256
# see https://ftp.gnu.org/old-gnu/Manuals/make-3.79.1/html_chapter/make_8.html#SEC76
define \n


endef
field_sep:=__SEP__
null:=
comma:=,
space:= $(null) $(null)
substline=$(subst $(\n),$1,$2)
define \t
$(null)	$(null)
endef

# export environment when $(1) is not empty. egg: $(eval $(call export_env_var_if_no_empty,GOOS))
# see https://stackoverflow.com/questions/2838715/makefile-variable-initialization-and-export
# see https://stackoverflow.com/questions/13421848/how-to-use-ifeq-inside-of-a-define-in-gnu-make
# see https://stackoverflow.com/questions/28328794/makefile-delay-variable-expansion
define export_env_var_if_no_empty
ifneq ($(strip $(1)),)
export $(1)=$($(1))
endif
endef

define export_env_var_if_no_empty_kv
ifneq ($(strip $(2)),)
export $(1)=$(2)
endif
endef

.PHONY: all init check build clean uninstall install installdirs test pack push deploy poll
all::
init::
check::
build::
clean::
uninstall::
install::
installdirs::
test::
pack::
push::
deploy::
poll::

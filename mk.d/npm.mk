# PLEASE DO NOT CHANGE SCRIPT BELOW
# AUTHOR: ArrowLee
# DATE: 20221019

ifeq ($(CICD_ARMOR),1)
$(disarm com.mk)
else
include com.mk
endif

HAVE_NPM = $(call exist_command, $(NPM))
NPM ?= npm
NPM_INSTALL = $(NPM) install
NPM_RUN = $(NPM) run

npmdist_PROGRAMS ?=
npmdist_COMMANDS ?=

NPM_INSTALL_FLAGS ?=
NPM_RUN_FLAGS ?=

ngxdatadir = ${datadir}/nginx
ngxhtmldir = ${ngxdatadir}/html

# single mode will strip dir of ${npmdist_PROGRAMS}, only if $(words $(npmdist_PROGRAMS)) == 1
# value 1 enable, 0 disable
NPM_ENABLE_SINGLE_MODE = 0

# for [include filename] instruction, same target will be overrided. to solve this
# problem, common target dependencies should append to a makefile variable;
all:: npm-run
build:: npm-run
install:: install-npmdistPROGRAMS
uninstall:: uninstall-npmdistPROGRAMS
check:: check-npm

.PHONY: check-npm
check-npm:
	@c=(npm); t=($(call batch_exist_command,npm)); $(am__print_result_of_check);


.PHONY: npm-install-dep
npm-install-dep:
	@test 1 -eq $(HAVE_NPM) || exit 0; \
	echo " ( cd $(srcdir) && $(NPM_INSTALL) $(NPM_INSTALL_FLAGS) )"; \
	( cd $(srcdir) && $(NPM_INSTALL) $(NPM_INSTALL_FLAGS) ) || exit $$?;


# export env variable.
# don't use function export_env_var_if_no_empty, which has no effect at some scenario.
# see https://stackoverflow.com/questions/23843106/how-to-set-child-process-environment-variable-in-makefile
.PHONY: npm-run
npm-run: export NPM_OUTPUT_ROOT_DIR=$(abs_builddir)
npm-run: npm-install-dep
	@test 1 -eq $(HAVE_NPM) || exit 0; \
	list="$(npmdist_PROGRAMS)"; test -n "$$list" || exit 0; \
	cmds="$(npmdist_COMMANDS)"; for p in $$list; do \
	  cmd=`echo $$cmds | sed '$(PAT_STRIP_REST_COLUMN)'`; cmds=`echo $$cmd | sed '$(PAT_STRIP_FIRST_COLUMN)'`; \
	  echo " ( cd $(srcdir) && $(NPM_RUN) $(NPM_RUN_FLAGS) $$cmd )"; \
	  ( export NPM_OUTPUT_DIR="$(abs_builddir)/$$p" && cd $(srcdir) && $(NPM_RUN) $(NPM_RUN_FLAGS) $$cmd ) || exit $$?; \
	done


.PHONY: install-npmdistPROGRAMS
install-npmdistPROGRAMS:
	@$(NORMAL_INSTALL)
	@list="$(npmdist_PROGRAMS)"; test -n "$(npmdist_PROGRAMS)" || exit 0; \
	if test -n "$$list"; then \
	  echo " $(MKDIR_P) '$(DESTDIR)$(ngxhtmldir)'"; \
	  $(MKDIR_P) "$(DESTDIR)$(ngxhtmldir)" || exit 1; \
	fi; \
	test 0 -eq $(NPM_ENABLE_SINGLE_MODE) || test 1 -eq $(words $(npmdist_PROGRAMS)) || { \
	  echo " ( $(am__cd) '$(DESTDIR)$(ngxhtmldir)' && $(MKDIR_P) $$list )"; \
	  ( $(am__cd) "$(DESTDIR)$(ngxhtmldir)" && $(MKDIR_P) $$list ) } || exit $$?; \
	for p in $$list; do \
	  if test -e $(srcdir)/$$p; then d=$(srcdir)/; elif test -e $$p; then d=; else \
	    echo " No such file or directory '$$p'"; exit 2; \
	  fi; \
	  if test 1 -eq $(NPM_ENABLE_SINGLE_MODE) && test 1 -eq $(words $(npmdist_PROGRAMS)); \
	    then m=; else m="$$p"; \
	  fi; \
	  src="$$d$$p"; dest="$(DESTDIR)$(ngxhtmldir)/$$m"; \
	  $(am__install_subdir_dirs_to_dir); \
	  $(am__install_subdir_files_to_dir); \
	done


.PHONY: install-npmdirs
install-npmdirs:
	@$(NORMAL_INSTALL)
	@list='$(npmdist_PROGRAMS)'; test -n "$(ngxhtmldir)" || list=; \
	if test -n "$$list"; then \
	  echo " $(MKDIR_P) '$(DESTDIR)$(ngxhtmldir)'"; \
	  $(MKDIR_P) "$(DESTDIR)$(ngxhtmldir)" || exit 1; \
	fi;

.PHONY: uninstall-npmdistPROGRAMS
uninstall-npmdistPROGRAMS:
	@$(NORMAL_UNINSTALL)
	@list="$(npmdist_PROGRAMS)"; test -n "$(npmdist_PROGRAMS)" || exit 0; \
	for p in $$list; do \
	  if test -e $(srcdir)/$$p; then d=$(srcdir)/; elif test -e $$p; then d=; else \
	    echo " No such file or directory '$$p'"; exit 2; \
	  fi; \
	  src="$$d$$p"; dest="$(DESTDIR)$(ngxhtmldir)/$$p"; $(am__uninstall_subdir_files_from_dir); \
	done


# PLEASE DO NOT CHANGE SCRIPT BELOW
# AUTHOR: ArrowLee
# DATE: 20190923

ifeq ($(CICD_ARMOR),1)
$(disarm com.mk)
else
include com.mk
endif

HAVE_PROTOC=$(call exist_command,protoc)
HAVE_GRPC_CPP_PLUGIN=$(call exist_command,grpc_cpp_plugin)
HAVE_PY_GRPC_TOOLS=$(call exist_python_module,grpc_tools)
HAVE_PROTOC_GEN_GO=$(call exist_command,protoc-gen-gofast)
HAVE_PROTOC_GEN_GO_GRPC=$(call exist_command,protoc-gen-go-grpc)

PROTOC ?= protoc
GRPC_CPP_PLUGIN ?= grpc_cpp_plugin

proto_DIR ?= .

PROTO_PATH ?=
PROTO_EXT = .proto
PROTO_CC_EXT = .h .cc .cpp .hpp
PROTO_PY_EXT = .py
PROTO_GO_EXT = .go
PROTO_CLEAN_EXT = $(PROTO_CC_EXT) $(PROTO_PY_EXT) $(PROTO_GO_EXT)
PROTO_CLEAN_FIND_FLAGS = $(foreach p,$(PROTO_CLEAN_EXT),$(if $(filter $p,$(lastword $(PROTO_CLEAN_EXT))),-name '*$p',-name '*$p' -o))
PROTO_FIND_FLAGS = $(foreach p,$(PROTO_EXT),$(if $(filter $p,$(lastword $(PROTO_EXT))),-name '*$p',-name '*$p' -o))
PROTO_PATH_FLAGS ?= --proto_path=. $(foreach p,$(PROTO_PATH),--proto_path=$p)

PROTOC_CC_FLAGS ?= --cpp_out=. $(PROTO_PATH_FLAGS)
PROTOC_CC_GRPC_FLAGS ?= --grpc_out=. --plugin=protoc-gen-grpc=$(GRPC_CPP_PLUGIN) $(PROTO_PATH_FLAGS)

PROTOC_PY_FLAGS ?= --python_out=. $(PROTO_PATH_FLAGS)
PROTOC_PY_GRPC_FLAGS ?= -m grpc_tools.protoc --python_out=. --grpc_python_out=. $(PROTO_PATH_FLAGS)

PROTOC_GO_FLAGS ?= --gofast_out=. --gofast_opt=paths=source_relative $(PROTO_PATH_FLAGS)
PROTOC_GO_GRPC_FLAGS ?= --go-grpc_out=. --go-grpc_opt=paths=source_relative $(PROTO_PATH_FLAGS)


all:: proto-py proto-cc proto-go
build:: proto-py proto-cc proto-go
clean:: clean-proto
check:: check-proto

.PHONY: check-proto
check-proto:
	@c=(protoc grpc_cpp_plugin grpc_tools protoc-gen-gofast protoc-gen-go-grpc); \
	t=($(call batch_exist_command,protoc grpc_cpp_plugin grpc_tools protoc-gen-gofast protoc-gen-go-grpc)); \
	$(am__print_result_of_check); \
	c=(grpc_tools); t=($(call batch_exist_command,grpc_tools)); $(am__print_result_of_check);

.PHONY: proto-cc
proto-cc:
	@test 1 -eq $(HAVE_PROTOC) || exit 0; \
	list="$(proto_DIR)"; test -n "$$list" || exit 0; \
	flist=; dlist=; for p in $$list; do \
	  if test -e $(srcdir)/$$p; then d=$(srcdir)/; elif test -e $$p; then d=; else \
	    echo " No such file or directory '$$p'"; exit 2; \
	  fi; \
	  if ! test -d "$$d$$p"; then flist="$$flist $$d$$p"; else dlist="$$dlist $$d$$p"; fi; \
	done; \
	for p in $$dlist; do \
	  flag=( $(PROTO_FIND_FLAGS) ); transform="$(PAT_STRIP_DIR)"; \
	  echo $$p | $(am__recursive_dir_files) | while read subd subf; do \
	    { echo " ( cd '$$subd' && $(PROTOC) $(PROTOC_CC_FLAGS)" $$subf " )"; \
	    ( $(am__cd) "$$subd" && $(PROTOC) $(PROTOC_CC_FLAGS) $$subf ) || exit $$?; }; \
	    if test 1 -eq $(HAVE_GRPC_CPP_PLUGIN); then \
	      { echo " ( cd '$$subd' && $(PROTOC) $(PROTOC_CC_GRPC_FLAGS)" $$subf " )"; \
	      ( $(am__cd) "$$subd" && $(PROTOC) $(PROTOC_CC_GRPC_FLAGS) $$subf ) || exit $$?; }; \
	    fi; \
	  done; \
	done; \
	for p in $$flist; do echo "$$p"; done | sed -e '$(PAT_SPLIT_DIR_BASENAME)' | while read subd subf; do \
	    { echo " ( cd '$$subd' && $(PROTOC) $(PROTOC_CC_FLAGS)" $$subf " )"; \
	    ( $(am__cd) "$$subd" && $(PROTOC) $(PROTOC_CC_FLAGS) $$subf ) || exit $$?; }; \
	    if test 1 -eq $(HAVE_GRPC_CPP_PLUGIN); then \
	      { echo " ( cd '$$subd' && $(PROTOC) $(PROTOC_CC_GRPC_FLAGS)" $$subf " )"; \
	      ( $(am__cd) "$$subd" && $(PROTOC) $(PROTOC_CC_GRPC_FLAGS) $$subf ) || exit $$?; }; \
	    fi; \
	done;

.PHONY: proto-py
proto-py:
	@test 1 -eq $(HAVE_PROTOC) || exit 0; \
	list="$(proto_DIR)"; test -n "$$list" || exit 0; \
	flist=; dlist=; for p in $$list; do \
	  if test -e $(srcdir)/$$p; then d=$(srcdir)/; elif test -e $$p; then d=; else \
	    echo " No such file or directory '$$p'"; exit 2; \
	  fi; \
	  if ! test -d "$$d$$p"; then flist="$$flist $$d$$p"; else dlist="$$dlist $$d$$p"; fi; \
	done; \
	for p in $$dlist; do \
	  flag=( $(PROTO_FIND_FLAGS) ); transform="$(PAT_STRIP_DIR)"; \
	  echo $$p | $(am__recursive_dir_files) | while read subd subf; do \
	    { echo " ( cd '$$subd' && $(PROTOC) $(PROTOC_PY_FLAGS)" $$subf " )"; \
	    ( $(am__cd) "$$subd" && $(PROTOC) $(PROTOC_PY_FLAGS) $$subf ) || exit $$?; }; \
	    if test 1 -eq $(HAVE_PY_GRPC_TOOLS); then \
	      { echo " ( cd '$$subd' && $(PROTOC) $(PROTOC_GO_GRPC_FLAGS)" $$subf " )"; \
	      ( $(am__cd) "$$subd" && $(PROTOC) $(PROTOC_GO_GRPC_FLAGS) $$subf ) || exit $$?; }; \
	    fi; \
	  done; \
	done; \
	for p in $$flist; do echo "$$p"; done | sed -e '$(PAT_SPLIT_DIR_BASENAME)' | while read subd subf; do \
	    { echo " ( cd '$$subd' && $(PROTOC) $(PROTOC_PY_FLAGS)" $$subf " )"; \
	    ( $(am__cd) "$$subd" && $(PROTOC) $(PROTOC_PY_FLAGS) $$subf ) || exit $$?; }; \
	    if test 1 -eq $(HAVE_PY_GRPC_TOOLS); then \
	      { echo " ( cd '$$subd' && $(PROTOC) $(PROTOC_GO_GRPC_FLAGS)" $$subf " )"; \
	      ( $(am__cd) "$$subd" && $(PROTOC) $(PROTOC_GO_GRPC_FLAGS) $$subf ) || exit $$?; }; \
	    fi; \
	done;

.PHONY: proto-go
proto-go:
	@test 1 -eq $(HAVE_PROTOC) || exit 0; \
	list="$(proto_DIR)"; test -n "$$list" || exit 0; \
	flist=; dlist=; for p in $$list; do \
	  if test -e $(srcdir)/$$p; then d=$(srcdir)/; elif test -e $$p; then d=; else \
	    echo " No such file or directory '$$p'"; exit 2; \
	  fi; \
	  if ! test -d "$$d$$p"; then flist="$$flist $$d$$p"; else dlist="$$dlist $$d$$p"; fi; \
	done; \
	for p in $$dlist; do \
	  flag=( $(PROTO_FIND_FLAGS) ); transform="$(PAT_STRIP_DIR)"; \
	  echo $$p | $(am__recursive_dir_files) | while read subd subf; do \
	    if test 1 -eq $(HAVE_PROTOC_GEN_GO); then \
	      { echo " ( cd '$$subd' && $(PROTOC) $(PROTOC_GO_FLAGS)" $$subf " )"; \
	      ( $(am__cd) "$$subd" && $(PROTOC) $(PROTOC_GO_FLAGS) $$subf ) || exit $$?; }; \
	    fi; \
	    if test 1 -eq $(HAVE_PROTOC_GEN_GO_GRPC); then \
	      { echo " ( cd '$$subd' && $(PROTOC) $(PROTOC_GO_GRPC_FLAGS)" $$subf " )"; \
	      ( $(am__cd) "$$subd" && $(PROTOC) $(PROTOC_GO_GRPC_FLAGS) $$subf ) || exit $$?; }; \
	    fi; \
	  done; \
	done; \
	for p in $$flist; do echo "$$p"; done | sed -e '$(PAT_SPLIT_DIR_BASENAME)' | while read subd subf; do \
	    if test 1 -eq $(HAVE_PROTOC_GEN_GO); then \
	      { echo " ( cd '$$subd' && $(PROTOC) $(PROTOC_GO_FLAGS)" $$subf " )"; \
	      ( $(am__cd) "$$subd" && $(PROTOC) $(PROTOC_GO_FLAGS) $$subf ) || exit $$?; }; \
	    fi; \
	    if test 1 -eq $(HAVE_PROTOC_GEN_GO_GRPC); then \
	      { echo " ( cd '$$subd' && $(PROTOC) $(PROTOC_GO_GRPC_FLAGS)" $$subf " )"; \
	      ( $(am__cd) "$$subd" && $(PROTOC) $(PROTOC_GO_GRPC_FLAGS) $$subf ) || exit $$?; }; \
	    fi; \
	done;

.PHONY: install-proto
install-proto:

.PHONY: uninstall-proto
uninstall-proto:

.PHONY: clean-proto
clean-proto:
	@list="$(proto_DIR)"; test -n "$$list" || exit 0; \
	flist=; dlist=; for p in $$list; do \
	  if test -e $(srcdir)/$$p; then d=$(srcdir)/; elif test -e $$p; then d=; else \
	    echo " No such file or directory '$$p'"; exit 2; \
	  fi; \
	  if ! test -d "$$d$$p"; then flist="$$flist $$d$$p"; else dlist="$$dlist $$d$$p"; fi; \
	done; \
	for p in $$dlist; do \
	  flag=( $(PROTO_CLEAN_FIND_FLAGS) ); transform="$(PAT_STRIP_DIR)"; \
	  echo $$p | $(am__recursive_dir_files) | while read subd subf; do \
	    { echo " ( cd '$$subd' && rm -f" $$subf " )"; \
	    ( $(am__cd) "$$subd" && rm -f $$subf ) || exit $$?; }; \
	  done; \
	done; \
	for p in $$flist; do echo "$$p"; done | sed -e '$(PAT_SPLIT_DIR_BASENAME)' | while read subd subf; do \
	    { echo " ( cd '$$subd' && rm -f" $$subf " )"; \
	    ( $(am__cd) "$$subd" && rm -f $$subf ) || exit $$?; }; \
	done;

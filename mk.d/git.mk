# PLEASE DO NOT CHANGE SCRIPT BELOW
# AUTHOR: ArrowLee
# DATE: 20230306

ifeq ($(CICD_ARMOR),1)
$(disarm com.mk)
else
include com.mk
endif

git_DIRS ?=
git_BRANCHES ?=
git_REPOS ?=

GIT ?= git
HAVE_GIT = $(call exist_command, $(GIT))

GIT_CLONE = $(GIT) clone
GIT_STASH = $(GIT) stash
GIT_BRANCH = $(GIT) branch
GIT_CHECKOUT = $(GIT) checkout
GIT_FETCH = $(GIT) fetch
GIT_REV_PARSE = $(GIT) rev-parse

rootgitdir ?= git-repo
gitdir ?= $(prefix)/$(rootgitdir)

# for [include filename] instruction, same target will be overrided. to solve this
# problem, common target dependencies should append to a makefile variable;
init:: git-clone-repo git-stash-repo git-fetch-repo git-checkout-branch
clean:: clean-git-repo
install:: install-git-repo
check:: check-git

.PHONY: check-git
check-git:
	@c=($(GIT)); t=($(call batch_exist_command,$(GIT))); $(am__print_result_of_check);


.PHONY: git-stash-repo
git-stash-repo:
	@test 1 -eq $(HAVE_GIT) || exit 0; \
	for p in $(git_DIRS); do \
	  if $(GIT) -C $$p rev-parse --is-inside-work-tree &> /dev/null; then \
	    echo " $(GIT) -C '"$$p"' stash push --all --quiet"; \
	    $(GIT) -C "$$p" stash push --all --quiet || exit $$?; \
	  fi; \
	done;

.PHONY: git-clone-repo
git-clone-repo:
	@test 1 -eq $(HAVE_GIT) || exit 0; \
	list="$(git_REPOS)"; test -n "$$list" || exit 0; \
	list="$(git_DIRS)"; test -n "$$list" || exit 0; \
	m=$(words $(git_DIRS)); n=$(words $(git_REPOS)); \
	if ! test $$m -eq $$n; then \
	  echo " ERR: number of git dir and repo are not same"; exit 1; \
	fi; \
	if test -n "$$list"; then \
	  echo " $(MKDIR_P)" $$list; \
	  $(MKDIR_P) $$list || exit 2; \
	fi; \
	for p in $$list; do \
	  rel=`$(GIT) -C "$$p" rev-parse --path-format=relative --show-toplevel 2>/dev/null`; \
	  test "./" != "$$rel" || continue; \
	  if ls -A1q "$$p" | grep -q .; then \
	    echo " ERR: $$p is not an empty directory"; exit 39; \
	  fi; \
	done; \
	repo="$(git_REPOS)"; branch="$(git_BRANCHES)"; for p in $$list; do \
	  rel=`$(GIT) -C "$$p" rev-parse --path-format=relative --show-toplevel 2>/dev/null`; \
	  test "./" != "$$rel" || continue; \
	  r=`echo $$repo | sed '$(PAT_STRIP_REST_WORD)'`; repo=`echo $$repo | sed '$(PAT_STRIP_FIRST_WORD)'`; \
	  b=`echo $$branch | sed '$(PAT_STRIP_REST_WORD)'`; branch=`echo $$branch | sed '$(PAT_STRIP_FIRST_WORD)'`; \
	  echo " $(GIT) -C '"$$p"' clone --progress --jobs 4 -b "$$b" $$r ."; \
	  $(GIT) -C "$$p" clone --progress --jobs 4 -b "$$b" $$r . || exit $$?; \
	done;


.PHONY: git-fetch-repo
git-fetch-repo:
	@test 1 -eq $(HAVE_GIT) || exit 0; \
	list="$(git_DIRS)"; test -n "$$list" || exit 0; \
	branch="$(git_BRANCHES)"; for p in $$list; do \
	  echo " $(GIT) -C "$$p" fetch --tags --progress --prune --jobs 4"; \
	  $(GIT) -C "$$p" fetch --tags --progress --prune --jobs 4 || exit $$?; \
	done;


.PHONY: git-checkout-branch
git-checkout-branch:
	@test 1 -eq $(HAVE_GIT) || exit 0; \
	list="$(git_DIRS)"; test -n "$$list" || exit 0; \
	br="$(git_BRANCHES)";for p in $$list; do \
	  b=`echo $$br | sed -e '$(PAT_STRIP_REST_WORD)'`; \
	  rb=`echo $$b | sed -e 's,^refs/heads/,,;s,^refs/remotes/origin/,,;s,^,refs/remotes/origin/,'`; \
	  lb=`echo $$b | sed -e 's,^refs/heads/,,;s,^,refs/heads/,;'`; \
	  br=`echo $$br | sed '$(PAT_STRIP_FIRST_WORD)'`; \
	  echo "$$p $$rb $$lb $$b"; \
	done | while read p rb lb b; do \
	  id=`$(GIT) -C "$$p" rev-parse "$$rb" 2> /dev/null`; \
	  if test -z "$$id" || test "$$id" = "$$rb"; then echo " ERR: no such branch $$rb in $$p"; exit 1; fi; \
	  $(GIT) -C "$$p" checkout --quiet -f "$$id" || exit $$?; \
	  if $(GIT) -C "$$p" show-ref --verify --quiet "$$lb" &> /dev/null; then \
	    echo " $(GIT) -C '$$p' branch -D $$b"; \
	    $(GIT) -C "$$p" branch --quiet -D "$$b" || exit $$?; \
	  fi; \
	  echo " $(GIT) -C '$$p' checkout --progress -b $$b $$id"; \
	  $(GIT) -C "$$p" checkout --progress -b "$$b" "$$id" || exit $$?; \
	done;


.PHONY: install-gitdirs
install-gitdirs:
	@for dir in "$(DESTDIR)$(gitdir)"; do \
	  test -z "$$dir" || $(MKDIR_P) "$$dir"; \
	done


.PHONY: clean-git-repo
clean-git-repo:
	-@echo " rm -rf $(git_DIRS)"; rm -rf $(git_DIRS);

.PHONY: uninstall-git-repo
uninstall-git-repo:
	@files="$(git_DIRS)"; dir="$(DESTDIR)$(gitdir)"; $(am__uninstall_files_from_dir);


.PHONY: install-git-repo
install-git-repo: install-gitdirs
	@dir2="$(abs_builddir)"; dir1="$(realpath $(DESTDIR)$(gitdir))"; $(am__relativize); \
	for dir in $(git_DIRS); do \
	  { echo " ( cd '$(DESTDIR)$(gitdir)' && $(LN_S) -n -f '$$reldir/$$dir' '$$dir' )"; \
	    ( $(am__cd) "$(DESTDIR)$(gitdir)" && $(LN_S) -n -f "$$reldir/$$dir" "$$dir"; ) || exit $$?;  } ; \
	done

# PLEASE DO NOT CHANGE SCRIPT BELOW
# AUTHOR: ArrowLee
# DATE: 20210430

ifeq ($(CICD_ARMOR),1)
$(disarm com.mk)
else
include com.mk
endif

img_DOCKERFILES ?=
img_NAMES ?=

REGISTRY_USERNAME ?=
REGISTRY_PASSWORD ?=
REGISTRY ?=

BUILDAH ?= buildah
HAVE_BUILDAH = $(call exist_command, $(BUILDAH))

BUILDAH_BUILD = $(BUILDAH) build-using-dockerfile
BUILDAH_CLEAN = $(BUILDAH) rmi
BUILDAH_IMAGE = $(BUILDAH) images
BUILDAH_LOGIN = $(BUILDAH) login
BUILDAH_PUSH = $(BUILDAH) push
BUILDAH_PULL = $(BUILDAH) pull

# option: --format docker. writing manifest: uploading manifest to harbor "received unexpected HTTP status: 500 Internal Server Error"
# https://www.mankier.com/1/buildah-build
# https://github.com/goharbor/harbor/issues/11328
BUILDAH_BUILD_FLAGS ?= --format docker
BUILDAH_SAVE_FLAGS ?=
BUILDAH_LOAD_FLAGS ?=
BUILDAH_PUSH_FLAGS ?=

BUILDAH_TMP_OUTPUT_FILE ?= .buildah.pack.stdout.tmp
BUILDAH_TMP_SHA_FILE ?= .buildah.pack.sha.tmp
BUILDAH_TMP_IMAGE_SHA ?= $(call substline,$(space),$(file < $(BUILDAH_TMP_SHA_FILE)))
OCI_ARCHIVE_TYPE ?= docker-archive

# if run in container, export image and import image must use --storage-driver=vfs, otherwise error
# will occurs.
# https://insujang.github.io/2020-11-09/building-container-image-inside-container-using-buildah/
# https://stackoverflow.com/questions/20010199/how-to-determine-if-a-process-runs-inside-lxc-docker
# https://stackoverflow.com/questions/23513045/how-to-check-if-a-process-is-running-inside-docker-container
rt__in_container = $(if $(shell grep -s 'docker\|lxc\|libpod\|kubepods' /proc/1/cgroup),1,0)

# run buildah inside container, to avoid some error:
# Error: mount /var/lib/containers/storage/overlay:/var/lib/containers/storage/overlay,
# flags: 0x1000: operation not permitted
#
# https://insujang.github.io/2020-11-09/building-container-image-inside-container-using-buildah/
ifeq ($(rt__in_container),1)
BUILDAH_BUILD_FLAGS = --isolation chroot --storage-driver vfs
BUILDAH_SAVE_FLAGS = --storage-driver vfs
BUILDAH_LOAD_FLAGS = --storage-driver vfs
BUILDAH_PUSH_FLAGS = --storage-driver vfs
endif


# for [include filename] instruction, same target will be overrided. to solve this
# problem, common target dependencies should append to a makefile variable;
pack:: buildah-build-image
push:: buildah-push-image
clean:: buildah-clean-image
ifeq ($(RT_SERIAL_PACK_PUSH),1)
else
install:: buildah-save-image
endif
check:: check-buildah

.PHONY: check-buildah
check-buildah:
	@c=(buildah); t=($(call batch_exist_command,buildah)); $(am__print_result_of_check);

.PHONY: build-image buildah-build-image
build-image buildah-build-image: buildah-login-registry $(img_DOCKERFILES)
	@test 1 -eq $(HAVE_BUILDAH) || exit 0; \
	list="$(img_NAMES)"; test -n "$(img_NAMES)" || exit 0; \
	rm -f $(BUILDAH_TMP_SHA_FILE); files="$(img_DOCKERFILES)"; for p in $$list; do \
	  file=`echo $$files | sed '$(PAT_STRIP_REST_WORD)'`; files=`echo $$files | sed '$(PAT_STRIP_FIRST_WORD)'`; \
	  if test -f "$$file"; then f="$$file"; else f="Dockerfile"; fi; \
	  echo " $(BUILDAH_BUILD) --tag $$p --file $$f $(BUILDAH_BUILD_FLAGS) $(srcdir)"; \
	  $(BUILDAH_BUILD) --tag $$p --file $$f $(BUILDAH_BUILD_FLAGS) $(srcdir) | tee $(BUILDAH_TMP_OUTPUT_FILE); \
	  sha_or_exit=`tail -n 1 $(BUILDAH_TMP_OUTPUT_FILE)`; \
	  if ! echo "$$sha_or_exit" | $(EGREP) -q "^[0-9a-zA-Z]{64}$$"; then \
	    echo "build image failed: $$sha_or_exit" && exit 1; \
	  fi; \
	  echo "$$sha_or_exit" >> $(BUILDAH_TMP_SHA_FILE) || exit $$?; \
	done;

.PHONY: save-image buildah-save-image
save-image buildah-save-image: $(img_DOCKERFILES)
	@$(NORMAL_INSTALL)
	@list='$(img_NAMES)'; test -n "$(datadir)" || list=; \
	if test -n "$$list"; then \
	  echo " $(MKDIR_P) '$(DESTDIR)$(datadir)'"; \
	  $(MKDIR_P) "$(DESTDIR)$(datadir)" || exit 1; \
	fi; \
	sha256s="$(BUILDAH_TMP_IMAGE_SHA)"; for p in $$list; do  \
	  test -z "$$sha256s" && continue; \
	  sha256=`echo $$sha256s | sed '$(PAT_STRIP_REST_WORD)'`; sha256s=`echo $$sha256s | sed '$(PAT_STRIP_FIRST_WORD)'`; \
	  echo "$$p $$sha256" | sed -e 's,:, ,'; \
	done | while read n t s; do \
	  if ! echo "$$s" | $(EGREP) -q "^[0-9a-zA-Z]{64}$$"; then \
	    echo "image sha256 format error: $$s" && exit 1; \
	  fi; \
	  dir="$(DESTDIR)$(datadir)"; files="$$n"; $(am__uninstall_files_from_dir); \
	  echo " $(BUILDAH_PUSH) $(BUILDAH_SAVE_FLAGS) $$s $(OCI_ARCHIVE_TYPE):$(DESTDIR)$(datadir)/$$n"; \
	  $(BUILDAH_PUSH) $(BUILDAH_SAVE_FLAGS) $$s $(OCI_ARCHIVE_TYPE):$(DESTDIR)$(datadir)/$$n || exit $$?; \
	done;

# when variable contains special char, eg: robot$bruce+bl.bot, make will expanded it to robotruce+bl.bot.
# to avoid this problem, use $(value) function
# see https://stackoverflow.com/questions/27414370/how-to-escape-special-chars-in-variable-when-using-makefile
# see https://www.gnu.org/software/make/manual/html_node/Value-Function.html
.PHONY: login-registry buildah-login-registry
login-registry buildah-login-registry:
	@test 1 -eq $(HAVE_BUILDAH) || exit 0; \
	if [ -z "$(REGISTRY)" ] || [ -z "$(REGISTRY_USERNAME)" ] || [ -z "$(REGISTRY_PASSWORD)" ]; then \
	  exit 0; \
	fi; \
	if ! $(BUILDAH_LOGIN) --get-login $(REGISTRY) &> /dev/null; then \
	  echo ' $(BUILDAH_LOGIN) -u $(value REGISTRY_USERNAME) -p $(value REGISTRY_PASSWORD) $(value REGISTRY)'; \
	  $(BUILDAH_LOGIN) -u '$(value REGISTRY_USERNAME)' -p '$(value REGISTRY_PASSWORD)' '$(value REGISTRY)'; \
	fi;


.PHONY: load-image buildah-load-image
load-image buildah-load-image:
	@test 1 -eq $(HAVE_BUILDAH) || exit 0; \
	list="$(img_NAMES)"; test -n "$(img_NAMES)" || exit 0; \
	rm -f $(BUILDAH_TMP_SHA_FILE); for p in $$list; do \
	  echo "$$p" | sed -e 's,:, ,'; \
	done | while read n t; do \
	  f=$(srcdir)$(datadir)/$$n; test -f $$f || continue; \
	  echo " $(BUILDAH_PULL) $(BUILDAH_LOAD_FLAGS) $(OCI_ARCHIVE_TYPE):$$f"; \
	  $(BUILDAH_PULL) $(BUILDAH_LOAD_FLAGS) $(OCI_ARCHIVE_TYPE):$$f | tee $(BUILDAH_TMP_OUTPUT_FILE); \
	  sha_or_exit=`tail -n 1 $(BUILDAH_TMP_OUTPUT_FILE)`; \
	  if ! echo "$$sha_or_exit" | $(EGREP) -q "^[0-9a-zA-Z]{64}$$"; then \
	    echo "import image failed: $$sha_or_exit" && exit 1; \
	  fi; \
	  echo "$$sha_or_exit" >> $(BUILDAH_TMP_SHA_FILE) || exit $$?; \
	done;

.PHONY: push-image buildah-push-image
ifeq ($(RT_SERIAL_PACK_PUSH),1)
push-image buildah-push-image:: buildah-login-registry
else
push-image buildah-push-image:: buildah-login-registry buildah-load-image
endif
	@test 1 -eq $(HAVE_BUILDAH) || exit 0; \
	list="$(img_NAMES)"; test -n "$(img_NAMES)" || exit 0; \
	sha256s="$(BUILDAH_TMP_IMAGE_SHA)"; test -n "$(BUILDAH_TMP_IMAGE_SHA)" || exit 0; \
	for p in $$list; do  \
	  test -n "$$sha256s" || break; \
	  sha256=`echo $$sha256s | sed '$(PAT_STRIP_REST_WORD)'`; sha256s=`echo $$sha256s | sed '$(PAT_STRIP_FIRST_WORD)'`; \
	  if ! echo "$$sha256" | $(EGREP) -q "^[0-9a-zA-Z]{64}$$"; then \
	    echo "image sha256 format error: $$sha256" && exit 1; \
	  fi; \
	  echo " $(BUILDAH_PUSH) $(BUILDAH_PUSH_FLAGS) $$sha256 $(REGISTRY)/$$p"; \
	  $(BUILDAH_PUSH) $(BUILDAH_PUSH_FLAGS) "$$sha256" $(REGISTRY)/$$p || exit $$?; \
	done;

.PHONY: clean-image buildah-clean-image
clean-image buildah-clean-image:
	@test 1 -eq $(HAVE_BUILDAH) || exit 0; \
	list="$(img_NAMES)"; test -n "$(img_NAMES)" || exit 0; \
	sha256s="$(BUILDAH_TMP_IMAGE_SHA)"; for p in $$list; do  \
	  test -n "$$sha256s" || break; \
	  sha256=`echo $$sha256s | sed '$(PAT_STRIP_REST_WORD)'`; sha256s=`echo $$sha256s | sed '$(PAT_STRIP_FIRST_WORD)'`; \
	  if ! echo "$$sha256" | $(EGREP) -q "^[0-9a-zA-Z]{64}$$"; then \
	    echo "image sha256 format error: $$sha256" && exit 1; \
	  fi; \
	  echo " $(BUILDAH_CLEAN) $$sha256"; $(BUILDAH_CLEAN) "$$sha256"; \
	done; \
	echo "rm -f $(BUILDAH_TMP_SHA_FILE) $(BUILDAH_TMP_OUTPUT_FILE)"; \
	rm -f $(BUILDAH_TMP_SHA_FILE) $(BUILDAH_TMP_OUTPUT_FILE);

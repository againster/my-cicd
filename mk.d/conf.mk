# PLEASE DO NOT CHANGE SCRIPT BELOW
# AUTHOR: ArrowLee
# DATE: 20190923

ifeq ($(CICD_ARMOR),1)
$(disarm com.mk)
else
include com.mk
endif

sysconf_DATA ?=
# if sysconf_DATA contains folders, SYSCONF_EXT indicates file extension list and that files should be installed.
# you could define a list, eg:.json .txt.
SYSCONF_EXT ?=
rootconfdir ?= etc
sysconfdir ?= $(prefix)/$(rootconfdir)

# if sysconf_DATA contains folders, SYSCONF_EXT indicates which files should be render. we use find command to find files.
# this variable will expand like this: -name *.go -o -name *.c. and finally be used in find command.
SYSCONF_FIND_FLAGS = $(foreach p,$(SYSCONF_EXT),$(if $(filter $p,$(lastword $(SYSCONF_EXT))),-name '*$p',-name '*$p' -o))


# for [include filename] instruction, same target will be overrided. to solve this
# problem, common target dependencies should append to a makefile variable;
install:: install-sysconfDATA
uninstall:: uninstall-sysconfDATA
installdirs:: install-sysconfdirs


.PHONY: install-sysconfDATA
install-sysconfDATA:
	@$(NORMAL_INSTALL)
	@list='$(sysconf_DATA)'; test -n "$(sysconfdir)" || list=; \
	if test -n "$$list"; then \
	  echo " $(MKDIR_P) '$(DESTDIR)$(sysconfdir)'"; \
	  $(MKDIR_P) "$(DESTDIR)$(sysconfdir)" || exit $$?; \
	fi; \
	flist=; for p in $$list; do \
	  if test -e $(srcdir)/$$p; then d=$(srcdir)/; elif test -e $$p; then d=; else \
	    echo " No such file or directory '$$p'"; exit 2; \
	  fi; \
	  if ! test -d "$$d$$p"; then flist="$$flist $$d$$p"; continue; fi; \
	  transform="s,^$$d$$p,,;s,^/,,;"; echo $$d$$p | $(am__recursive_dir_dirs) | \
	  while read subd; do \
	    test -n "$$subd" || continue; \
	    { echo " ( cd '$(DESTDIR)$(sysconfdir)' && $(MKDIR_P)" $$subd ")"; \
	    ( $(am__cd) "$(DESTDIR)$(sysconfdir)" && $(MKDIR_P) $$subd ) || exit $$?; } \
	  done; \
	  flag=( $(SYSCONF_FIND_FLAGS) ); echo $$d$$p | $(am__recursive_dir_files) | \
	  while read subd subf; do \
	    reldir=`echo $$subd | sed -e "s,^$$d$$p,,;"`; \
	    { echo " ( cd '$$subd' && $(INSTALL_DATA)" $$subf "'$(DESTDIR)$(sysconfdir)$$reldir' )"; \
	    ( $(am__cd) "$$subd" && $(INSTALL_DATA) $$subf "$(DESTDIR)$(sysconfdir)$$reldir" ) || exit $$?; } \
	  done; \
	done; \
	for p in $$flist; do echo "$$p"; done | $(am__base_list) | \
	while read f; do \
	  echo " $(INSTALL_DATA) $$f '$(DESTDIR)$(sysconfdir)'"; \
	  $(INSTALL_DATA) $$f "$(DESTDIR)$(sysconfdir)" || exit $$?; \
	done;


.PHONY: install-sysconfDATA-OLD1
install-sysconfDATA-OLD1:
	@$(NORMAL_INSTALL)
	@list='$(sysconf_DATA)'; test -n "$(sysconfdir)" || list=; \
	if test -n "$$list"; then \
	  echo " $(MKDIR_P) '$(DESTDIR)$(sysconfdir)'"; \
	  $(MKDIR_P) "$(DESTDIR)$(sysconfdir)" || exit $$?; \
	fi; \
	for p in $$list; do \
	  if test -d $(srcdir)/$$p; then d=$(srcdir)/; \
	  elif test -d $$p; then d=; \
	  else continue; fi; \
	  src=$$d$$p; dest="$(DESTDIR)$(sysconfdir)"; \
	  $(am__install_subdir_dirs_to_dir); \
	  flag=( $(SYSCONF_FIND_FLAGS) ); $(am__install_subdir_files_to_dir); \
	done; \
	for p in $$list; do \
	  if test -f $(srcdir)/$$p; then d=$(srcdir)/; \
	  elif test -f $$p; then d=; \
	  else continue; fi; \
	  echo "$$d$$p"; \
	done | $(am__base_list) | while read files; do \
	  echo " $(INSTALL_DATA) $$files '$(DESTDIR)$(sysconfdir)'"; \
	  $(INSTALL_DATA) $$files "$(DESTDIR)$(sysconfdir)" || exit $$?; \
	done;

# https://stackoverflow.com/questions/1909188/define-make-variable-at-rule-execution-time
# $(eval)

.PHONY: uninstall-sysconfDATA
uninstall-sysconfDATA:
	@$(NORMAL_UNINSTALL)
	-@list='$(sysconf_DATA)'; test -n "$(sysconfdir)" || list=; \
	for p in $$list; do \
	  if test -d $(srcdir)/$$p; then d=$(srcdir)/; \
	  elif test -d $$p; then d=; \
	  else continue; fi; \
	  src=$$d$$p; dest="$(DESTDIR)$(sysconfdir)"; \
	  flag=( $(SYSCONF_FIND_FLAGS) ); $(am__uninstall_subdir_files_from_dir); \
	done; \
	files=`for p in $$list; do test -d "$(srcdir)/$$p" || echo $$p; done | sed -e 's|^.*/||'`; \
	dir='$(DESTDIR)$(sysconfdir)'; $(am__uninstall_files_from_dir);


.PHONY: install-sysconfdirs
install-sysconfdirs:
	@list='$(sysconf_DATA)'; test -n "$(sysconfdir)" || list=; \
	if test -n "$$list"; then \
	  echo " $(MKDIR_P) '$(DESTDIR)$(sysconfdir)'"; \
	  $(MKDIR_P) "$(DESTDIR)$(sysconfdir)" || exit 1; \
	fi;
# PLEASE DO NOT CHANGE SCRIPT BELOW
# AUTHOR: ArrowLee
# DATE: 20180726

ifeq ($(CICD_ARMOR),1)
$(disarm com.mk)
else
include com.mk
endif

gobin_PROGRAMS ?=
go_MAINS ?=

GOOS ?=
GOARCH ?=
GO111MODULE ?=
GOPROXY ?=
GOPATH ?=
GOROOT ?=
PKG_CONFIG_PATH ?=
C_INCLUDE_PATH ?=
CGO_ENABLED ?=

GO ?= go
HAVE_GO = $(call exist_command, $(GO))

GO_BUILD = ${GO} build
GO_TEST = ${GO} test
GO_ENV = ${GO} env

GO_BUILD_FLAGS ?=
GO_TEST_FLAGS ?=
GO_EXE_EXT ?=

# for [include filename] instruction, same target will be overrided. to solve this
# problem, common target dependencies should append to a makefile variable;
all:: bin-go
build:: bin-go
clean:: clean-gobinPROGRAMS
uninstall:: uninstall-gobinPROGRAMS
install:: install-gobinPROGRAMS
installdirs:: install-godirs
test:: test-go
check:: check-go


.PHONY: check-go
check-go:
	@c=(go); t=($(call batch_exist_command,go)); $(am__print_result_of_check);

# https://stackoverflow.com/questions/1909188/define-make-variable-at-rule-execution-time
# $(eval)

.PHONY: gobuild-env
gobuild-env:
$(eval $(call export_env_var_if_no_empty,GOOS))
$(eval $(call export_env_var_if_no_empty,GOARCH))
$(eval $(call export_env_var_if_no_empty,GO111MODULE))
$(eval $(call export_env_var_if_no_empty,GOPROXY))
$(eval $(call export_env_var_if_no_empty,GOPATH))
$(eval $(call export_env_var_if_no_empty,GOROOT))
$(eval $(call export_env_var_if_no_empty,PKG_CONFIG_PATH))
$(eval $(call export_env_var_if_no_empty,C_INCLUDE_PATH))
$(eval $(call export_env_var_if_no_empty,CGO_ENABLED))

.PHONY: bin-go
bin-go: gobuild-env
	@test 1 -eq $(HAVE_GO) || exit 0; \
	list="$(gobin_PROGRAMS)"; test -n "$(gobin_PROGRAMS)" || exit 0; \
	m="$(go_MAINS)"; for p in $$list; do \
	  main=`echo $$m | sed '$(PAT_STRIP_REST_WORD)'`; m=`echo $$m | sed '$(PAT_STRIP_FIRST_WORD)'`; \
	  if test "$(abs_srcdir)" -ef "$(abs_builddir)"; then \
	    echo " $(GO_BUILD) -o $$p $(GO_BUILD_FLAGS) $$main"; \
	    $(GO_BUILD) -o $$p $(GO_BUILD_FLAGS) $$main || exit $$?; \
	  else \
	    echo " ( cd $(srcdir) && $(GO_BUILD) -o $(abs_builddir)/$$p $(GO_BUILD_FLAGS) $$main )"; \
	    ( cd $(srcdir) && $(GO_BUILD) -o $(abs_builddir)/$$p $(GO_BUILD_FLAGS) $$main; ) || exit $$?;\
	  fi; \
	done;

.PHONY: test-go
test-go: gobuild-env
	@test 1 -eq $(HAVE_GO) || exit 0; \
	echo " $(GO_TEST) $(GO_TEST_FLAGS)"; $(GO_TEST) $(GO_TEST_FLAGS) || exit $$?;


.PHONY: install-godirs
install-godirs:
	@for dir in "$(DESTDIR)$(bindir)"; do \
	  test -z "$$dir" || $(MKDIR_P) "$$dir"; \
	done


.PHONY: install-gobinPROGRAMS
install-gobinPROGRAMS: $(gobin_PROGRAMS)
	@$(NORMAL_INSTALL)
	@list='$(gobin_PROGRAMS)'; test -n "$(bindir)" || list=; \
	if test -n "$$list"; then \
	  echo " $(MKDIR_P) '$(DESTDIR)$(bindir)'"; \
	  $(MKDIR_P) "$(DESTDIR)$(bindir)" || exit 1; \
	fi; \
	for p in $$list; do echo "$$p $$p"; done | \
	sed 's/$(GO_EXE_EXT)$$//' | \
	while read p p1; do if test -f $$p \
	 || test -f $$p1 \
	  ; then echo "$$p"; echo "$$p"; else :; fi; \
	done | \
	sed -e 'p;s,.*/,,;n;h' \
		-e 's|.*|.|' \
		-e 'p;x;s,.*/,,;s/$(GO_EXE_EXT)$$//;$(transform);s/$$/$(GO_EXE_EXT)/' | \
	sed 'N;N;N;s,\n, ,g' | \
	$(AWK) 'BEGIN { files["."] = ""; dirs["."] = 1 } \
	  { d=$$3; if (dirs[d] != 1) { print "d", d; dirs[d] = 1 } \
		if ($$2 == $$4) files[d] = files[d] " " $$1; \
		else { print "f", $$3 "/" $$4, $$1; } } \
	  END { for (d in files) print "f", d, files[d] }' | \
	while read type dir files; do \
		if test "$$dir" = .; then dir=; else dir=/$$dir; fi; \
		test -z "$$files" || { \
		echo " $(INSTALL_PROGRAM) $$files '$(DESTDIR)$(bindir)$$dir'"; \
		$(INSTALL_PROGRAM) $$files "$(DESTDIR)$(bindir)$$dir" || exit $$?; \
		} \
	; done

.PYONY: uninstall-gobinPROGRAMS
uninstall-gobinPROGRAMS:
	@$(NORMAL_UNINSTALL)
	@list='$(gobin_PROGRAMS)'; test -n "$(bindir)" || list=; \
	files=`for p in $$list; do echo "$$p"; done | \
	  sed -e 'h;s,^.*/,,;s/$(GO_EXE_EXT)$$//;$(transform)' \
		  -e 's/$$/$(GO_EXE_EXT)/' \
	`; \
	test -n "$$list" || exit 0; \
	echo " ( cd '$(DESTDIR)$(bindir)' && rm -f" $$files ")"; \
	cd "$(DESTDIR)$(bindir)" && rm -f $$files

.PHONY: clean-gobinPROGRAMS
clean-gobinPROGRAMS:
	@list='$(gobin_PROGRAMS)'; test -n "$$list" || exit 0; \
	echo " rm -f" $$list; \
	rm -f $$list || exit $$?; \
	test -n "$(GO_EXE_EXT)" || exit 0; \
	list=`for p in $$list; do echo "$$p"; done | sed 's/$(GO_EXE_EXT)$$//'`; \
	echo " rm -f" $$list; \
	rm -f $$list

.PHONY: clean-goCACHE
clean-goCACHE:
	@test 1 -eq $(HAVE_GO) || exit 0; \
	if test -e "$$($(GO_ENV) GOCACHE)"; then \
	  echo " rm -rf $$($(GO_ENV) GOCACHE)"; \
	  rm -rf $$($(GO_ENV) GOCACHE) || exit $$?; \
	fi;


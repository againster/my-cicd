# PLEASE DO NOT CHANGE SCRIPT BELOW
# AUGHOR: ArrowLee
# DATE: 20210430

ifeq ($(CICD_ARMOR),1)
$(disarm com.mk)
else
include com.mk
endif

HAVE_DOCKER = $(call exist_command, docker)
HAVE_BUILDAH = $(call exist_command, buildah)

ifeq ($(HAVE_BUILDAH), 1)
MAKE_IMAGE_MK = buildah.mk
else ifeq ($(HAVE_DOCKER), 1)
MAKE_IMAGE_MK = docker.mk
else
$(error no found docker, podman or buildah)
endif

ifeq ($(CICD_ARMOR),1)
$(disarm $(MAKE_IMAGE_MK))
else
include $(MAKE_IMAGE_MK)
endif

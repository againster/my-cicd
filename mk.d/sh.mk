# PLEASE DO NOT CHANGE SCRIPT BELOW
# AUTHOR: ArrowLee
# DATE: 20230203

ifeq ($(CICD_ARMOR),1)
$(disarm com.mk)
else
include com.mk
endif

shbin_PROGRAMS ?=
# if shbin_PROGRAMS contains folders, SHBIN_EXT indicates file extension list and that files should be installed.
# you could define a list, eg:.json .txt.
SHBIN_EXT ?=
rootshbindir ?= bin
shbindir ?= $(prefix)/$(rootshbindir)

# if shbin_PROGRAMS contains folders, SHBIN_EXT indicates which files should be render. we use find command to find files.
# this variable will expand like this: -name *.go -o -name *.c. and finally be used in find command.
SHBIN_FIND_FLAGS = $(foreach p,$(SHBIN_EXT),$(if $(filter $p,$(lastword $(SHBIN_EXT))),-name '*$p',-name '*$p' -o))


# for [include filename] instruction, same target will be overrided. to solve this
# problem, common target dependencies should append to a makefile variable;
install:: install-shbinPROGRAMS
uninstall:: uninstall-shbinPROGRAMS
installdirs:: install-shdirs


.PHONY: install-shbinPROGRAMS
install-shbinPROGRAMS:
	@$(NORMAL_INSTALL)
	@list='$(shbin_PROGRAMS)'; test -n "$(shbindir)" || list=; \
	if test -n "$$list"; then \
	  echo " $(MKDIR_P) '$(DESTDIR)$(shbindir)'"; \
	  $(MKDIR_P) "$(DESTDIR)$(shbindir)" || exit $$?; \
	fi; \
	flist=; for p in $$list; do \
	  if test -e $(srcdir)/$$p; then d=$(srcdir)/; elif test -e $$p; then d=; else \
	    echo " No such file or directory '$$p'"; exit 2; \
	  fi; \
	  if ! test -d "$$d$$p"; then flist="$$flist $$d$$p"; continue; fi; \
	  transform="s,^$$d$$p,,;s,^/,,;"; echo $$d$$p | $(am__recursive_dir_dirs) | \
	  while read subd; do \
	    test -n "$$subd" || continue; \
	    { echo " ( cd '$(DESTDIR)$(shbindir)' && $(MKDIR_P)" $$subd ")"; \
	    ( $(am__cd) "$(DESTDIR)$(shbindir)" && $(MKDIR_P) $$subd ) || exit $$?; } \
	  done; echo "shit $$p";\
	  flag=( $(SHBIN_FIND_FLAGS) ); echo $$d$$p | $(am__recursive_dir_files) | \
	  while read subd subf; do \
	    reldir=`echo $$subd | sed -e "s,^$$d$$p,,;"`; \
	    { echo " ( cd '$$subd' && $(INSTALL_PROGRAM)" $$subf "'$(DESTDIR)$(shbindir)$$reldir' )"; \
	    ( $(am__cd) "$$subd" && $(INSTALL_PROGRAM) $$subf "$(DESTDIR)$(shbindir)$$reldir" ) || exit $$?; } \
	  done; \
	done; \
	for p in $$flist; do echo "$$p"; done | $(am__base_list) | \
	while read f; do \
	  echo " $(INSTALL_PROGRAM) $$f '$(DESTDIR)$(shbindir)'"; \
	  $(INSTALL_PROGRAM) $$f "$(DESTDIR)$(shbindir)" || exit $$?; \
	done;


.PHONY: uninstall-shbinPROGRAMS
uninstall-shbinPROGRAMS:
	@$(NORMAL_UNINSTALL)
	-@list='$(shbin_PROGRAMS)'; test -n "$(shbindir)" || list=; \
	flist=; dlist=; for p in $$list; do \
	  if test -e $(srcdir)/$$p; then d=$(srcdir)/; elif test -e $$p; then d=; else \
	    echo " No such file or directory '$$p'"; exit 2; \
	  fi; \
	  if ! test -d "$$d$$p"; then flist="$$flist $$d$$p"; else dlist="$$dlist $$d$$p"; fi; \
	done; \
	for p in $$dlist; do \
	  flag=( $(SHBIN_FIND_FLAGS) ); transform=; \
	  echo $$p | $(am__recursive_dir_files) | while read subd subf; do \
	    reldir=`echo $$subd | sed -e "s,^$$p,,;s,^/,,;"`; \
	    if test -z "$$reldir"; then \
	      dir="$(DESTDIR)$(shbindir)"; files=$$subf; \
	      $(am__uninstall_files_from_dir); \
	    else \
	      dir="$(DESTDIR)$(shbindir)/$$reldir"; files=$$subf; \
	      $(am__uninstall_files_from_dir); \
        fi; \
	  done; \
	done; \
	for p in $$flist; do echo "$$p" | sed -e "$(PAT_STRIP_EXT);$(PAT_STRIP_DIR);"; done | \
	$(am__base_list) | while read files; do  \
	  dir="$(DESTDIR)$(shbindir)/$$reldir"; \
	  $(am__uninstall_files_from_dir); \
	done;


.PHONY: install-shbindirs
install-shbindirs:
	@list='$(shbin_PROGRAMS)'; test -n "$(shbindir)" || list=; \
	if test -n "$$list"; then \
	  echo " $(MKDIR_P) '$(DESTDIR)$(shbindir)'"; \
	  $(MKDIR_P) "$(DESTDIR)$(shbindir)" || exit 1; \
	fi;
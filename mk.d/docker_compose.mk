# PLEASE DO NOT CHANGE SCRIPT BELOW
# AUTHOR: ArrowLee
# DATE: 20230718
ifeq ($(CICD_ARMOR),1)
$(disarm com.mk)
else
include com.mk
endif

docker_COMPOSEFILES ?=

DOCKER_COMPOSE ?= docker-compose
HAVE_DOCKER_COMPOSE = $(call exist_command, $(DOCKER_COMPOSE))

DOCKER_COMPOSE_FLAGS ?=
DOCKER_COMPOSE_UP_FLAGS ?= --detach --remove-orphans --renew-anon-volumes --wait
DOCKER_COMPOSE_HOST ?=

# for [include filename] instruction, same target will be overrided. to solve this
# problem, common target dependencies should append to a makefile variable;
deploy:: docker-compose-deploy-image
check:: check-docker-compose

.PHONY: check-docker-compose
check-docker-compose:
	@c=($(DOCKER_COMPOSE)); t=($(HAVE_DOCKER_COMPOSE)); $(am__print_result_of_check);

.PHONY: docker-compose-context-env
docker-compose-context-env:
$(eval $(call export_env_var_if_no_empty_kv,DOCKER_HOST,$(DOCKER_COMPOSE_HOST)))

.PHONY: docker-compose-deploy-image
docker-compose-deploy-image: docker-compose-context-env $(docker_COMPOSEFILES)
	@test 1 -eq $(HAVE_DOCKER_COMPOSE) || exit 0; \
	list="$(docker_COMPOSEFILES)"; test -n "$(docker_COMPOSEFILES)" || exit 0; \
	for p in $$list; do \
	  if test -f $$p; then d=; else d="$(srcdir)/"; fi; \
	  echo "-f $$d$$p"; \
	done | $(am__base_list) | \
	while read files; do \
	  echo " $(DOCKER_COMPOSE) $(DOCKER_COMPOSE_FLAGS) $$files up $(DOCKER_COMPOSE_UP_FLAGS)"; \
	  $(DOCKER_COMPOSE) $(DOCKER_COMPOSE_FLAGS) $$files up $(DOCKER_COMPOSE_UP_FLAGS) || exit $$?; \
	done;

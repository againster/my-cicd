# PLEASE DO NOT CHANGE SCRIPT BELOW
# AUTHOR: ArrowLee
# DATE: 20230115

ifeq ($(CICD_ARMOR),1)
$(disarm com.mk)
else
include com.mk
endif

tar_NAMES ?=
tar_FILES ?=

TAR ?= tar
HAVE_TAR = $(call exist_command, $(TAR))

TAR_BUILD = $(TAR) -c -f
TAR_BUILD_FLAGS ?= -z
TAR_BUILD_IGNOREFILE ?= .tarignore

pack:: tar-build
clean:: tar-uninstall
install:: tar-install
check:: check-tar

.PHONY: check-tar
check-tar:
	@c=($(TAR)); t=($(HAVE_TAR)); $(am__print_result_of_check);


.PHONY: tar-build
tar-build:
	@test 1 -eq $(HAVE_TAR) || exit 0; \
	list="$(tar_NAMES)"; test -n "$(tar_NAMES)" || exit 0; \
	file="$(tar_FILES)"; test -n "$(tar_FILES)" || exit 0; \
	for p in $$list; do \
	  f=`echo $$file | sed '$(PAT_STRIP_REST_WORD)'`; file=`echo $$file | sed '$(PAT_STRIP_FIRST_WORD)'`;\
	  if test -f $$f; then d=; else d="$(srcdir)/"; fi; f=$$d$$f; \
	  if test -f $(TAR_BUILD_IGNOREFILE); then d=; else d="$(srcdir)/"; fi; \
	  if test -f $$d$$p; then x=; else x="-X $(TAR_BUILD_IGNOREFILE)"; fi; \
	  echo "$$p $$f $$x"; \
	done | while read p f x; do \
	  echo " $(TAR_BUILD) $$p $$x -C $(srcdir) -T $$f $(TAR_BUILD_FLAGS)"; \
	  $(TAR_BUILD) $$p $$x -C $(srcdir) -T $$f $(TAR_BUILD_FLAGS) || exit $$?;\
	done;


.PHONY: tar-install
tar-install:
	@$(NORMAL_INSTALL)
	@list='$(tar_NAMES)'; test -n "$(tar_NAMES)" || exit 0; \
	if test -n "$$list"; then \
	  echo " $(MKDIR_P) '$(DESTDIR)$(datadir)'"; \
	  $(MKDIR_P) $(DESTDIR)$(datadir) || exit $$?; \
	fi; \
	echo " $(INSTALL_DATA)" $$list "'$(DESTDIR)$(datadir)'"; \
	$(INSTALL_DATA) $$list $(DESTDIR)$(datadir) || exit $$?;


.PHONY: tar-uninstall
tar-uninstall:
	@$(NORMAL_UNINSTALL)
	@list='$(tar_NAMES)'; test -n "$(tar_NAMES)" || exit 0; \
	echo " ( cd '$(DESTDIR)$(datadir)' && rm -f" $$list ")"; \
	cd $(DESTDIR)$(datadir) && rm -f $$list;
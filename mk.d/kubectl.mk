# PLEASE DO NOT CHANGE SCRIPT BELOW
# AUTHOR: ArrowLee
# DATE: 20210618

ifeq ($(CICD_ARMOR),1)
$(disarm com.mk)
else
include com.mk
endif

k8s_MANIFESTS ?=

KUBECTL ?= kubectl
HAVE_KUBECTL = $(call exist_command, $(KUBECTL))

KUBECTL_APPLY = $(KUBECTL) apply
KUBECTL_ROLLOUT_STATUS = $(KUBECTL) rollout status

KUBECTL_APPLY_FLAGS =
KUBECTL_ROLLOUT_STATUS_FLAGS = --watch=false --timeout=3s
KUBECTL_ROLLOUT_STATUS_MAX_RETRY = 120
KUBECTL_ROLLOUT_STATUS_WAIT = 5
KUBECTL_ROLLOUT_STATUS_SUCCESS_REGEX = ^.*successfully\ rolled\ out$$

# for [include filename] instruction, same target will be overrided. to solve this
# problem, common target dependencies should append to a makefile variable;
deploy:: kubectl-deploy
poll:: kubectl-poll
check:: check-kubectl

.PHONY: check-kubectl
check-kubectl:
	@c=($(KUBECTL)); t=($(HAVE_KUBECTL)); $(am__print_result_of_check);

.PHONY: kubectl-deploy
kubectl-deploy: $(k8s_MANIFESTS)
	@test 1 -eq $(HAVE_KUBECTL) || exit 0; \
	list='$(k8s_MANIFESTS)'; test -n '$(k8s_MANIFESTS)' || exit 0; \
	for p in $$list; do \
	  if test -f $$p; then d=; else d="$(srcdir)/"; fi; \
	  echo "-f $$d$$p"; \
	done | $(am__base_list) | \
	while read files; do \
	  echo " $(KUBECTL_APPLY) $(KUBECTL_APPLY_FLAGS) $$files"; \
	  $(KUBECTL_APPLY) $(KUBECTL_APPLY_FLAGS) $$files || exit $$?; \
	done;


.PHONY: kubectl-poll
kubectl-poll: $(k8s_MANIFESTS)
	@test 1 -eq $(HAVE_KUBECTL) || exit 0; \
	list='$(k8s_MANIFESTS)'; test -n '$(k8s_MANIFESTS)' || exit 0; \
	n=1; failed=0; while [ $$n -le $(KUBECTL_ROLLOUT_STATUS_MAX_RETRY) ]; do \
	  echo " poll deployment status [$$n/$(KUBECTL_ROLLOUT_STATUS_MAX_RETRY)]"; \
	  failed=0; for p in $$list; do \
	    if test -f $$p; then d=; else d="$(srcdir)/"; fi; \
	    status=`$(KUBECTL_ROLLOUT_STATUS) $(KUBECTL_ROLLOUT_STATUS_FLAGS) -f $$d$$p;`; echo "$$status"; \
	    if ! echo "$$status" | $(EGREP) -q "$(KUBECTL_ROLLOUT_STATUS_SUCCESS_REGEX)"; then \
	      (( failed = failed + 1)); \
	    fi; \
	  done; \
	  if [ $$failed -eq 0 ]; then break; fi; \
	  (( n = n + 1)); sleep $(KUBECTL_ROLLOUT_STATUS_WAIT); \
	done; if [ $$n -gt $(KUBECTL_ROLLOUT_STATUS_MAX_RETRY) ]; then \
	  echo " deployment failed [$$failed/$(words $(k8s_MANIFESTS))]"; exit 1; \
	fi;

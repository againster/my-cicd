# PLEASE DO NOT CHANGE SCRIPT BELOW
# AUTHOR: ArrowLee
# DATE: 20230110

ifeq ($(CICD_ARMOR),1)
$(disarm com.mk)
else
include com.mk
endif

img_DOCKERFILES ?=
img_NAMES ?=

DOCKER_RUN_NAMES ?= $(filter-out :::%,$(subst :, :::,$(filter-out %///,$(subst /,/// ,$(img_NAMES)))))
DOCKER_RUN_REPLICAS ?= 1

REGISTRY_USERNAME ?=
REGISTRY_PASSWORD ?=
REGISTRY ?=

DOCKER ?= docker
HAVE_DOCKER = $(call exist_command, $(DOCKER))

DOCKER_BUILD = $(DOCKER) build
DOCKER_IMAGE = $(DOCKER) image
DOCKER_LOGIN = $(DOCKER) login
DOCKER_CONTAINER = $(DOCKER) container
DOCKER_RUN = $(DOCKER) run

DOCKER_BUILD_FLAGS ?=
DOCKER_SAVE_FLAGS ?=
DOCKER_LOAD_FLAGS ?=
DOCKER_PUSH_FLAGS ?=
DOCKER_CLEAN_FLAGS ?=
DOCKER_RUN_FLAGS ?= -d

DOCKER_HOST ?=

DOCKER_IIDFILE_EXT = .sha.tmp
DOCKER_NAME_TRANSFORM = s,:,_,g

# for [include filename] instruction, same target will be overrided. to solve this
# problem, common target dependencies should append to a makefile variable;
pack:: docker-build-image
push:: docker-push-image
clean:: docker-clean-image
ifeq ($(RT_SERIAL_PACK_PUSH),1)
else
install:: docker-save-image
endif
deploy:: docker-recreate-deploy-image
check:: check-docker

.PHONY: check-docker
check-docker:
	@c=($(DOCKER)); t=($(HAVE_DOCKER)); $(am__print_result_of_check);

.PHONY: docker-build-env
docker-build-env:
export DOCKER_SCAN_SUGGEST=false #https://github.com/docker/scan-cli-plugin/issues/149
$(eval $(call export_env_var_if_no_empty,DOCKER_BUILDKIT)) # --mount option requires BuildKit, https://blog.opstree.com/2021/04/20/docker-buildkit-faster-builds-mounts-and-features/

.PHONY: docker-context-env
docker-context-env:
$(eval $(call export_env_var_if_no_empty,DOCKER_HOST))

.PHONY: build-image docker-build-image
build-image docker-build-image: docker-build-env docker-login-registry $(img_DOCKERFILES)
	@test 1 -eq $(HAVE_DOCKER) || exit 0; \
	list="$(img_NAMES)"; test -n "$(img_NAMES)" || exit 0; \
	files="$(img_DOCKERFILES)"; for p in $$list; do \
	  file=`echo $$files | sed '$(PAT_STRIP_REST_WORD)'`; files=`echo $$files | sed '$(PAT_STRIP_FIRST_WORD)'`; \
	  if test -f "$$file"; then f="$$file"; else f="Dockerfile"; fi; \
	  name=`echo "$$p" | sed -e '$(DOCKER_NAME_TRANSFORM)'`; \
	  echo " $(DOCKER_IMAGE) build --tag $$p --iidfile $$name$(DOCKER_IIDFILE_EXT) --file $$f $(DOCKER_BUILD_FLAGS) $(srcdir)"; \
	  $(DOCKER_IMAGE) build --tag $$p --iidfile $$name$(DOCKER_IIDFILE_EXT) --file $$f $(DOCKER_BUILD_FLAGS) $(srcdir) || exit $$?; \
	  iid=`cat "$$name$(DOCKER_IIDFILE_EXT)"`; echo "$$iid"; \
	done;

.PHONY: save-image docker-save-image
save-image docker-save-image: $(img_DOCKERFILES)
	@$(NORMAL_INSTALL)
	@list='$(img_NAMES)'; test -n "$(datadir)" || list=; \
	if test -n "$$list"; then \
	  echo " $(MKDIR_P) '$(DESTDIR)$(datadir)'"; \
	  $(MKDIR_P) "$(DESTDIR)$(datadir)" || exit 1; \
	fi; \
	for p in $$list; do  \
	  name=`echo "$$p" | sed -e '$(DOCKER_NAME_TRANSFORM)'`; \
	  test -f "$$name$(DOCKER_IIDFILE_EXT)" || continue; \
	  iid=`cat $$name$(DOCKER_IIDFILE_EXT)`; test -n "$$iid" || continue; \
	  echo "$$name $$iid"; \
	done | while read n s; do \
	  if ! echo "$$s" | $(EGREP) -q '^sha256:[0-9a-zA-Z]{64}$$'; then \
	    echo "image sha256 format error: $$n,$$s" && exit 1; \
	  fi; \
	  dir="$(DESTDIR)$(datadir)"; files="$$n"; $(am__uninstall_files_from_dir); \
	  echo " $(DOCKER_IMAGE) save $(DOCKER_SAVE_FLAGS) -o $(DESTDIR)$(datadir)/$$n $$s"; \
	  $(DOCKER_IMAGE) save $(DOCKER_SAVE_FLAGS) -o $(DESTDIR)$(datadir)/$$n $$s || exit $$?; \
	done;

# when variable contains special char, eg: robot$bruce+bl.bot, make will expanded it to robotruce+bl.bot.
# to avoid this problem, use $(value) function
# see https://stackoverflow.com/questions/27414370/how-to-escape-special-chars-in-variable-when-using-makefile
# see https://www.gnu.org/software/make/manual/html_node/Value-Function.html
.PHONY: login-registry docker-login-registry
login-registry docker-login-registry:
	@test 1 -eq $(HAVE_DOCKER) || exit 0; \
	if [ -z "$(REGISTRY)" ] || [ -z "$(REGISTRY_USERNAME)" ] || [ -z "$(REGISTRY_PASSWORD)" ]; then \
	  exit 0; \
	fi; \
	echo ' $(DOCKER_LOGIN) -u $(value REGISTRY_USERNAME) -p $(value REGISTRY_PASSWORD) $(REGISTRY)'; \
	echo '$(value REGISTRY_PASSWORD)' | $(DOCKER_LOGIN) -u '$(value REGISTRY_USERNAME)' \
	--password-stdin '$(value REGISTRY)' || exit $$?;

.PHONY: load-image docker-load-image
load-image docker-load-image:
	@test 1 -eq $(HAVE_DOCKER) || exit 0; \
	list="$(img_NAMES)"; test -n "$(img_NAMES)" || exit 0; \
	for p in $$list; do \
	  name=`echo "$$p" | sed -e '$(DOCKER_NAME_TRANSFORM)'`; \
	  echo "$$name $$name$(DOCKER_IIDFILE_EXT) $$p"; \
	done | while read n s p; do \
	  f=$(srcdir)$(datadir)/$$n; test -f $$f || continue; \
	  echo " $(DOCKER_IMAGE) load $(DOCKER_LOAD_FLAGS) -i $$f "; \
	  $(DOCKER_IMAGE) load $(DOCKER_LOAD_FLAGS) -i $$f || exit $$?; \
	done;


.PHONY: push-image docker-push-image
ifeq ($(RT_SERIAL_PACK_PUSH),1)
push-image docker-push-image:: docker-login-registry
else
push-image docker-push-image:: docker-login-registry docker-load-image
endif
	@test 1 -eq $(HAVE_DOCKER) || exit 0; \
	list="$(img_NAMES)"; test -n "$(img_NAMES)" || exit 0; \
	for p in $$list; do  \
	  echo " $(DOCKER_IMAGE) tag $$p $(REGISTRY)/$$p"; \
	  $(DOCKER_IMAGE) tag $$p $(REGISTRY)/$$p || exit $$?; \
	  echo " $(DOCKER_IMAGE) push $(DOCKER_PUSH_FLAGS) $(REGISTRY)/$$p"; \
	  $(DOCKER_IMAGE) push $(DOCKER_PUSH_FLAGS) $(REGISTRY)/$$p || exit $$?; \
	done;

.PHONY: clean-image docker-clean-image
clean-image docker-clean-image:
	@test 1 -eq $(HAVE_DOCKER) || exit 0; \
	list="$(img_NAMES)"; test -n "$(img_NAMES)" || exit 0; \
	for p in $$list; do  \
	  name=`echo "$$p" | sed -e '$(DOCKER_NAME_TRANSFORM)'`; \
	  test -f "$$name$(DOCKER_IIDFILE_EXT)" || continue; \
	  iid=`cat $$name$(DOCKER_IIDFILE_EXT)`; test -n "$$iid" || continue; \
	  if ! echo "$$iid" | $(EGREP) -q '^sha256:[0-9a-zA-Z]{64}$$'; then \
	    continue; \
	  fi; \
	  echo " $(DOCKER_IMAGE) rm $(DOCKER_CLEAN_FLAGS) $$iid"; \
	  $(DOCKER_IMAGE) rm $(DOCKER_CLEAN_FLAGS) "$$iid" || continue; \
	  echo "rm -f $$name$(DOCKER_IIDFILE_EXT)"; \
	  rm -f $$name$(DOCKER_IIDFILE_EXT); \
	done;

# this deploy like k8s recreate deployment. see https://kubernetes.io/docs/concepts/workloads/controllers/deployment/#recreate-deployment
.PHONY: docker-recreate-deploy-image
docker-recreate-deploy-image: docker-context-env docker-login-registry
	@test 1 -eq $(HAVE_DOCKER) || exit 0; \
	list="$(img_NAMES)"; test -n "$(img_NAMES)" || exit 0; \
	if test $(words $(img_NAMES)) -ne $(words $(DOCKER_RUN_NAMES)); then \
	  echo "number of run name and image name not same"; exit 1; \
	fi; \
	names="$(DOCKER_RUN_NAMES)"; for p in $$list; do \
	  name=$$(sed -e '$(PAT_STRIP_REST_WORD);' <<< "$$names"); \
	  names=$$(sed -e '$(PAT_STRIP_FIRST_WORD);' <<< "$$names"); \
	  echo "$$name $$p"; \
	done | while read d p; do \
	  oiid=$$($(DOCKER_CONTAINER) ls -a -q -f "label=docker_run_name=$${d}"); \
	  if test -n "$$oiid"; then \
	    echo " $(DOCKER_CONTAINER) rm -f -v $$oiid"; \
	    $(DOCKER_CONTAINER) rm -f -v $$oiid || exit $$?; \
	  fi; \
	  pids=;for ((i=1;i<=$(DOCKER_RUN_REPLICAS);i++)); do \
	    cname="$${d}-$${i}"; \
	    echo " $(DOCKER_RUN) --name $${cname} --label docker_run_name=$$d $(DOCKER_RUN_FLAGS) $$p"; \
	    $(DOCKER_RUN) --name "$${cname}" --label "docker_run_name=$$d" $(DOCKER_RUN_FLAGS) $$p || exit $$? & \
	    pids="$$pids $$!"; \
	  done; wait $$pids || exit $$?; \
	done;
